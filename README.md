# Open Core LEMS

Main repository for the [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/) LEMS Module.

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.
