package it.csi.lems.lemsbatch.exception;

public class BadErrorNeedsEmailSendingException extends Exception{
		
	private int errorCode;
	
	public BadErrorNeedsEmailSendingException(String message) {
		super(message);
        
    }
	
	public BadErrorNeedsEmailSendingException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public BadErrorNeedsEmailSendingException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
	
	public BadErrorNeedsEmailSendingException(String message, Throwable cause, int errorCode) {
        this(message, cause);
        this.errorCode = errorCode;
    }
	
	public int getErrorCode() {
		return errorCode;
	}
	
	public void sendEmail() {
		
	}
	
}
