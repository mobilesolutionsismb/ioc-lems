package it.csi.lems.lemsbatch.dto.ws.uk;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FloodsAreaDto implements Serializable  {
	private Item items;

	public Item getItems() {
		return items;
	}

	@JsonProperty("items")
	public void setItems(Item items) {
		this.items = items;
	}

	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Item {
		private String id;
		private String lat;
		private String longitude;
		private String label;

		@JsonProperty("@id")
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getLat() {
			return lat;
		}

		public void setLat(String lat) {
			this.lat = lat;
		}

		@JsonProperty("long")
		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		@JsonProperty("label")
		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}
		
		
		
	}
}

