package it.csi.lems.lemsbatch.dto.ws;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IdiResult implements Serializable{
	int metadataid;

	@JsonProperty("metadataid")
	public int getMetadataid() {
		return metadataid;
	}

	public void setMetadataid(int metadataid) {
		this.metadataid = metadataid;
	}
}
