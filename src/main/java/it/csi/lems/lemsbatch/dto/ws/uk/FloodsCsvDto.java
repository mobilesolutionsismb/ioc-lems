package it.csi.lems.lemsbatch.dto.ws.uk;

import java.io.Serializable;

import it.csi.lems.lemsbatch.utils.LemsWebConstant;

public class FloodsCsvDto implements Serializable  {
	private String county;
	private int day;
	private String date;
	private String floodRisk;
	private String rivers;
	private String surfaceWater;
	private String coastalTidal;
	private String groundwater;

	
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFloodRisk() {
		return floodRisk;
	}
	public void setFloodRisk(String floodRisk) {
		this.floodRisk = floodRisk;
	}
	public String getRivers() {
		return rivers;
	}
	public void setRivers(String rivers) {
		this.rivers = rivers;
	}
	public String getSurfaceWater() {
		return surfaceWater;
	}
	public void setSurfaceWater(String surfaceWater) {
		this.surfaceWater = surfaceWater;
	}
	public String getCoastalTidal() {
		return coastalTidal;
	}
	public void setCoastalTidal(String coastalTidal) {
		this.coastalTidal = coastalTidal;
	}
	public String getGroundwater() {
		return groundwater;
	}
	public void setGroundwater(String groundwater) {
		this.groundwater = groundwater;
	}
}

