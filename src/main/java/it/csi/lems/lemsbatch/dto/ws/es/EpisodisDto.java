package it.csi.lems.lemsbatch.dto.ws.es;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EpisodisDto implements Serializable  {
	private Long idEpisodi;
	private Meteor meteor;
	private Avisos[] avisos;
	private Long idEstatEpisodi;
	private String nomEstatEpisodi;
	private String dataCancelacio;
	
	@JsonProperty("idEpisodi")
	public Long getIdEpisodi() {
		return idEpisodi;
	}

	public void setIdEpisodi(Long idEpisodi) {
		this.idEpisodi = idEpisodi;
	}	
	
	@JsonProperty("meteor")
	public Meteor getMeteor() {
		return meteor;
	}

	public void setMeteor(Meteor meteor) {
		this.meteor = meteor;
	}

	@JsonProperty("avisos")
	public Avisos[] getAvisos() {
		return avisos;
	}

	public void setAvisos(Avisos[] avisos) {
		this.avisos = avisos;
	}

	@JsonProperty("idEstatEpisodi")
	public Long getIdEstatEpisodi() {
		return idEstatEpisodi;
	}

	public void setIdEstatEpisodi(Long idEstatEpisodi) {
		this.idEstatEpisodi = idEstatEpisodi;
	}

	@JsonProperty("nomEstatEpisodi")
	public String getNomEstatEpisodi() {
		return nomEstatEpisodi;
	}

	public void setNomEstatEpisodi(String nomEstatEpisodi) {
		this.nomEstatEpisodi = nomEstatEpisodi;
	}

	@JsonProperty("dataCancelacio")
	public String getDataCancelacio() {
		return dataCancelacio;
	}

	public void setDataCancelacio(String dataCancelacio) {
		this.dataCancelacio = dataCancelacio;
	}



	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Meteor {
		private Long idMeteor;
		private String nom;
		
		@JsonProperty("idMeteor")
		public Long getIdMeteor() {
			return idMeteor;
		}
		public void setIdMeteor(Long idMeteor) {
			this.idMeteor = idMeteor;
		}
		@JsonProperty("nom")
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Avisos{
		private Long idTipusAvis;
		private String nomTipusAvis;
		private Long idAvis;
		private Long idEpisodi;
		private String dataEmisio;
		private String dataInici;
		private String dataFi;
		private String dataFinalitzacio;
		private String dataPublicacio;
		private Evolucions[] evolucions;
		private Long idAvisPare;
		private Long idEstatAvis;
		private String nomEstatAvis;
		
		@JsonProperty("idTipusAvis")
		public Long getIdTipusAvis() {
			return idTipusAvis;
		}
		public void setIdTipusAvis(Long idTipusAvis) {
			this.idTipusAvis = idTipusAvis;
		}
		
		@JsonProperty("nomTipusAvis")
		public String getNomTipusAvis() {
			return nomTipusAvis;
		}
		public void setNomTipusAvis(String nomTipusAvis) {
			this.nomTipusAvis = nomTipusAvis;
		}
		
		@JsonProperty("idAvis")
		public Long getIdAvis() {
			return idAvis;
		}
		public void setIdAvis(Long idAvis) {
			this.idAvis = idAvis;
		}
		
		@JsonProperty("idEpisodi")
		public Long getIdEpisodi() {
			return idEpisodi;
		}
		public void setIdEpisodi(Long idEpisodi) {
			this.idEpisodi = idEpisodi;
		}
		
		@JsonProperty("dataEmisio")
		public String getDataEmisio() {
			return dataEmisio;
		}
		public void setDataEmisio(String dataEmisio) {
			this.dataEmisio = dataEmisio;
		}
		
		@JsonProperty("dataInici")
		public String getDataInici() {
			return dataInici;
		}
		public void setDataInici(String dataInici) {
			this.dataInici = dataInici;
		}
		
		@JsonProperty("dataFi")
		public String getDataFi() {
			return dataFi;
		}
		public void setDataFi(String dataFi) {
			this.dataFi = dataFi;
		}
		
		@JsonProperty("dataFinalitzacio")
		public String getDataFinalitzacio() {
			return dataFinalitzacio;
		}
		public void setDataFinalitzacio(String dataFinalitzacio) {
			this.dataFinalitzacio = dataFinalitzacio;
		}
		
		@JsonProperty("dataPublicacio")
		public String getDataPublicacio() {
			return dataPublicacio;
		}
		public void setDataPublicacio(String dataPublicacio) {
			this.dataPublicacio = dataPublicacio;
		}
		
		@JsonProperty("evolucions")
		public Evolucions[] getEvolucions() {
			return evolucions;
		}
		public void setEvolucions(Evolucions[] evolucions) {
			this.evolucions = evolucions;
		}
		
		@JsonProperty("idAvisPare")
		public Long getIdAvisPare() {
			return idAvisPare;
		}
		public void setIdAvisPare(Long idAvisPare) {
			this.idAvisPare = idAvisPare;
		}
		
		@JsonProperty("idEstatAvis")
		public Long getIdEstatAvis() {
			return idEstatAvis;
		}
		public void setIdEstatAvis(Long idEstatAvis) {
			this.idEstatAvis = idEstatAvis;
		}
		
		@JsonProperty("nomEstatAvis")
		public String getNomEstatAvis() {
			return nomEstatAvis;
		}
		public void setNomEstatAvis(String nomEstatAvis) {
			this.nomEstatAvis = nomEstatAvis;
		}
		
		
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Evolucions{
		private String dia;
		private String comentari;
		private Long representatiu;
		private Long idLlindar1;
		private String nomLlindar1;
		private Long idLlindar2;	
		private String nomLlindar2;	
		private Long idDistribucioGeografica;
		private String labelDistribucioGrografica;
		private Periodes periodes[];
		private String valorMaxim;
		
		@JsonProperty("dia")
		public String getDia() {
			return dia;
		}
		public void setDia(String dia) {
			this.dia = dia;
		}
		
		@JsonProperty("comentari")
		public String getComentari() {
			return comentari;
		}
		public void setComentari(String comentari) {
			this.comentari = comentari;
		}
		
		@JsonProperty("representatiu")
		public Long getRepresentatiu() {
			return representatiu;
		}
		public void setRepresentatiu(Long representatiu) {
			this.representatiu = representatiu;
		}
		
		@JsonProperty("idLlindar1")
		public Long getIdLlindar1() {
			return idLlindar1;
		}
		public void setIdLlindar1(Long idLlindar1) {
			this.idLlindar1 = idLlindar1;
		}
		
		@JsonProperty("nomLlindar1")
		public String getNomLlindar1() {
			return nomLlindar1;
		}
		public void setNomLlindar1(String nomLlindar1) {
			this.nomLlindar1 = nomLlindar1;
		}
		
		@JsonProperty("idLlindar2")
		public Long getIdLlindar2() {
			return idLlindar2;
		}
		public void setIdLlindar2(Long idLlindar2) {
			this.idLlindar2 = idLlindar2;
		}
		
		@JsonProperty("nomLlindar2")
		public String getNomLlindar2() {
			return nomLlindar2;
		}
		public void setNomLlindar2(String nomLlindar2) {
			this.nomLlindar2 = nomLlindar2;
		}
		
		@JsonProperty("idDistribucioGeografica")
		public Long getIdDistribucioGeografica() {
			return idDistribucioGeografica;
		}
		public void setIdDistribucioGeografica(Long idDistribucioGeografica) {
			this.idDistribucioGeografica = idDistribucioGeografica;
		}
		
		@JsonProperty("labelDistribucioGrografica")
		public String getLabelDistribucioGrografica() {
			return labelDistribucioGrografica;
		}
		public void setLabelDistribucioGrografica(String labelDistribucioGrografica) {
			this.labelDistribucioGrografica = labelDistribucioGrografica;
		}
		
		@JsonProperty("periodes")
		public Periodes[] getPeriodes() {
			return periodes;
		}
		public void setPeriodes(Periodes[] periodes) {
			this.periodes = periodes;
		}
		
		@JsonProperty("valorMaxim")
		public String getValorMaxim() {
			return valorMaxim;
		}
		public void setValorMaxim(String valorMaxim) {
			this.valorMaxim = valorMaxim;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Periodes{	
		private Long idPeriode;
		private String nomPeriode;
		private Afectacions[] afectacions;
		
		@JsonProperty("idPeriode")
		public Long getIdPeriode() {
			return idPeriode;
		}
		public void setIdPeriode(Long idPeriode) {
			this.idPeriode = idPeriode;
		}
		
		@JsonProperty("nomPeriode")
		public String getNomPeriode() {
			return nomPeriode;
		}
		public void setNomPeriode(String nomPeriode) {
			this.nomPeriode = nomPeriode;
		}
		
		@JsonProperty("afectacions")
		public Afectacions[] getAfectacions() {
			return afectacions;
		}
		public void setAfectacions(Afectacions[] afectacions) {
			this.afectacions = afectacions;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Afectacions{
		private Long idLlindar;
		private String nomLlindar;
		private Boolean auxiliar;
		private Long perill;
		private Long idRegio;
		private String nomRegio;
		private Long nivell;
		
		@JsonProperty("idLlindar")
		public Long getIdLlindar() {
			return idLlindar;
		}
		public void setIdLlindar(Long idLlindar) {
			this.idLlindar = idLlindar;
		}
		
		@JsonProperty("nomLlindar")
		public String getNomLlindar() {
			return nomLlindar;
		}
		public void setNomLlindar(String nomLlindar) {
			this.nomLlindar = nomLlindar;
		}
		
		@JsonProperty("auxiliar")
		public Boolean getAuxiliar() {
			return auxiliar;
		}
		public void setAuxiliar(Boolean auxiliar) {
			this.auxiliar = auxiliar;
		}
		
		@JsonProperty("perill")
		public Long getPerill() {
			return perill;
		}
		public void setPerill(Long perill) {
			this.perill = perill;
		}
		
		@JsonProperty("idRegio")
		public Long getIdRegio() {
			return idRegio;
		}
		public void setIdRegio(Long idRegio) {
			this.idRegio = idRegio;
		}
		
		@JsonProperty("nomRegio")
		public String getNomRegio() {
			return nomRegio;
		}
		public void setNomRegio(String nomRegio) {
			this.nomRegio = nomRegio;
		}
		
		@JsonProperty("nivell")
		public Long getNivell() {
			return nivell;
		}
		public void setNivell(Long nivell) {
			this.nivell = nivell;
		}
	}
}
