package it.csi.lems.lemsbatch.dto.ws.uk;

import java.io.Serializable;
import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MultiPolygon implements Serializable{
	private Feature features[];
		
	public Feature[] getFeatures() {
		return features;
	}

	public void setFeatures(Feature[] features) {
		this.features = features;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Feature {
		private Geometry geometry;

		public Geometry getGeometry() {
			return geometry;
		}

		public void setGeometry(Geometry geometry) {
			this.geometry = geometry;
		}

		
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Geometry {
		private String type;
		private double coordinates[][][][];
		
		public void setType(String type) {
			this.type = type;
		}
		
		public String getType() {
			// TODO Auto-generated method stub
			return type;
		}

		public double[][][][] getCoordinates() {
			return coordinates;
		}

		public void setCoordinates(double[][][][] coordinates) {
			this.coordinates = coordinates;
		}

		
		
		
	}	
	
	
	
}
