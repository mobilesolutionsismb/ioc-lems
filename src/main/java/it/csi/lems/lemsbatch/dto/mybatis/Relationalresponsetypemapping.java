package it.csi.lems.lemsbatch.dto.mybatis;

import java.io.Serializable;
import java.util.Date;

public class Relationalresponsetypemapping extends RelationalresponsetypemappingKey implements Serializable {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.RelationalResponseTypeMapping.Creation Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Date creationTime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.RelationalResponseTypeMapping.LastModification Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Date lastmodificationTime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.RelationalResponseTypeMapping.IsDeleted
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Boolean isdeleted;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.RelationalResponseTypeMapping.DeletionTime
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Date deletiontime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table lems.RelationalResponseTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.RelationalResponseTypeMapping.Creation Time
	 * @return  the value of lems.RelationalResponseTypeMapping.Creation Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Date getCreationTime() {
		return creationTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.RelationalResponseTypeMapping.Creation Time
	 * @param creationTime  the value for lems.RelationalResponseTypeMapping.Creation Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.RelationalResponseTypeMapping.LastModification Time
	 * @return  the value of lems.RelationalResponseTypeMapping.LastModification Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Date getLastmodificationTime() {
		return lastmodificationTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.RelationalResponseTypeMapping.LastModification Time
	 * @param lastmodificationTime  the value for lems.RelationalResponseTypeMapping.LastModification Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setLastmodificationTime(Date lastmodificationTime) {
		this.lastmodificationTime = lastmodificationTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.RelationalResponseTypeMapping.IsDeleted
	 * @return  the value of lems.RelationalResponseTypeMapping.IsDeleted
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Boolean getIsdeleted() {
		return isdeleted;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.RelationalResponseTypeMapping.IsDeleted
	 * @param isdeleted  the value for lems.RelationalResponseTypeMapping.IsDeleted
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.RelationalResponseTypeMapping.DeletionTime
	 * @return  the value of lems.RelationalResponseTypeMapping.DeletionTime
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Date getDeletiontime() {
		return deletiontime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.RelationalResponseTypeMapping.DeletionTime
	 * @param deletiontime  the value for lems.RelationalResponseTypeMapping.DeletionTime
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setDeletiontime(Date deletiontime) {
		this.deletiontime = deletiontime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.RelationalResponseTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", creationTime=").append(creationTime);
		sb.append(", lastmodificationTime=").append(lastmodificationTime);
		sb.append(", isdeleted=").append(isdeleted);
		sb.append(", deletiontime=").append(deletiontime);
		sb.append(", serialVersionUID=").append(serialVersionUID);
		sb.append("]");
		return sb.toString();
	}
}