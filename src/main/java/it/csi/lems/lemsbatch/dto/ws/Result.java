package it.csi.lems.lemsbatch.dto.ws;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result implements Serializable{
	int id;

	@JsonProperty("result")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
