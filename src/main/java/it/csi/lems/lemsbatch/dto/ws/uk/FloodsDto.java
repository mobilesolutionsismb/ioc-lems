package it.csi.lems.lemsbatch.dto.ws.uk;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FloodsDto implements Serializable  {
	private String context;
	private Meta meta;
	private Item[] items;

	@JsonProperty("@context")
	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
	
	@JsonProperty("meta")
	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public Item[] getItems() {
		return items;
	}

	@JsonProperty("items")
	public void setItems(Item items[]) {
		this.items = items;
	}


	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Meta {
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Item {
		private String id;
		private String description;
		private String eaAreaName;
		private String eaRegionName;
		private FloodArea floodArea;
		private String floodAreaID;
		private Boolean isTidal;
		private String message;
		private String severity;
		private Long severityLevel;
		private String timeMessageChanged;	
		private String timeRaised;	
		private String timeSeverityChanged;	
		private String idLink;
		

		public String getIdLink() {
			return idLink;
		}

		public void setIdLink(String idLink) {
			this.idLink = idLink;
		}

		@JsonProperty("@id")
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		@JsonProperty("description")
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@JsonProperty("eaAreaName")
		public String getEaAreaName() {
			return eaAreaName;
		}

		public void setEaAreaName(String eaAreaName) {
			this.eaAreaName = eaAreaName;
		}

		@JsonProperty("eaRegionName")
		public String getEaRegionName() {
			return eaRegionName;
		}

		public void setEaRegionName(String eaRegionName) {
			this.eaRegionName = eaRegionName;
		}

		@JsonProperty("floodArea")
		public FloodArea getFloodArea() {
			return floodArea;
		}

		public void setFloodArea(FloodArea floodArea) {
			this.floodArea = floodArea;
		}

		@JsonProperty("floodAreaID")
		public String getFloodAreaID() {
			return floodAreaID;
		}

		public void setFloodAreaID(String floodAreaID) {
			this.floodAreaID = floodAreaID;
		}

		@JsonProperty("isTidal")
		public Boolean getIsTidal() {
			return isTidal;
		}

		public void setIsTidal(Boolean isTidal) {
			this.isTidal = isTidal;
		}

		@JsonProperty("message")
		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		@JsonProperty("severity")
		public String getSeverity() {
			return severity;
		}

		public void setSeverity(String severity) {
			this.severity = severity;
		}

		@JsonProperty("severityLevel")
		public Long getSeverityLevel() {
			return severityLevel;
		}

		public void setSeverityLevel(Long severityLevel) {
			this.severityLevel = severityLevel;
		}

		@JsonProperty("timeMessageChanged")
		public String getTimeMessageChanged() {
			return timeMessageChanged;
		}

		public void setTimeMessageChanged(String timeMessageChanged) {
			this.timeMessageChanged = timeMessageChanged;
		}

		@JsonProperty("timeRaised")
		public String getTimeRaised() {
			return timeRaised;
		}

		public void setTimeRaised(String timeRaised) {
			this.timeRaised = timeRaised;
		}

		@JsonProperty("timeSeverityChanged")
		public String getTimeSeverityChanged() {
			return timeSeverityChanged;
		}

		public void setTimeSeverityChanged(String timeSeverityChanged) {
			this.timeSeverityChanged = timeSeverityChanged;
		}
		
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class FloodArea{
		private String id;
		private String county;
		private String notation;
		private String polygon;
		private String riverOrSea;
		
		@JsonProperty("@id")
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		@JsonProperty("county")
		public String getCounty() {
			return county;
		}

		public void setCounty(String county) {
			this.county = county;
		}

		@JsonProperty("notation")
		public String getNotation() {
			return notation;
		}

		public void setNotation(String notation) {
			this.notation = notation;
		}

		@JsonProperty("polygon")
		public String getPolygon() {
			return polygon;
		}

		public void setPolygon(String polygon) {
			this.polygon = polygon;
		}

		@JsonProperty("riverOrSea")
		public String getRiverOrSea() {
			return riverOrSea;
		}

		public void setRiverOrSea(String riverOrSea) {
			this.riverOrSea = riverOrSea;
		}
	}
}

