package it.csi.lems.lemsbatch.dto.ws.uk;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StatementDto implements Serializable  {
	private Statement[] statements;

	@JsonProperty("statements")
	public Statement[] getStatements() {
		return statements;
	}

	public void setStatements(Statement[] statements) {
		this.statements = statements;
	}


	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Statement {
		private Long id;
		private String detailedCsvUrl;
		private String issuedAt;
		private String lastModifiedAt;
		private String nextIssueDueAt;
		
		
		@JsonProperty("id")
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		@JsonProperty("detailed_csv_url")
		public String getDetailedCsvUrl() {
			return detailedCsvUrl;
		}
		
		public void setDetailedCsvUrl(String detailedCsvUrl) {
			this.detailedCsvUrl = detailedCsvUrl;
		}

		@JsonProperty("issued_at")
		public String getIssuedAt() {
			return issuedAt;
		}

		public void setIssuedAt(String issuedAt) {
			this.issuedAt = issuedAt;
		}

		@JsonProperty("last_modified_at")
		public String getLastModifiedAt() {
			return lastModifiedAt;
		}

		public void setLastModifiedAt(String lastModifiedAt) {
			this.lastModifiedAt = lastModifiedAt;
		}

		@JsonProperty("next_issue_due_at")
		public String getNextIssueDueAt() {
			return nextIssueDueAt;
		}

		public void setNextIssueDueAt(String nextIssueDueAt) {
			this.nextIssueDueAt = nextIssueDueAt;
		}
	}

}

