package it.csi.lems.lemsbatch.dto.mybatis;

import java.io.Serializable;
import java.util.Date;

public class Responsetypemapping implements Serializable {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.ResponseTypeMapping.Id
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.ResponseTypeMapping.Value
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private String value;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.ResponseTypeMapping.Creation Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Date creationTime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.ResponseTypeMapping.LastModification Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Date lastmodificationTime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.ResponseTypeMapping.IsDeleted
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Boolean isdeleted;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.ResponseTypeMapping.Deletion Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private Date deletionTime;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table lems.ResponseTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.ResponseTypeMapping.Id
	 * @return  the value of lems.ResponseTypeMapping.Id
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.ResponseTypeMapping.Id
	 * @param id  the value for lems.ResponseTypeMapping.Id
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.ResponseTypeMapping.Value
	 * @return  the value of lems.ResponseTypeMapping.Value
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public String getValue() {
		return value;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.ResponseTypeMapping.Value
	 * @param value  the value for lems.ResponseTypeMapping.Value
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.ResponseTypeMapping.Creation Time
	 * @return  the value of lems.ResponseTypeMapping.Creation Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Date getCreationTime() {
		return creationTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.ResponseTypeMapping.Creation Time
	 * @param creationTime  the value for lems.ResponseTypeMapping.Creation Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.ResponseTypeMapping.LastModification Time
	 * @return  the value of lems.ResponseTypeMapping.LastModification Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Date getLastmodificationTime() {
		return lastmodificationTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.ResponseTypeMapping.LastModification Time
	 * @param lastmodificationTime  the value for lems.ResponseTypeMapping.LastModification Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setLastmodificationTime(Date lastmodificationTime) {
		this.lastmodificationTime = lastmodificationTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.ResponseTypeMapping.IsDeleted
	 * @return  the value of lems.ResponseTypeMapping.IsDeleted
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Boolean getIsdeleted() {
		return isdeleted;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.ResponseTypeMapping.IsDeleted
	 * @param isdeleted  the value for lems.ResponseTypeMapping.IsDeleted
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.ResponseTypeMapping.Deletion Time
	 * @return  the value of lems.ResponseTypeMapping.Deletion Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public Date getDeletionTime() {
		return deletionTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.ResponseTypeMapping.Deletion Time
	 * @param deletionTime  the value for lems.ResponseTypeMapping.Deletion Time
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	public void setDeletionTime(Date deletionTime) {
		this.deletionTime = deletionTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ResponseTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", value=").append(value);
		sb.append(", creationTime=").append(creationTime);
		sb.append(", lastmodificationTime=").append(lastmodificationTime);
		sb.append(", isdeleted=").append(isdeleted);
		sb.append(", deletionTime=").append(deletionTime);
		sb.append(", serialVersionUID=").append(serialVersionUID);
		sb.append("]");
		return sb.toString();
	}
}