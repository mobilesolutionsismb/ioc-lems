package it.csi.lems.lemsbatch.dto.ws.uk;

import java.io.Serializable;
import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Geojson implements Serializable{
	private String type;
	private Feature features[];
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Feature[] getFeatures() {
		return features;
	}

	public void setFeatures(Feature[] features) {
		this.features = features;
	}

	public static class Feature {
		private String type="Feature";
		private HashMap<String, Object> properties;
		private HashMap<String, Object> geometry;
		
		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public HashMap<String, Object> getProperties() {
			return properties;
		}

		public void setProperties(HashMap<String, Object> properties) {
			this.properties = properties;
		}

		public HashMap<String, Object> getGeometry() {
			return geometry;
		}

		public void setGeometry(HashMap<String, Object> geometry) {
			this.geometry = geometry;
		}
	}
	
	
	
}
