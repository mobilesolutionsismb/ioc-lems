package it.csi.lems.lemsbatch.dto.mybatis;

import java.io.Serializable;

public class Meteoalarmpolygon implements Serializable {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.MeteoAlarmPolygon.Id
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.MeteoAlarmPolygon.emmaId
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	private String emmaid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.MeteoAlarmPolygon.point
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	private String point;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.MeteoAlarmPolygon.polygonAsString
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	private String polygonasstring;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.MeteoAlarmPolygon.nut
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	private String nut;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column lems.MeteoAlarmPolygon.nome
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	private String nome;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table lems.MeteoAlarmPolygon
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.MeteoAlarmPolygon.Id
	 * @return  the value of lems.MeteoAlarmPolygon.Id
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.MeteoAlarmPolygon.Id
	 * @param id  the value for lems.MeteoAlarmPolygon.Id
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.MeteoAlarmPolygon.emmaId
	 * @return  the value of lems.MeteoAlarmPolygon.emmaId
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public String getEmmaid() {
		return emmaid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.MeteoAlarmPolygon.emmaId
	 * @param emmaid  the value for lems.MeteoAlarmPolygon.emmaId
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public void setEmmaid(String emmaid) {
		this.emmaid = emmaid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.MeteoAlarmPolygon.point
	 * @return  the value of lems.MeteoAlarmPolygon.point
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public String getPoint() {
		return point;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.MeteoAlarmPolygon.point
	 * @param point  the value for lems.MeteoAlarmPolygon.point
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public void setPoint(String point) {
		this.point = point;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.MeteoAlarmPolygon.polygonAsString
	 * @return  the value of lems.MeteoAlarmPolygon.polygonAsString
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public String getPolygonasstring() {
		return polygonasstring;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.MeteoAlarmPolygon.polygonAsString
	 * @param polygonasstring  the value for lems.MeteoAlarmPolygon.polygonAsString
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public void setPolygonasstring(String polygonasstring) {
		this.polygonasstring = polygonasstring;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.MeteoAlarmPolygon.nut
	 * @return  the value of lems.MeteoAlarmPolygon.nut
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public String getNut() {
		return nut;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.MeteoAlarmPolygon.nut
	 * @param nut  the value for lems.MeteoAlarmPolygon.nut
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public void setNut(String nut) {
		this.nut = nut;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column lems.MeteoAlarmPolygon.nome
	 * @return  the value of lems.MeteoAlarmPolygon.nome
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column lems.MeteoAlarmPolygon.nome
	 * @param nome  the value for lems.MeteoAlarmPolygon.nome
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmPolygon
	 * @mbg.generated  Fri Oct 12 10:49:07 CEST 2018
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", emmaid=").append(emmaid);
		sb.append(", point=").append(point);
		sb.append(", polygonasstring=").append(polygonasstring);
		sb.append(", nut=").append(nut);
		sb.append(", nome=").append(nome);
		sb.append(", serialVersionUID=").append(serialVersionUID);
		sb.append("]");
		return sb.toString();
	}
}