package it.csi.lems.lemsbatch.dto.ws;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonProperty;
import org.json.JSONObject;

public class CreateAlertAndWarning implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String type;
	private String start;
	private String end;
	private String level;
	private String hazard;
	private String description;
	private String areaOfInterest;
	private String location;
	private int receivers[];
	private String sourceOfInformation;
	private String communicationStatus; 
	//from api_v2
	private String CAPStatus;
	private String CAPResponseType;
	private String CAPUrgency;
	private String CAPCertainty;
	private String CAPCategory;
	private String CAPIdentifier;
	private String WarningLevel;
	private JSONObject extensionData;
	private String instruction;
	private String resource;
	private String web;
	private String title;
	
	//for meteoalarm
	private String language;
	
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(Long level) {
		int realValue = 6-level.intValue();
		switch (realValue) {
			//***minor -> level 4
			case 2: this.level="noSpecialAwarenessRequired";
					break;
			//***moderate -> level 3
			case 3: this.level="bePrepared";
					break;
			//***severe -> level 2
			case 4: this.level="actionRequired";
					break;
			//***extreme -> level 1
			case 5: this.level="dangerToLife";
					break;
			
		}
	}
	public String getHazard() {
		return hazard;
	}
	public void setHazard(String hazard) {
		this.hazard = hazard;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAreaOfInterest() {
		return areaOfInterest;
	}
	public void setAreaOfInterest(String areaOfInterest) {
		this.areaOfInterest = areaOfInterest;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int[] getReceivers() {
		return receivers;
	}
	public void setReceivers(int[] receivers) {
		this.receivers = receivers;
	}
	public String getSourceOfInformation() {
		return sourceOfInformation;
	}
	public void setSourceOfInformation(String sourceOfInformation) {
		this.sourceOfInformation = sourceOfInformation;
	}
	public String getCommunicationStatus() {
		return communicationStatus;
	}
	public void setCommunicationStatus(String communicationStatus) {
		this.communicationStatus = communicationStatus;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@JsonProperty("CAPStatus")
	public String getCAPStatus() {
		return CAPStatus;
	}
	public void setCAPStatus(String CAPStatus) {
		this.CAPStatus = CAPStatus;
	}
	@JsonProperty("CAPResponseType")
	public String getCAPResponseType() {
		return CAPResponseType;
	}
	public void setCAPResponseType(String CAPResponseType) {
		this.CAPResponseType = CAPResponseType;
	}
	@JsonProperty("CAPUrgency")
	public String getCAPUrgency() {
		return CAPUrgency;
	}
	public void setCAPUrgency(String CAPUrgency) {
		this.CAPUrgency = CAPUrgency;
	}
	@JsonProperty("CAPCertainty")
	public String getCAPCertainty() {
		return CAPCertainty;
	}
	public void setCAPCertainty(String CAPCertainty) {
		this.CAPCertainty = CAPCertainty;
	}
	@JsonProperty("CAPCategory")
	public String getCAPCategory() {
		return CAPCategory;
	}
	public void setCAPCategory(String CAPCategory) {
		this.CAPCategory = CAPCategory;
	}
	@JsonProperty("CAPIdentifier")
	public String getCAPIdentifier() {
		return CAPIdentifier;
	}
	public void setCAPIdentifier(String CAPIdentifier) {
		this.CAPIdentifier = CAPIdentifier;
	}
	@JsonProperty("warningLevel")
	public String getWarningLevel() {
		return WarningLevel;
	}
	public void setWarningLevel(String warningLevel) {
		this.WarningLevel = warningLevel;
	}
	public JSONObject getExtensionData() {
		return extensionData;
	}
	public void setExtensionData(JSONObject extensionData) {
		this.extensionData = extensionData;
	}
	public String getInstruction() {
		return instruction;
	}
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
			
	public String getWeb() {
		return web;
	}
	public void setWeb(String web) {
		this.web = web;
	}
}
