package it.csi.lems.lemsbatch.dto.ws;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

public class ApiAuthentication implements Serializable{
	
	@JsonProperty("userNameOrEmailAddress")
	private String user;
	
	@JsonProperty("password")
	private String pwd;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	
}
