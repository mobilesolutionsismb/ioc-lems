package it.csi.lems.lemsbatch.helper;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;
import it.csi.lems.lemsbatch.dto.ws.uk.FloodsCsvDto;

public class CsvHelper {

	public static List<FloodsCsvDto>[] readFloodCsv(String url) throws Exception {
		InputStream is;
		is = new URL(url).openConnection().getInputStream();
		CSVReader csvReader = new CSVReader(new InputStreamReader(is, "UTF-8"), ',', '"', '@', 4);
		ColumnPositionMappingStrategy<FloodsCsvDto> mappingStrategy = new ColumnPositionMappingStrategy<FloodsCsvDto>();
		mappingStrategy.setType(FloodsCsvDto.class);
		String[] columns = new String[] { "county", "day", "date", "floodRisk", "rivers", "surfaceWater",
				"coastalTidal", "groundwater" };
		mappingStrategy.setColumnMapping(columns);
		CsvToBean<FloodsCsvDto> ctb = new CsvToBean<FloodsCsvDto>();
		List<FloodsCsvDto> floodsList = ctb.parse(mappingStrategy, csvReader);
		@SuppressWarnings("unchecked")
		List<FloodsCsvDto> result[] = new List[5];
		result[0] = new ArrayList<FloodsCsvDto>();
		result[1] = new ArrayList<FloodsCsvDto>();
		result[2] = new ArrayList<FloodsCsvDto>();
		result[3] = new ArrayList<FloodsCsvDto>();
		result[4] = new ArrayList<FloodsCsvDto>();

		for (FloodsCsvDto flood : floodsList) {
			switch (flood.getDay()) {
			case 1:
				result[0].add(flood);
				break;
			case 2:
				result[1].add(flood);
				break;
			case 3:
				result[2].add(flood);
				break;
			case 4:
				result[3].add(flood);
				break;
			case 5:
				result[4].add(flood);
				break;
			}
		}

		return result;
	}
}
