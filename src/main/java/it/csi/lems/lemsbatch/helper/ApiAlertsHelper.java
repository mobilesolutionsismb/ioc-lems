package it.csi.lems.lemsbatch.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import it.csi.lems.lemsbatch.dto.mybatis.Capalerts;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalertareageocodes;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalertareas;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalertparameters;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalerts;
import it.csi.lems.lemsbatch.dto.mybatis.Relationalcategorymapping;
import it.csi.lems.lemsbatch.dto.mybatis.Relationalresponsetypemapping;
import it.csi.lems.lemsbatch.dto.ws.ResultGetEmergency;
import it.csi.lems.lemsbatch.dto.ws.ResultGetEmergency.Item;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert;
import it.csi.lems.lemsbatch.utils.Util;

public class ApiAlertsHelper {
	
	public static Capalerts xmlCapToCapalerts(Alert alert, int serviceId, Date today,
			HashMap<String, Integer> mapMsgType, HashMap<String, Integer> mapStatus,
			HashMap<String, Integer> mapScope) {
		Capalerts record = new Capalerts();
		record.setServiceid(serviceId);
		if (alert.getIdentifier() != null) {
			record.setIdentifier(alert.getIdentifier());
		} else {
			record.setIdentifier("");
		}
		if (alert.getSender() != null) {
			record.setSender(alert.getSender());
		} else {
			record.setSender("");
		}
		if (alert.getSent() != null) {
			record.setSent(Util.calendarToDate(alert.getSent()));
		} else {
			record.setSent(new Date());
		}
		if (alert.getSource() != null) {
			record.setSource(alert.getSource());
		} else {
			record.setSource("");
		}
		if (alert.getRestriction() != null) {
			record.setRestriction(alert.getRestriction());
		} else {
			record.setRestriction("");
		}
		if (alert.getAddresses() != null) {
			record.setAddress(alert.getAddresses());
		} else {
			record.setAddress("");
		}
		if (alert.getNote() != null) {
			record.setNote(alert.getNote());
		} else {
			record.setNote("");
		}
		if (alert.getReferences() != null) {
			record.setReferenza(alert.getReferences());
		} else {
			record.setReferenza("");
		}
		if (alert.getCode() != null) {
			record.setCode(Util.arrayToString(alert.getCode()));
		} else {
			record.setCode("");
		}
		if (alert.getIncidents() != null) {
			record.setIncidents(alert.getIncidents());
		} else {
			record.setIncidents("");
		}

		if (alert.getStatus() != null) {
			record.setStatus(checkDecode(mapStatus, alert.getStatus()));
		} else {
			record.setStatus(0);
		}
		if (alert.getMsgType() != null) {
			record.setMsgtype(checkDecode(mapMsgType, alert.getMsgType()));
		} else {
			record.setMsgtype(0);
		}
		if (alert.getScope() != null) {
			record.setScope(checkDecode(mapScope, alert.getScope()));
		} else {
			record.setScope(0);
		}
		record.setCreationtime(today);
		record.setIsdeleted(Boolean.FALSE);
		return record;
	}

	public static Capinfoalerts xmlCapToCapinfoalerts(Alert.Info info, long alertId, Date today,
			HashMap<String, Integer> mapUrgency, HashMap<String, Integer> mapSeverity,
			HashMap<String, Integer> mapCertainty) {
		Capinfoalerts record = new Capinfoalerts();
		record.setAlertid(alertId);
		record.setLanguage(info.getLanguage());
		record.setEvent(info.getEvent());
		record.setUrgency(checkDecode(mapUrgency, info.getUrgency()));
		record.setSeverity(checkDecode(mapSeverity, info.getSeverity()));
		record.setCertainty(checkDecode(mapCertainty, info.getCertainty()));
		record.setAudience(info.getAudience());
		record.setEffective(Util.calendarToDate(info.getEffective()));
		record.setOnset(Util.calendarToDate(info.getOnset()));
		record.setExpires(Util.calendarToDate(info.getExpires()));
		record.setSendername(info.getSenderName());
		record.setHeadline(info.getHeadline());
		record.setDescription(info.getDescription());
		record.setInstruction(info.getInstruction());
		record.setWeb(info.getWeb());
		record.setContact(info.getContact());
		record.setCreationtime(today);
		record.setIsdeleted(Boolean.FALSE);
		return record;
	}
	
	public static Capinfoalertparameters xmlCapToCapinfoalertparameters(Alert.Info.Parameter parameter, long infoalertid, Date today){
		Capinfoalertparameters record=new Capinfoalertparameters();
		record.setInfoalertid(infoalertid);
		record.setValue(parameter.getValue());
		record.setValuename(parameter.getValueName());
		record.setCreationtime(today);
		record.setIsdeleted(Boolean.FALSE);	    
		return record;
	}
	
	
	public static Capinfoalertareas xmlCapToCapinfoalertareas(Alert.Info.Area area, long infoalertid, Date today){
		Capinfoalertareas record=new Capinfoalertareas();
		record.setInfoalertid(infoalertid);
		record.setDescription(area.getAreaDesc());
		if (area.getAltitude()!=null)
			record.setAltitude(area.getAltitude());
		else record.setAltitude(BigDecimal.ONE);
		if (area.getCeiling()!=null)
			record.setCeiling(area.getCeiling());
		else record.setCeiling(BigDecimal.ONE);
		record.setPolygon(Util.arrayToString(area.getPolygon()));
		record.setCircle(Util.arrayToString(area.getCircle()));
		record.setCreationtime(today);
		record.setIsdeleted(Boolean.FALSE);	    
		return record;
	}
	
	public static Capinfoalertareageocodes xmlCapToCapinfoalertareageocodes(Alert.Info.Area.Geocode geocode, long infoalertareaid, Date today){
		Capinfoalertareageocodes geocodeRecord=new Capinfoalertareageocodes();
		geocodeRecord.setInfoalertareaid(infoalertareaid);
		geocodeRecord.setValue(geocode.getValue());
		geocodeRecord.setValuename(geocode.getValueName());
		geocodeRecord.setCreationtime(today);
		geocodeRecord.setIsdeleted(Boolean.FALSE);	    
		return geocodeRecord;
	}
	
	public static ArrayList<Relationalcategorymapping> xmlCapInfoAlertToRelationalCategoryMapping(Alert.Info alert, long idCapinfoAlert, Date today, HashMap<String, Integer> mapCategory) {
		ArrayList<Relationalcategorymapping> list = new ArrayList<>();
		Relationalcategorymapping record = null;
		if(alert.getCategory()!=null) {
		int size = alert.getCategory().size();
			for(int i=0; i<size; i++) {
				record = new Relationalcategorymapping();
				record.setIdcapinfoalert(idCapinfoAlert);
				record.setIdcategorymapping(mapCategory.get(alert.getCategory().get(i)));
				record.setCreationTime(today);
				record.setLastmodificationTime(today);
				record.setIsdeleted(Boolean.FALSE);
				record.setDeletionTime(null);
				list.add(record);
			}
		}
		return list;
		
	}
	
	public static ArrayList<Relationalresponsetypemapping> xmlCapInfoAlertToRelationalResponseTypeMapping(Alert.Info alert, long idCapInfoAlert, Date today, HashMap<String, Integer> mapResponseType) {
		ArrayList<Relationalresponsetypemapping> list = new ArrayList<>();
		Relationalresponsetypemapping record = null;
		if(alert.getResponseType()!=null) {
			int size = alert.getResponseType().size();
			for(int i=0; i<size; i++) {
				record = new Relationalresponsetypemapping();
				record.setIdcapinfoalert(idCapInfoAlert);
				record.setIdresponsetypemapping(mapResponseType.get(alert.getResponseType().get(i)));
				record.setCreationTime(today);
				record.setIsdeleted(Boolean.FALSE);
				list.add(record);
			}
		}		
		return list;
		
	}
	
	private static Integer checkDecode(HashMap<String, Integer> map, String key) {
		if(map==null || map.get(key) == null)
		{
			return -1;
		}
		return map.get(key);
	}
	
	public static List<Item> getOnlyOpenItems(ResultGetEmergency resultGetEmergency){
		List<Item> result=new ArrayList<Item>();
		if (resultGetEmergency!=null && resultGetEmergency.getResult()!=null && resultGetEmergency.getResult().getItems()!=null){
			for (Item item:resultGetEmergency.getResult().getItems()){
				if (item!=null && item.getEnd()==null)
					result.add(item);
			}
		}
		return result;
	}
}
