package it.csi.lems.lemsbatch.helper;

public class UkFloodHelper {
	
	public String Name;
	public String Meaning; 
	public String color;
	public String Warninglevel;
	public String CAPCertainty;
	public String CAPUrgency;
	public String Type;
	public String CAPCategory;
	public String CAPStatus;	
	private int level;
	
	public UkFloodHelper(String name, String meaning, String color, String warninglevel, String cAPCertainty,
			String cAPUrgency, String type, String capCategory, String capStatus, int level) {
		super();
		this.Name = name;
		this.Meaning = meaning;
		this.color = color;
		this.Warninglevel = warninglevel;
		this.CAPCertainty = cAPCertainty;
		this.CAPUrgency = cAPUrgency;
		this.Type = type;
		this.CAPCategory = capCategory;
		this.CAPStatus = capStatus;
		this.level = level;
	}

	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getMeaning() {
		return Meaning;
	}
	public void setMeaning(String meaning) {
		Meaning = meaning;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getWarninglevel() {
		return Warninglevel;
	}
	public void setWarninglevel(String warninglevel) {
		Warninglevel = warninglevel;
	}
	public String getCAPCertainty() {
		return CAPCertainty;
	}
	public void setCAPCertainty(String CAPCertainty) {
		this.CAPCertainty = CAPCertainty;
	}
	public String getCAPUrgency() {
		return CAPUrgency;
	}
	public void setCAPUrgency(String CAPUrgency) {
		this.CAPUrgency = CAPUrgency;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		this.Type = type;
	}
	public String getCAPCategory() {
		return CAPCategory;
	}
	public void setCAPCategory(String CAPCategory) {
		this.CAPCategory = CAPCategory;
	}
	public String getCAPStatus() {
		return CAPStatus;
	}
	public void setCAPStatus(String CAPStatus) {
		this.CAPStatus = CAPStatus;
	}
	
	public UkFloodHelper getValuesFromFloodLevel(long longLevel) {
		UkFloodHelper uk = new UkFloodHelper("", "", "", "", "", "", ""	, "", "", 4);
		int level = (int) (long) longLevel;
		switch (level) {
		case 1:
			uk.setName("Severe Flood Warning");
			uk.setMeaning("Severe flooding, danger to life.");
			uk.setWarninglevel("Extreme");
			uk.setColor("Red");
			uk.setCAPCertainty("Observed");
			uk.setCAPUrgency("Immediate");
			uk.setType("Warning");
			uk.setCAPCategory("Met");
			uk.setCAPStatus("Actual");
			uk.setLevel(level);
			break;
		case 2:
			uk.setName("Flood Warning");
			uk.setMeaning("Flooding is Expected, Immediate Action Required.");
			uk.setWarninglevel("Severe");
			uk.setColor("Orange");
			uk.setCAPCertainty("Likely");
			uk.setCAPUrgency("Immediate");
			uk.setType("Alert");
			uk.setCAPCategory("Met");
			uk.setCAPStatus("Actual");
			uk.setLevel(level);

			break;
		case 3:
			uk.setName("Flood Alert");
			uk.setMeaning("Flooding is Possible, Be Prepared.");
			uk.setWarninglevel("Moderate");
			uk.setColor("Yellow");
			uk.setCAPCertainty("Possible");
			uk.setCAPUrgency("Expected");
			uk.setType("Alert");
			uk.setCAPCategory("Met");
			uk.setCAPStatus("Actual");
			uk.setLevel(level);

			break;
		case 4:
			uk.setName("Warning no longer in force");
			uk.setMeaning("The warning is no longer in force.");
			uk.setWarninglevel(null);
			uk.setColor(null);
			uk.setCAPCertainty(null);
			uk.setCAPUrgency(null);
			uk.setType(null);
			uk.setCAPCategory("Met");
			uk.setCAPStatus("Actual");
			uk.setLevel(level);

			break;
		default:
			break;
		}
		
		return uk; 
	}
	
}
