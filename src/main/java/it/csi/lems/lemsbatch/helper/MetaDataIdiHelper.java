package it.csi.lems.lemsbatch.helper;

import java.util.HashMap;

import it.csi.lems.lemsbatch.dto.mybatis.Services;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;


public class MetaDataIdiHelper {
	
	public static HashMap<String, String> createIdiMetaData(Services service){
		HashMap<String, String> metadata = new HashMap<>();				
		metadata.put(LemsWebConstant.METADATA_TASK, ""+(service.getIdtask()));
		metadata.put("geographic_bounding_boxes", service.getServiceboundbox());
		metadata.put("identification_resourcetype", service.getIdentificationResourcetype());
		metadata.put("classification_topiccategory", service.getClassificationTopiccategory());		
		metadata.put("classification_spatialdataservicetype", service.getClassificationSpatialdataservicetype());
		metadata.put("qualityandvalidity_spatialresolution_measureunit", service.getQualityandvaliditySpatialresolutionMeasureunit());		
		metadata.put("qualityandvalidity_spatialresolution_longitude", service.getQualityandvaliditySpatialresolutionLongitude());		
		metadata.put("qualityandvalidity_spatialresolution_latitude", service.getQualityandvaliditySpatialresolutionLatitude());		
		metadata.put("qualityandvalidity_spatialresolution_scale", service.getQualityandvaliditySpatialresolutionScale());	
		metadata.put("conformity_degree", service.getConformityDegree());
		metadata.put("responsibleorganization_responsiblepartyrole", service.getResponsibleorganizationResponsiblepartyrole());		
		metadata.put("responsibleorganization_responsibleparty_organizationname", service.getProvider());		
		metadata.put("responsibleorganization_responsibleparty_email", service.getResponsibleorganizationResponsiblepartyEmail());	
		metadata.put("metadataonmetadata_date", service.getMetadataonmetadataDate());		
		metadata.put("identification_resourcetitle", service.getDescription());		
		metadata.put("identification_resourceabstract", service.getIdentificationResourceabstract());
		metadata.put("keyword_keywordvalue", service.getKeywordKeywordvalue());
		metadata.put("identification_resourcelanguage", service.getIdentificationResourcelanguage());		
		metadata.put("coordinatesystemreference_code", service.getCoordinatesystemreferenceCode());	
		metadata.put("coordinatesystemreference_codespace", service.getCoordinatesystemreferenceCodespace());		
		metadata.put("metadataonmetadata_pointofcontact_organizationname", service.getMetadataonmetadataPointofcontactOrganizationname());		
		metadata.put("metadataonmetadata_pointofcontact_email", service.getMetadataonmetadataPointofcontactEmail());		
		metadata.put("metadataonmetadata_language", service.getMetadataonmetadataLanguage());
		metadata.put("keyword_originatingcontrolledvocabulary", service.getKeywordOriginatingcontrolledvocabulary());		
		metadata.put("conformity_specification",service.getConformitySpecification());		
		return metadata;
	}
	
	
	
}
