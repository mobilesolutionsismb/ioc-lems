package it.csi.lems.lemsbatch.helper;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import it.csi.lems.lemsbatch.utils.LemsWebConstant;

public class MailHelper {

	private static final String MAIL_FROM = LemsWebConstant.MAILFROM;
	private static final String MAIL_TO[] = { LemsWebConstant.MAILTO };
	private static final String SMTPHOST = LemsWebConstant.SMTPHOST;
	private static final int PORT = Integer.parseInt(LemsWebConstant.PORT);
	private static final String username = LemsWebConstant.MAIL_USERNAME;
	private static final String password = LemsWebConstant.MAIL_PASSWORD;

	public static void postMail(String subject, String text) {
		try {
			Properties properties = new Properties();
			properties.put("mail.smtp.host", SMTPHOST);
			properties.put("mail.transport.protocol", "smtp");
			properties.put("mail.smtp.socketFactory.port", PORT);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");

			Session emailSession = Session.getInstance(properties, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			Message emailMessage = new MimeMessage(emailSession);
			int lengthTo = (MAIL_TO != null) ? MAIL_TO.length : 0;
			Address[] addressesTo = new Address[lengthTo];
			for (int i = 0; i < lengthTo; i++) {
				addressesTo[i] = new InternetAddress(MAIL_TO[i]);
			}

			emailMessage.setRecipients(Message.RecipientType.TO, addressesTo);

			emailMessage.setFrom(new InternetAddress(MAIL_FROM));
			emailMessage.setSubject(subject);
			emailMessage.setText(text);

			emailSession.setDebug(Boolean.TRUE);

			Transport.send(emailMessage);

		} catch (AddressException e) {
		} catch (MessagingException e) {
		}
	}

}
