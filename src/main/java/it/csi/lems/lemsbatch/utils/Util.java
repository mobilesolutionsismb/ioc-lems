package it.csi.lems.lemsbatch.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class Util {
	
	
	public static String getStringTimeForIDI(XMLGregorianCalendar xmlGregCal){
		String stringDate = null;
		if(xmlGregCal != null){
			stringDate= xmlGregCal.normalize().toString();
		}
		return stringDate;
	}
	
	
	public static String getDateFileName(XMLGregorianCalendar xmlGregCal){
		String stringDate = null;
		if(xmlGregCal != null){
			DateFormat df = new SimpleDateFormat("ddMMyyyy");
			Date javaDate = xmlGregCal.toGregorianCalendar().getTime();
			stringDate = df.format(javaDate);
			}
		return stringDate;
	}
	
	
	public static Date getSysDate() {
		return  new Date (System.currentTimeMillis());
	}
	
	
	static public XMLGregorianCalendar dateToCalendar(Date date){
		XMLGregorianCalendar gCdate = null;
		if (date!=null)
		{
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			try {
				gCdate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
		}
		return gCdate;
	}
	
	static public Date calendarToDate(XMLGregorianCalendar calendar){
		Date date = null;
		if(calendar != null)
		{
			date = calendar.toGregorianCalendar().getTime();
		}
		return date;
	}
	
	static public String arrayToString(List<String> input){
		if (input!=null && input.size()>0){
			StringBuilder str=new StringBuilder();
			boolean first=Boolean.TRUE;
			for(String s:input){
				if (s!=null){
					if (first)	first=Boolean.FALSE;
					else str.append(";"); 
					str.append(s);
				}
			}
			return str.toString();
		}
		return null;
	}
	
	static public String transformPropertyGeojson(String input){
		if (input!=null){
			return input.toLowerCase().replace(' ', '_').replace('-', '_');
		} else return null;
	}
	
	public static HashMap<String, Object> transformProperties(HashMap<String, Object> properties){
		HashMap<String, Object> newProperties=new HashMap<>();		
		for ( String key : properties.keySet() ) {
			if (LemsWebConstant.KEY_TO_EXCLUDE.get(key)==null)
				newProperties.put(Util.transformPropertyGeojson(key), properties.get(key));
		}
		return newProperties;
	}

	
	public static LocalDateTime stringToDateTime(String str){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		return LocalDateTime.parse(str,formatter);
	}
	
		
	
	public static LocalDateTime isoDateTimeStringToDateTime(String str){
		return LocalDateTime.parse(str,DateTimeFormatter.ISO_DATE_TIME);
	}
	
	
	public static String getLeadTimeFromDate(LocalDateTime date){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm'Z'");
		return date.format(formatter);
	}
	
	
	public static boolean isEmpty(String str){
		if (str==null || "".equals(str.trim())) return Boolean.TRUE;
		return Boolean.FALSE;
	}	
	
	public static String stringPlusDayToDateTimePrint(XMLGregorianCalendar xml,int days){
		LocalDateTime date=LocalDateTime.ofInstant(Instant.ofEpochMilli(xml.toGregorianCalendar().getTimeInMillis()), ZoneId.systemDefault());		
		return (date.plusDays(days)).toString();
	}
	
	
	public static String getStackTrace(Throwable aThrowable) {
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    aThrowable.printStackTrace(printWriter);
	    return result.toString();
	  }
	
	public static boolean isToday(XMLGregorianCalendar xml){
		LocalDate date=LocalDateTime.ofInstant(Instant.ofEpochMilli(xml.toGregorianCalendar().getTimeInMillis()), ZoneId.systemDefault()).toLocalDate();
		LocalDate today=LocalDate.now();
		return date.isEqual(today)?Boolean.TRUE:Boolean.FALSE; 
	}
	
	public static boolean isTomorrow(XMLGregorianCalendar xml){
		LocalDate date=LocalDateTime.ofInstant(Instant.ofEpochMilli(xml.toGregorianCalendar().getTimeInMillis()), ZoneId.systemDefault()).toLocalDate();
		LocalDate tomorrow=LocalDate.now().plusDays(1);
		return date.isEqual(tomorrow)?Boolean.TRUE:Boolean.FALSE; 
	}
	
	public static boolean isAfterTomorrow(XMLGregorianCalendar xml){
		LocalDate date=LocalDateTime.ofInstant(Instant.ofEpochMilli(xml.toGregorianCalendar().getTimeInMillis()), ZoneId.systemDefault()).toLocalDate();
		LocalDate tomorrow=LocalDate.now().plusDays((long) 1);
		return date.isAfter(tomorrow)?Boolean.TRUE:Boolean.FALSE; 
	}
	
	public static void main(String[] str){
	}
}
