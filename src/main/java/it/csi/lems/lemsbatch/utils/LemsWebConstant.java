package it.csi.lems.lemsbatch.utils;

import java.util.HashMap;
import java.util.ResourceBundle;

public class LemsWebConstant {
	
	private static ResourceBundle rb;
	
	static {				
		rb = ResourceBundle.getBundle("properties/constant");
	}
	
	//MAIL HELPER COORDINATES
	public static final String MAILFROM 				= rb.getString("mailfrom").trim();
	public static final String MAILTO 					= rb.getString("mailto").trim();
	public static final String SMTPHOST 				= rb.getString("smtphost").trim();
	public static final String PORT 					= rb.getString("port").trim();
	public static final String MAIL_USERNAME 			= rb.getString("usernamemail").trim();
	public static final String MAIL_PASSWORD 			= rb.getString("passwordmail").trim();
	
	//CLASSNAMES
	public static final String CLASSNAME_AIPO 			= rb.getString("classnameaipo").trim();
	public static final String CLASSNAME_ARPA_XML 		= rb.getString("classnamearpaxml").trim();
	public static final String CLASSNAME_FMI_XML 		= rb.getString("classnamefmixml").trim();
	public static final String CLASSNAME_XML_CAP 		= rb.getString("classnamexmlcap").trim();
	public static final String CLASSNAME_CATALUNYA 		= rb.getString("classnamecatalunya").trim();
	public static final String CLASSNAME_UK_FLOOD 		= rb.getString("classnameukflood").trim();
	public static final String CLASSNAME_UK_STATEMENT	= rb.getString("classnameukstetement").trim();
	public static final String CLASSNAME_SAVA			= rb.getString("classnamesava").trim();

	
	//LOCAL FILES
	public static final String SAVA_LOCAL_FILE			= rb.getString("savalocalfile").trim();
	
	//TITLES
	public static final String TITLEMETEOALARM 			= rb.getString("titlemeteoalarm").trim();
	public static final String SOURCEOFINFOMETEOALARM 	= rb.getString("sourceofinfometeoalarm").trim();
	
	//FMI PARAMETERS
	public static final String NOLONGERINFORCE 			= rb.getString("nolongerinforce").trim();
	public static final boolean FILTERFMI 				= Boolean.TRUE;
	public static final boolean FILTERFMIPAST 			= Boolean.TRUE;
	public static final boolean FILTERFMI1H 			= Boolean.FALSE;
	public static final boolean FILTERFMIEXPONSET 		= Boolean.TRUE;	
	public static final long HOUR						= 3600000;
	
	//EXTERNAL METADATA
	public static final String METADATA_TASK  			= rb.getString("metadatatask").trim();
	
	/******* TASK START *******/
	//TASK ARPA START
	public static final int TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN	=3401;
	public static final int TASK_ARPA_STORM_ALERT						=3402;
	public static final int TASK_ARPA_HYDROMETRIC_OVERTOP_ALERT			=3403;
	//TASK ARPA END
	
	//TASK UK START
	public static final int TASK_UK_FLOOD_WARNING_AND_ALERT				=3431;
	public static final int TASK_UK_5_DAY_BULLETIN						=3432;
	public static final int TASK_UK_MEASUREMENTS_WATER_LEVELS			=3433;
	//TASK UK END
	
	//TASK AIPO START
	public static final int TASK_AIPO_HYDROMETRIC_LEVELS				=3451;
	//TASK AIPO END
	
	//TASK FMI START
	public static final int TASK_FMI_WIND_ALERT							=3411; 
	public static final int TASK_FMI_SNOW_ICE_ALERT						=3412; 
	public static final int TASK_FMI_THUNDERSTORMS_ALERT				=3413; 
	public static final int TASK_FMI_HIGH_TEMPERATURE_ALERT				=3414; 
	public static final int TASK_FMI_LOW_TEMPERATURE_ALERT				=3415; 
	public static final int TASK_FMI_FOREST_FIRE_ALERT					=3416; 
	public static final int TASK_FMI_RAIN_ALERT							=3417; 
	//TASK FMI END
	
	//TASK CATALUNYA START
	public static final int TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT			=3421;
	public static final int TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT		=3422;
	public static final int TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT					=3423;
	public static final int TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT					=3424;
	public static final int TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT			=3425;
	public static final int TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT				=3426;
	public static final int TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT				=3427;
	//TASK CATALUNYA END
	
	//TASK SAVA START
	public static final int TASK_SAVA_ALERT_BULLETIN					=3442;
	//TASK SAVA END
	
	/******* TASK END *******/
	
	//SAVA PARAMETERS
	public static final String SAVA_REGULAR_WATER 				= rb.getString("savaregularwater").trim();
	public static final String SAVA_MINOR_FLOODS 				= rb.getString("savaminorfloods").trim();
	public static final String SAVA_FLOODS 						= rb.getString("savafloods").trim();
	public static final String SAVA_SEVERE_FLOODS 				= rb.getString("savaseverefloods").trim();
	
	//ARPA PARAMETERS
	public static final String ARPA_NO_WARNINGS 				= rb.getString("arpanowarnings").trim();
	public static final String ARPA_NO_VALUES	 				= rb.getString("arpanovalues").trim();
	
	
	//RESOURCE START
	public static final String RESOURCE_ARPA_ALERT				= rb.getString("resourcerpaalert").trim();
	public static final String RESOURCE_ARPA_HYDROMETRIC_ALERT	= rb.getString("resourcearpahydro").trim();  
	public static final String RESOURCE_FMI_LAND_ALERT			= rb.getString("resourcefmiland").trim();
	public static final String RESOURCE_FMI_SEA_ALERT			= rb.getString("resourcefmisea").trim();
	public static final String RESOURCE_UK_5_DAY_BULLETIN_ALERT	= rb.getString("resourceuk").trim();
	public static final String RESOURCE_CATALUNYA_ALERT			= rb.getString("resourcecatalunya").trim();
	public static final String RESOURCE_CATALUNYA_COASTAL_ALERT	= rb.getString("resourcecatalunyacoastal").trim();
	public static final String RESOURCE_SAVA_ALERT				= rb.getString("resourcesava").trim(); 
	public static final String RESOURCE_EUROPE_ALERT			= rb.getString("resourceeurope").trim();
	//RESOURCE END
	
	//COLOR GEOJSON SHAPE START
	public static final String COLOR_GREEN						= rb.getString("colorgreen").trim(); 
	public static final String COLOR_YELLOW						= rb.getString("coloryellow").trim(); 
	public static final String COLOR_ORANGE						= rb.getString("colororange").trim(); 
	public static final String COLOR_RED						= rb.getString("colorred").trim(); 
	//COLOR GEOJSON SHAPE END
	
	//NAME FOR MAPPING FMI IREATTASK START
	public static final String FMI_WIND_ALERT					= "1; WIND";  
	public static final String FMI_SNOW_ICE_ALERT				= "2; SNOW-ICE";  
	public static final String FMI_THUNDERSTORMS_ALERT			= "3; THUNDERSTORM"; 
	public static final String FMI_HIGH_TEMPERATURE_ALERT		= "5; HIGH-TEMPERATURE"; 
	public static final String FMI_LOW_TEMPERATURE_ALERT		= "6; LOW-TEMPERATURE";  
	public static final String FMI_FOREST_FIRE_ALERT			= "8; FOREST-FIRE"; 
	public static final String FMI_RAIN_ALERT					= "10; RAIN";  
	//NAME FOR MAPPING FMI IREATTASK END
	
	//NAME FOR MAPPING FMI CATALUCNIA START
	public static final String CATALUNYA_RAIN_ACCUMULATION_ALERT		="1"; 
	public static final String CATALUNYA_RAIN_INTENSITY_ALERT			="2"; 
	public static final String CATALUNYA_WIND_ALERT						="3"; 
	public static final String CATALUNYA_HEAT_WAVE_ALERT				="4"; 
	public static final String CATALUNYA_COLD_WAVE_ALERT				="5"; 
	public static final String CATALUNYA_STORM_SURGES_ALERT				="6"; 
	public static final String CATALUNYA_SNOW_ALERT						="7"; 
	//NAME FOR MAPPING FMI CATALUNIA END

	/* FILE NAME FOR IDI START*/
	public static final String ARPA_ALERT_FILE_NAME													= rb.getString("arpaalertfilename").trim();  
	public static final String SAVA_FILE_NAME														= rb.getString("savafilename").trim();  
	public static final String UK_ALERT_STATEMENTS_FILE_NAME										= rb.getString("ukalertstatementsfilename").trim();  
	public static final String FMI_ALERT_FILE_NAME_WIND												= rb.getString("fmifilenamewind").trim();  
	public static final String FMI_ALERT_FILE_NAME_SNOW_ICE											= rb.getString("fmifilenamesnow").trim();  
	public static final String FMI_ALERT_FILE_NAME_THUNDERSTORMS									= rb.getString("fmifilenamethunderstorm").trim();  
	public static final String FMI_ALERT_FILE_NAME_HIGH_TEMPERATURE									= rb.getString("fmifilenamehigh").trim();  
	public static final String FMI_ALERT_FILE_NAME_LOW_TEMPERATURE									= rb.getString("fmifilenamelow").trim();  
	public static final String FMI_ALERT_FILE_NAME_FOREST_FIRE										= rb.getString("fmifilenameforest").trim();  
	public static final String FMI_ALERT_FILE_NAME_RAIN												= rb.getString("fmifilenamerain").trim();  
	public static final String CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT_FILE_NAME		= rb.getString("catalunyarainintensityfilename").trim();  
	public static final String CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT_FILE_NAME	= rb.getString("catalunyarainaccumulationfilename").trim();  
	public static final String CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT_FILE_NAME				= rb.getString("catalunyasnowfilename").trim();  
	public static final String CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT_FILE_NAME				= rb.getString("catalunyawindfilename").trim();  
	public static final String CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT_FILE_NAME		= rb.getString("catalunyastormfilename").trim();  
	public static final String CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT_FILE_NAME			= rb.getString("catalunyacoldfilename").trim();  
	public static final String CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT_FILE_NAME			= rb.getString("catalunyaheatfilename").trim();  
	/* FILE NAME FOR IDI END*/
	
	/**
	 * URL and PASSWORD START
	 */
	public static final String ENDPOINT_WS_UK_FLOODS					= rb.getString("endpointwsukfloods").trim();
	public static final String ENDPOINT_WS_UK_STATEMENTS_ALERT			= rb.getString("endpointwsukstatements").trim();
	public static final String URL_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN	= rb.getString("endpointarpa").trim();
	public static final String URL_ARPA_HYDROMETRIC_ALERT_BULLETIN		= rb.getString("enpointarpahydro").trim();
	public static final String ENDPOINT_WS_CATALUNYA_ALTER				= rb.getString("endpointcatalunya").trim();
	public static final String URL_FMI_ALERT							= rb.getString("endpointfmi").trim();
	public static final String AIPO_FTP_URL								= rb.getString("aipoftpurl").trim();
	public static final String USER_AIPO								= rb.getString("aipouser").trim();
	public static final String PWD_AIPO 								= rb.getString("aipopwd").trim();	
	public static final String URL_IDI_INVIO							= rb.getString("url_idi_invio").trim();
	public static final String USER_IDI									= rb.getString("useridi").trim();
	public static final String PWD_IDI									= rb.getString("pwdidi").trim();	
	public static final String URL_API_TOKEN							= rb.getString("url_api_token").trim();
	public static final String URL_API									= rb.getString("url_api").trim(); 
	public static final String USER_API_TOKEN							= rb.getString("userapitoken").trim();
	public static final String PWD_API_TOKEN							= rb.getString("pwdapitoken").trim();	
	public static final String URL_API_V2_WEB_FIELD 					= rb.getString("urlapiwebfield").trim();	
	public static final String V2_URL_TAIL 								= rb.getString("urltail").trim();	
	/**
	 *  URL and PASSWORD END
	 */
	
	public static final String LANGUAGE_ENGLISH_FMI="en-GB";	
	public static HashMap<String,String> BULLETIN_HAZARD				=new HashMap<String,String>(); 	
	public static HashMap<String,String> KEY_TO_EXCLUDE					=new HashMap<String,String>(); 	
	public static HashMap<String,HashMap> NATIONS_REGION				=new HashMap<String,HashMap>(); 
	public static HashMap<String,String> IT_REGION						=new HashMap<String,String>(); 
	
	static{
		/*Arpa start*/
		BULLETIN_HAZARD.put(TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN+"_service", "ARPA Weather alert");
		BULLETIN_HAZARD.put(TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN+"_organizationName", "ARPA Piemonte");
		BULLETIN_HAZARD.put(TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN+"_country", "IT");
		/*Arpa end*/
		
		/*Sava start*/
		BULLETIN_HAZARD.put(TASK_SAVA_ALERT_BULLETIN+"_service", "Sava River Basin Flood Alert");
		BULLETIN_HAZARD.put(TASK_SAVA_ALERT_BULLETIN+"_hazardCode", "FL");
		BULLETIN_HAZARD.put(TASK_SAVA_ALERT_BULLETIN+"_organizationName", "Sava River Basin Commission");
		BULLETIN_HAZARD.put(TASK_SAVA_ALERT_BULLETIN+"_country", "SI");
		/*Sava end*/
		
		/*UK start*/
		BULLETIN_HAZARD.put(TASK_UK_5_DAY_BULLETIN+"_service", "UK Flood alert");
		BULLETIN_HAZARD.put(TASK_UK_5_DAY_BULLETIN+"_hazardCode", "FL");
		BULLETIN_HAZARD.put(TASK_UK_5_DAY_BULLETIN+"_hazardGLIDECode", "FL");
		BULLETIN_HAZARD.put(TASK_UK_5_DAY_BULLETIN+"_hazardName", "flood");
		BULLETIN_HAZARD.put(TASK_UK_5_DAY_BULLETIN+"_organizationName", "UK Met Office");
		BULLETIN_HAZARD.put(TASK_UK_5_DAY_BULLETIN+"_country", "GB");
		/*UK end*/
		
		/*FMI start*/
		BULLETIN_HAZARD.put(TASK_FMI_WIND_ALERT+"_service", "FMI Weather alert - Wind");
		BULLETIN_HAZARD.put(TASK_FMI_WIND_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_FMI_WIND_ALERT+"_hazardGLIDECode", "VW");
		BULLETIN_HAZARD.put(TASK_FMI_WIND_ALERT+"_hazardName", "wind");
		BULLETIN_HAZARD.put(TASK_FMI_WIND_ALERT+"_organizationName", "Finnish Meteorological Institute");
		BULLETIN_HAZARD.put(TASK_FMI_WIND_ALERT+"_country", "FI");
		
		BULLETIN_HAZARD.put(TASK_FMI_SNOW_ICE_ALERT+"_service", "FMI Weather alert - Snow-ice");
		BULLETIN_HAZARD.put(TASK_FMI_SNOW_ICE_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_FMI_SNOW_ICE_ALERT+"_hazardGLIDECode", "OT");
		BULLETIN_HAZARD.put(TASK_FMI_SNOW_ICE_ALERT+"_hazardName", "snow-ice");
		BULLETIN_HAZARD.put(TASK_FMI_SNOW_ICE_ALERT+"_organizationName", "Finnish Meteorological Institute");
		BULLETIN_HAZARD.put(TASK_FMI_SNOW_ICE_ALERT+"_country", "FI");
		
		BULLETIN_HAZARD.put(TASK_FMI_THUNDERSTORMS_ALERT+"_service", "FMI Weather alert - Thunderstorms");
		BULLETIN_HAZARD.put(TASK_FMI_THUNDERSTORMS_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_FMI_THUNDERSTORMS_ALERT+"_hazardGLIDECode", "ST");
		BULLETIN_HAZARD.put(TASK_FMI_THUNDERSTORMS_ALERT+"_hazardName", "thunderstorms");
		BULLETIN_HAZARD.put(TASK_FMI_THUNDERSTORMS_ALERT+"_organizationName", "Finnish Meteorological Institute");
		BULLETIN_HAZARD.put(TASK_FMI_THUNDERSTORMS_ALERT+"_country", "FI");
		
		BULLETIN_HAZARD.put(TASK_FMI_HIGH_TEMPERATURE_ALERT+"_service", "FMI Weather Alert - High-temperature");
		BULLETIN_HAZARD.put(TASK_FMI_HIGH_TEMPERATURE_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_FMI_HIGH_TEMPERATURE_ALERT+"_hazardGLIDECode", "HW");
		BULLETIN_HAZARD.put(TASK_FMI_HIGH_TEMPERATURE_ALERT+"_hazardName", "high-temperature");
		BULLETIN_HAZARD.put(TASK_FMI_HIGH_TEMPERATURE_ALERT+"_organizationName", "Finnish Meteorological Institute");
		BULLETIN_HAZARD.put(TASK_FMI_HIGH_TEMPERATURE_ALERT+"_country", "FI");
		
		BULLETIN_HAZARD.put(TASK_FMI_LOW_TEMPERATURE_ALERT+"_service", "FMI Weather Alert - Low-temperature");
		BULLETIN_HAZARD.put(TASK_FMI_LOW_TEMPERATURE_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_FMI_LOW_TEMPERATURE_ALERT+"_hazardGLIDECode", "CV");
		BULLETIN_HAZARD.put(TASK_FMI_LOW_TEMPERATURE_ALERT+"_hazardName", "low-temperature");
		BULLETIN_HAZARD.put(TASK_FMI_LOW_TEMPERATURE_ALERT+"_organizationName", "Finnish Meteorological Institute");
		BULLETIN_HAZARD.put(TASK_FMI_LOW_TEMPERATURE_ALERT+"_country", "FI");
		
		BULLETIN_HAZARD.put(TASK_FMI_FOREST_FIRE_ALERT+"_service", "FMI Weather Alert - Forest-fire");
		BULLETIN_HAZARD.put(TASK_FMI_FOREST_FIRE_ALERT+"_hazardCode", "FR");
		BULLETIN_HAZARD.put(TASK_FMI_FOREST_FIRE_ALERT+"_hazardGLIDECode", "WF");
		BULLETIN_HAZARD.put(TASK_FMI_FOREST_FIRE_ALERT+"_hazardName", "forest-fire");
		BULLETIN_HAZARD.put(TASK_FMI_FOREST_FIRE_ALERT+"_organizationName", "Finnish Meteorological Institute");
		BULLETIN_HAZARD.put(TASK_FMI_FOREST_FIRE_ALERT+"_country", "FI");
		
		BULLETIN_HAZARD.put(TASK_FMI_RAIN_ALERT+"_service", "FMI Weather Alert - Rain");
		BULLETIN_HAZARD.put(TASK_FMI_RAIN_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_FMI_RAIN_ALERT+"_hazardGLIDECode", "OT");
		BULLETIN_HAZARD.put(TASK_FMI_RAIN_ALERT+"_hazardName", "rain");
		BULLETIN_HAZARD.put(TASK_FMI_RAIN_ALERT+"_organizationName", "Finnish Meteorological Institute");
		BULLETIN_HAZARD.put(TASK_FMI_RAIN_ALERT+"_country", "FI");
		/*FMI end*/
		
		/* CATALUNYA start */
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT+"_service", "Catalunya Weather Danger Forecast - Rain intensity");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT+"_hazardGLIDECode", "OT");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT+"_hazardName", "rain intensity");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT+"_organizationName", "ProtecciÃ³ Civil de Catalunya");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT+"_country", "ES");
		
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT+"_service", "Catalunya Weather Danger Forecast - Rain accumulation");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT+"_hazardGLIDECode", "OT");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT+"_hazardName", "rain accumulation");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT+"_organizationName", "ProtecciÃ³ Civil de Catalunya");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT+"_country", "ES");
		
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT+"_service", "Catalunya Weather Danger Forecast - Snow");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT+"_hazardGLIDECode", "OT");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT+"_hazardName", "snow");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT+"_organizationName", "ProtecciÃ³ Civil de Catalunya");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT+"_country", "ES");
		
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT+"_service", "Catalunya Weather Danger Forecast - Wind");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT+"_hazardGLIDECode", "VW");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT+"_hazardName", "wind");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT+"_organizationName", "ProtecciÃ³ Civil de Catalunya");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT+"_country", "ES");
		
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT+"_service", "Catalunya Weather Danger Forecast - Storm surges");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT+"_hazardCode", "FL");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT+"_hazardGLIDECode", "SS");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT+"_hazardName", "storm surges");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT+"_organizationName", "ProtecciÃ³ Civil de Catalunya");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT+"_country", "ES");
		
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT+"_service", "Catalunya Weather Danger Forecast - Cold wave");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT+"_hazardGLIDECode", "CW");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT+"_hazardName", "cold wave");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT+"_organizationName", "ProtecciÃ³ Civil de Catalunya");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT+"_country", "ES");
		
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT+"_service", "Catalunya Weather Danger Forecast - Heat wave");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT+"_hazardCode", "EW");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT+"_hazardGLIDECode", "HT");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT+"_hazardName", "heat wave");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT+"_organizationName", "ProtecciÃ³ Civil de Catalunya");
		BULLETIN_HAZARD.put(TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT+"_country", "ES");
		/* CATALUNYA end */
		
		KEY_TO_EXCLUDE.put("area_cod", "area_cod");
		KEY_TO_EXCLUDE.put("area_name", "area_name");
			
		IT_REGION.put("IT001", "ITF6");//Calabria
		IT_REGION.put("IT003", "ITC4");//Lombardia
		IT_REGION.put("IT004", "ITC2");//Valle d'Aosta
		IT_REGION.put("IT005", "ITC1");//Piemonte
		IT_REGION.put("IT006", "ITH3");//Veneto
		IT_REGION.put("IT007", "ITC3");//Liguria
		IT_REGION.put("IT008", "ITH5");//Emilia Romagna
		IT_REGION.put("IT009", "ITI1");//Toscana
		IT_REGION.put("IT010", "ITI2");//Umbria
		IT_REGION.put("IT011", "ITI3");//Marche
		IT_REGION.put("IT012", "ITI4");//Lazio
		IT_REGION.put("IT013", "ITF1");//Abruzzo
		IT_REGION.put("IT016", "ITF3");//Campania
		IT_REGION.put("IT017", "ITF5");//Basilicata
		IT_REGION.put("IT018", "ITG1");//Sicilia
		IT_REGION.put("IT019", "ITG2");//Sardegna
		IT_REGION.put("IT020", "ITH4");//Fiuli-Venezia Giulia	
		
		NATIONS_REGION.put("IT", IT_REGION);
	}
	
	
}
