package it.csi.lems.lemsbatch.mybatis.mapper.ottimizzati;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalertareas;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalertparameters;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalerts;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmurl;
import it.csi.lems.lemsbatch.dto.mybatis.Relationalcategorymapping;
import it.csi.lems.lemsbatch.dto.mybatis.Relationalresponsetypemapping;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert.Info.Area.Geocode;

public interface XmlCapMapper {

	int insertRelationalResponseTypeMapping(@Param("elencoRelationalResponseTypeMapping") List<Relationalresponsetypemapping> elencoRelationalResponseTypeMapping);
	int insertRelationalCategoryMapping(@Param("elencoRelationalCategoryMapping") List<Relationalcategorymapping> elencoRelationalCategoryMapping);
	int insertCapInfoAlertAreaGeocodes(@Param("elencoGeocodes") List<Geocode> elencoGeocodes);
	int insertCapInfoAlertAreas(@Param("elencoAreas")ArrayList<Capinfoalertareas> list);
	int insertCapInfoAlerts(@Param("elencoCapInfoAlerts") ArrayList<Capinfoalerts> listCapInfoAlerts);
	int insertCapinfoalertparameters(@Param("elencoCapInfoParameters")ArrayList<Capinfoalertparameters> listCapinfoalertparameters);
	List<Meteoalarmurl> selectMeteoAlarm();
}
