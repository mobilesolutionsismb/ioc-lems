package it.csi.lems.lemsbatch.mybatis.mapper.generati;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import it.csi.lems.lemsbatch.dto.mybatis.Apimeasures;
import it.csi.lems.lemsbatch.dto.mybatis.ApimeasuresExample;

public interface ApimeasuresMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	long countByExample(ApimeasuresExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int deleteByExample(ApimeasuresExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int deleteByPrimaryKey(Long id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int insert(Apimeasures record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int insertSelective(Apimeasures record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	List<Apimeasures> selectByExample(ApimeasuresExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	Apimeasures selectByPrimaryKey(Long id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int updateByExampleSelective(@Param("record") Apimeasures record, @Param("example") ApimeasuresExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int updateByExample(@Param("record") Apimeasures record, @Param("example") ApimeasuresExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int updateByPrimaryKeySelective(Apimeasures record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ApiMeasures
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int updateByPrimaryKey(Apimeasures record);
}