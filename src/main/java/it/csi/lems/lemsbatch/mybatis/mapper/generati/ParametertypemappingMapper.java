package it.csi.lems.lemsbatch.mybatis.mapper.generati;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import it.csi.lems.lemsbatch.dto.mybatis.Parametertypemapping;
import it.csi.lems.lemsbatch.dto.mybatis.ParametertypemappingExample;

public interface ParametertypemappingMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	long countByExample(ParametertypemappingExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int deleteByExample(ParametertypemappingExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int deleteByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int insert(Parametertypemapping record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int insertSelective(Parametertypemapping record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	List<Parametertypemapping> selectByExample(ParametertypemappingExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	Parametertypemapping selectByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int updateByExampleSelective(@Param("record") Parametertypemapping record,
			@Param("example") ParametertypemappingExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int updateByExample(@Param("record") Parametertypemapping record,
			@Param("example") ParametertypemappingExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int updateByPrimaryKeySelective(Parametertypemapping record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.ParameterTypeMapping
	 * @mbg.generated  Tue Apr 10 09:23:18 CEST 2018
	 */
	int updateByPrimaryKey(Parametertypemapping record);
}