package it.csi.lems.lemsbatch.mybatis.mapper.generati;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import it.csi.lems.lemsbatch.dto.mybatis.Hazard;
import it.csi.lems.lemsbatch.dto.mybatis.HazardExample;
import it.csi.lems.lemsbatch.dto.mybatis.HazardKey;

public interface HazardMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    long countByExample(HazardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    int deleteByExample(HazardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    int deleteByPrimaryKey(HazardKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    int insert(Hazard record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    int insertSelective(Hazard record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    List<Hazard> selectByExample(HazardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    Hazard selectByPrimaryKey(HazardKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    int updateByExampleSelective(@Param("record") Hazard record, @Param("example") HazardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    int updateByExample(@Param("record") Hazard record, @Param("example") HazardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    int updateByPrimaryKeySelective(Hazard record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lems.Hazard
     *
     * @mbg.generated Tue Nov 13 17:02:59 CET 2018
     */
    int updateByPrimaryKey(Hazard record);
}