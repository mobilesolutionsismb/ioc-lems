package it.csi.lems.lemsbatch.mybatis.mapper.generati;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmactivealert;
import it.csi.lems.lemsbatch.dto.mybatis.MeteoalarmactivealertExample;

public interface MeteoalarmactivealertMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	long countByExample(MeteoalarmactivealertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	int deleteByExample(MeteoalarmactivealertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	int deleteByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	int insert(Meteoalarmactivealert record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	int insertSelective(Meteoalarmactivealert record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	List<Meteoalarmactivealert> selectByExample(MeteoalarmactivealertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	Meteoalarmactivealert selectByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	int updateByExampleSelective(@Param("record") Meteoalarmactivealert record,
			@Param("example") MeteoalarmactivealertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	int updateByExample(@Param("record") Meteoalarmactivealert record,
			@Param("example") MeteoalarmactivealertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	int updateByPrimaryKeySelective(Meteoalarmactivealert record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.MeteoAlarmActiveAlert
	 * @mbg.generated  Wed Nov 14 14:31:23 CET 2018
	 */
	int updateByPrimaryKey(Meteoalarmactivealert record);
}