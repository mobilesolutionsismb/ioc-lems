package it.csi.lems.lemsbatch.mybatis.mapper.generati;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import it.csi.lems.lemsbatch.dto.mybatis.Catalunyaalert;
import it.csi.lems.lemsbatch.dto.mybatis.CatalunyaalertExample;

public interface CatalunyaalertMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	long countByExample(CatalunyaalertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	int deleteByExample(CatalunyaalertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	int deleteByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	int insert(Catalunyaalert record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	int insertSelective(Catalunyaalert record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	List<Catalunyaalert> selectByExample(CatalunyaalertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	Catalunyaalert selectByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	int updateByExampleSelective(@Param("record") Catalunyaalert record,
			@Param("example") CatalunyaalertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	int updateByExample(@Param("record") Catalunyaalert record, @Param("example") CatalunyaalertExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	int updateByPrimaryKeySelective(Catalunyaalert record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table lems.CatalunyaAlert
	 * @mbg.generated  Fri Mar 15 10:50:21 CET 2019
	 */
	int updateByPrimaryKey(Catalunyaalert record);
}