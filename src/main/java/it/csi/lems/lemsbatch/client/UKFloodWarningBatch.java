package it.csi.lems.lemsbatch.client;

import it.csi.lems.lemsbatch.manager.UKFloodManager;

public class UKFloodWarningBatch {

	public static void main(String arg[]) {

		try {
			UKFloodManager floodManager = new UKFloodManager();
			floodManager.process();
		} catch (Exception e) {
		} finally {
			System.exit(0);
		}
	}

}
