package it.csi.lems.lemsbatch.client.alert;

import it.csi.lems.lemsbatch.manager.bulletin.xmlcap.ArpaXmlCapManager;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;

public class ArpaHydrometricOvertopAlertBatch {

	public static void main(String arg[]) {
		try {
			ArpaXmlCapManager xmlCapManager = new ArpaXmlCapManager();
			xmlCapManager.processBatch(LemsWebConstant.TASK_ARPA_HYDROMETRIC_OVERTOP_ALERT);
		} catch (Exception e) {
		} finally {
		}
	}
}
