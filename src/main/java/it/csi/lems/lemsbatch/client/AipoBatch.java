package it.csi.lems.lemsbatch.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import it.csi.lems.lemsbatch.utils.LemsWebConstant;

public class AipoBatch {

	final static String CLASSNAME = LemsWebConstant.CLASSNAME_AIPO;

	public static void main(String arg[]) {
		AipoBatch aipoBatch = new AipoBatch();
		aipoBatch.ftpDownload();
	}

	private void ftpDownload() {
		FTPClient ftp = null;
		try {
			ftp = new FTPClient();
			ftp.connect(LemsWebConstant.AIPO_FTP_URL);
			try {
				int reply = ftp.getReplyCode();
				if (!FTPReply.isPositiveCompletion(reply)) {
					String replyString = ftp.getReplyString();
					throw new Exception(CLASSNAME + "::Connect failed: " + replyString);
				}
				if (!ftp.login(LemsWebConstant.USER_AIPO, LemsWebConstant.PWD_AIPO)) {
					String replyString = ftp.getReplyString();
					throw new Exception(CLASSNAME + "::Login failed: " + replyString);
				}
				try {
					ftp.enterLocalPassiveMode();
					if (!ftp.setFileType(FTP.BINARY_FILE_TYPE)) {
					}
					transferFile(ftp);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (!ftp.logout()) {
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				ftp.disconnect();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void transferFile(FTPClient ftp) throws Exception {

		InputStream is = retrieveFileStream(ftp, "pontelagoscuro_17_10_24.dbf");
		is.close();

		if (!ftp.completePendingCommand()) {
			throw new Exception(CLASSNAME + "::Pending command failed: " + ftp.getReplyString());
		}

	}

	public static String read(InputStream input) throws IOException {
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
			return buffer.lines().collect(Collectors.joining("\n"));
		}
	}

	private InputStream retrieveFileStream(FTPClient ftp, String filePath) throws Exception {
		InputStream is = ftp.retrieveFileStream(filePath);
		int reply = ftp.getReplyCode();
		if (is == null || (!FTPReply.isPositivePreliminary(reply) && !FTPReply.isPositiveCompletion(reply))) {
			throw new Exception(ftp.getReplyString());
		}
		return is;
	}

}
