package it.csi.lems.lemsbatch.client.alert;

import it.csi.lems.lemsbatch.manager.UKStatementsAlertManager;

public class UKStatementsAlertBatch {

	public static void main(String arg[]) {

		try {
			UKStatementsAlertManager statementsManager = new UKStatementsAlertManager();
			statementsManager.process();
		} catch (Exception e) {
		} finally {
		}
	}

}
