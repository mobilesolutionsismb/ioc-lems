package it.csi.lems.lemsbatch.client.alert;

import it.csi.lems.lemsbatch.manager.bulletin.xmlcap.ArpaXmlCapManager;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;

public class ArpaHydrogeologicalAlertBatch {

	public static void main(String arg[]) {
		try {
			ArpaXmlCapManager xmlCapManager = new ArpaXmlCapManager();
			xmlCapManager.processBatch(LemsWebConstant.TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN);
		} catch (Exception e) {
		} finally {
		}
	}

}
