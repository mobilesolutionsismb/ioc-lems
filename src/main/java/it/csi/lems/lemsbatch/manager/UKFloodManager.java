package it.csi.lems.lemsbatch.manager;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.locationtech.jts.io.geojson.GeoJsonReader;
import org.locationtech.spatial4j.exception.InvalidShapeException;

import it.csi.lems.lemsbatch.dto.mybatis.Ukfloods;
import it.csi.lems.lemsbatch.dto.ws.CreateAlertAndWarning;
import it.csi.lems.lemsbatch.dto.ws.Result;
import it.csi.lems.lemsbatch.dto.ws.Token;
import it.csi.lems.lemsbatch.dto.ws.uk.FloodsAreaDto;
import it.csi.lems.lemsbatch.dto.ws.uk.FloodsDto;
import it.csi.lems.lemsbatch.dto.ws.uk.FloodsDto.Item;
import it.csi.lems.lemsbatch.dto.ws.uk.MultiPolygon;
import it.csi.lems.lemsbatch.dto.ws.uk.Polygon;
import it.csi.lems.lemsbatch.helper.MailHelper;
import it.csi.lems.lemsbatch.helper.UkFloodHelper;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;

public class UKFloodManager extends ApiManager {

	final static String CLASSNAME = LemsWebConstant.CLASSNAME_UK_FLOOD;
	final static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	

	public void insertDataDB(Object data, int serviceId, Date today) {

	}

	public void process() {
		try {
			Token token = getApiToken();
			if (token.isSuccess()) {
				HashMap<Integer, Integer> oldFloodMap=new HashMap<>();
				oldFloodMap=readOldFlood();
				HashMap<Integer, Ukfloods> oldFloodMapIdItem=new HashMap<>();				
				oldFloodMapIdItem=readOldFloodIdItem();				
				FloodsDto floods = callWsFlood();			
				if (floods != null && floods.getItems() != null && floods.getItems().length > 0) {
					String p = null;										
					HashMap<Integer, Integer> floodMap=new HashMap<Integer, Integer>();						
					for (int i = 0; i < floods.getItems().length; i++) {
						String polyString;						
						ObjectMapper mapper = new ObjectMapper();
						polyString = callWsFloodPolygon(floods.getItems()[i].getFloodArea().getPolygon());						
						if(polyString.contains("\"type\": \"Polygon\"")){
							Polygon polygon = new Polygon();
							polygon = mapper.readValue(polyString, Polygon.class);
							p = getWKTPolygon(polyString, polygon, getIdFlood(floods.getItems()[i].getId()));
						} else{
							MultiPolygon multipolygon = mapper.readValue(polyString, MultiPolygon.class);
							p = getWKT(polyString, multipolygon, getIdFlood(floods.getItems()[i].getId()));							
						}						
						FloodsAreaDto floodsAreaDto = callWsFloodArea(floods.getItems()[i].getFloodArea().getId());
						CreateAlertAndWarning alertAndWarning = new CreateAlertAndWarning();						
						alertAndWarning.setStart(floods.getItems()[i].getTimeRaised());						
						alertAndWarning.setEnd(null);
						int receiver[] = { 5 };
						alertAndWarning.setReceivers(receiver);
						alertAndWarning.setDescription(floods.getItems()[i].getMessage());
						alertAndWarning.setSourceOfInformation("UK Flood Warning & Alerts");
						alertAndWarning.setCommunicationStatus("Validated");
						alertAndWarning.setLevel(floods.getItems()[i].getSeverityLevel());
						alertAndWarning.setHazard("flood");
						int idFlood=getIdFlood(floods.getItems()[i].getId());
						alertAndWarning.setAreaOfInterest(p);							
						alertAndWarning.setId(idFlood);						
						StringBuilder point = new StringBuilder();
						point.append("POINT(").append(floodsAreaDto.getItems().getLongitude()).append(" ")
								.append(floodsAreaDto.getItems().getLat()).append(")");
						alertAndWarning.setLocation(point.toString());
						UkFloodHelper uk = new UkFloodHelper("", "", "", "", "", "", ""	, "", "", 4);
						uk = uk.getValuesFromFloodLevel(floods.getItems()[i].getSeverityLevel()); 
						alertAndWarning.setType(uk.getType());
						alertAndWarning.setCAPStatus(uk.getCAPStatus());
						alertAndWarning.setCAPCategory(uk.getCAPCategory());
						alertAndWarning.setCAPResponseType(null);
						alertAndWarning.setInstruction(null);
						alertAndWarning.setCAPUrgency(uk.getCAPUrgency());
						alertAndWarning.setWarningLevel(uk.getWarninglevel());					
						alertAndWarning.setCAPCertainty(uk.getCAPCertainty());						
						alertAndWarning.setCAPIdentifier(null);
						alertAndWarning.setInstruction(null);
						String code = floods.getItems()[i].getFloodAreaID();
						alertAndWarning.setWeb(LemsWebConstant.URL_API_V2_WEB_FIELD+code);
						alertAndWarning.setExtensionData(null);
						alertAndWarning.setResource(null);						
						setAlertAndWarning(alertAndWarning);							
						floodMap.put(idFlood, idFlood);																				
						if (oldFloodMap.get(idFlood)==null){							
							if(!LemsWebConstant.NOLONGERINFORCE.equals(floods.getItems()[i].getSeverity())) {								
								String result = sendCreateAlert(token,idFlood, alertAndWarning.getAreaOfInterest(), floods.getItems()[i], floodsAreaDto);
							}							
						} 						
						else {							
							if(LemsWebConstant.NOLONGERINFORCE.equals(floods.getItems()[i].getSeverity())) {								
								String result = updateAlert(token, ZonedDateTime.now(), oldFloodMap.get(idFlood),idFlood, true, null, floodsAreaDto);
							}else {
								
								Ukfloods oldUkfloods=oldFloodMapIdItem.get(oldFloodMap.get(idFlood));								
								Date newTimeRised=format.parse(floods.getItems()[i].getTimeRaised());
								Date newTimeMessageChanged=format.parse(floods.getItems()[i].getTimeMessageChanged());
								Date newTimeSeverityChanged=format.parse(floods.getItems()[i].getTimeSeverityChanged());								
								Integer oldServerityLevel=oldUkfloods.getSeveritylevel();								
								if (oldServerityLevel==null) oldServerityLevel=-1;								
								boolean update=false;
								
								if (newTimeRised.after(oldUkfloods.getTimeraised()))
									update=true;
								
								if (newTimeMessageChanged.after(oldUkfloods.getTimemessagechanged()))
									update=true;
								
								if (newTimeSeverityChanged.after(oldUkfloods.getTimeseveritychanged()))
									update=true;
								
								if (oldServerityLevel.longValue() != floods.getItems()[i].getSeverityLevel().longValue())
									update=true;
								
								if(update) {
									Ukfloods record=new Ukfloods();
									record.setId(oldFloodMap.get(idFlood));
									record.setIdlink(floods.getItems()[i].getIdLink());
									record.setDescription(floods.getItems()[i].getDescription());
									record.setEaareaname(floods.getItems()[i].getEaAreaName());
									record.setEaregionname(floods.getItems()[i].getEaRegionName());
									record.setFloodareaidurl(floods.getItems()[i].getFloodArea().getId());
									record.setFloodareacounty(floods.getItems()[i].getFloodArea().getCounty());
									record.setFloodareanotation(floods.getItems()[i].getFloodArea().getNotation());
									record.setFloodareariverorsea(floods.getItems()[i].getFloodArea().getRiverOrSea());
									record.setFloodareaid(floods.getItems()[i].getFloodAreaID());
									record.setIstidal(floods.getItems()[i].getIsTidal());
									record.setMessage(floods.getItems()[i].getMessage());
									record.setSeverity(floods.getItems()[i].getSeverity());
									record.setSeveritylevel((int) (long)floods.getItems()[i].getSeverityLevel());
									record.setTimemessagechanged(format.parse(floods.getItems()[i].getTimeMessageChanged()));
									record.setTimeraised(format.parse(floods.getItems()[i].getTimeRaised()));
									record.setTimeseveritychanged(format.parse(floods.getItems()[i].getTimeSeverityChanged()));			
									record.setCreationtime(Util.getSysDate());
									record.setIdflood(idFlood);
									record.setIsdeleted(false);
									String result = updateAlert(token, ZonedDateTime.now(), oldFloodMap.get(idFlood),idFlood, false, record, floodsAreaDto);
								}								
							}							
						}
					}
					ZonedDateTime now = ZonedDateTime.now();
					if(oldFloodMap!=null && oldFloodMap.size()>0){
						for (@SuppressWarnings("unused") Map.Entry<Integer, Integer> entry : oldFloodMap.entrySet())
						{ 						
							if(floodMap.get(entry.getKey())==null){
								CreateAlertAndWarning alertAndWarning = new CreateAlertAndWarning();
								alertAndWarning.setId(entry.getValue());
								alertAndWarning.setEnd(now.format(DateTimeFormatter.ISO_INSTANT));
								setAlertAndWarning(alertAndWarning);
								String result = updateAlert(token, now, entry.getValue(), entry.getKey(), true, null, null);
							}
						}
					}				
				}
				else{
					ZonedDateTime now = ZonedDateTime.now();
					if(oldFloodMap!=null && oldFloodMap.size()>0){
						for (@SuppressWarnings("unused") Map.Entry<Integer, Integer> entry : oldFloodMap.entrySet())
						{ 									
							CreateAlertAndWarning alertAndWarning = new CreateAlertAndWarning();
							alertAndWarning.setId(entry.getValue());
							alertAndWarning.setEnd(now.format(DateTimeFormatter.ISO_INSTANT));
							setAlertAndWarning(alertAndWarning);
							String result = updateAlert(token, now, entry.getValue(), entry.getKey(), true, null, null);			
						}
					}					
				}
			}
			else {
			}
		} catch (Exception e) {
		} finally {
			closeDBSession();
		}

	}
	
	

	private FloodsDto callWsFlood() {
		HttpGet httpGet = new HttpGet(LemsWebConstant.ENDPOINT_WS_UK_FLOODS);
		HttpClient client = HttpClientBuilder.create().build();
		client = HttpClientBuilder.create().build();		
		ObjectMapper mapper = new ObjectMapper();
		try {
			HttpResponse response = client.execute(httpGet);
			String body = EntityUtils.toString(response.getEntity(), "UTF-8");
			FloodsDto floods = mapper.readValue(body, FloodsDto.class);
			return floods;
		} catch (ClientProtocolException e) {
			return null;
		} catch (IOException e) {
			return null;
		} finally {
		}
	}
	
	
	private String callWsFloodPolygon(String url) {
		HttpGet httpGet = new HttpGet(url);
		HttpClient client = HttpClientBuilder.create().build();		
		try {
			client = HttpClientBuilder.create().build();
			HttpResponse response = client.execute(httpGet);
			String body = EntityUtils.toString(response.getEntity(), "UTF-8");
			return body;
		} catch (ClientProtocolException e) {
			return null;
		} catch (IOException e) {
			return null;
		} finally {
		}
	}

	private FloodsAreaDto callWsFloodArea(String url) {
		HttpGet httpGet = new HttpGet(url);
		HttpClient client = HttpClientBuilder.create().build();
		ObjectMapper mapper = new ObjectMapper();
		try {
			client = HttpClientBuilder.create().build();			
			HttpResponse response = client.execute(httpGet);
			String body = EntityUtils.toString(response.getEntity(), "UTF-8");
			FloodsAreaDto floodsAreaDto = mapper.readValue(body, FloodsAreaDto.class);
			return floodsAreaDto;
		} catch (ClientProtocolException e) {
			return null;
		} catch (IOException e) {
			return null;
		} finally {
		}
	}
	 	
	
	private String getWKTPolygon(String geojson, Polygon polygon, int id) throws InvalidShapeException, IOException, java.text.ParseException {
		ObjectMapper mapper = new ObjectMapper();
		GeoJsonReader reader = new GeoJsonReader(new org.locationtech.jts.geom.GeometryFactory());
		org.locationtech.jts.geom.Geometry s ;
		StringBuilder stringBuilderWkt = new StringBuilder();
		try {
			for(int i=0; i<polygon.getFeatures().length; i++) {
				if(i!=0) {
					stringBuilderWkt.append("\n");
				}
				s = reader.read(mapper.writeValueAsString(polygon.getFeatures()[i].getGeometry()));				
				org.locationtech.jts.io.WKTWriter w = new org.locationtech.jts.io.WKTWriter();				
				s.normalize();
				String wkt=null;
				wkt = w.write(s);				
				stringBuilderWkt.append(wkt);
			}
			return stringBuilderWkt.toString();
		} catch (org.locationtech.jts.io.ParseException e) {
			return null;
		}
	   
	}
	
	
	private String getWKT(String geojson, MultiPolygon polygon, int id) throws InvalidShapeException, IOException, java.text.ParseException {
		ObjectMapper mapper = new ObjectMapper();
		GeoJsonReader reader = new GeoJsonReader(new org.locationtech.jts.geom.GeometryFactory());
		org.locationtech.jts.geom.Geometry s ;
		StringBuilder stringBuilderWkt = new StringBuilder();
		try {
			for(int i=0; i<polygon.getFeatures().length; i++) {
				if(i!=0) {
					stringBuilderWkt.append("\n");
				}
				s = reader.read(mapper.writeValueAsString(polygon.getFeatures()[i].getGeometry()));				
				org.locationtech.jts.io.WKTWriter  w= new org.locationtech.jts.io.WKTWriter();
				s.normalize();
				String wkt=null;
				wkt = w.write(s);				
				stringBuilderWkt.append(wkt);
			}			
			return stringBuilderWkt.toString();
		} catch (org.locationtech.jts.io.ParseException e) {
			return null;
		}	   
	}
	
	
	private int getIdFlood(String idStr){
		int indice=idStr.indexOf("floods");
		indice+=7;
		idStr=idStr.substring(indice,idStr.length());
		return Integer.parseInt(idStr);
	}
	
	@Override
	public void insertToDbUkFlood(Result result,int idflood, Item item){
		String mailSubject = CLASSNAME+"::writeDBAndsendToIDI with idflood:"+idflood;
		try{
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");			
			Ukfloods record=new Ukfloods();
			record.setId(result.getId());
			record.setIdlink(item.getIdLink());
			record.setDescription(item.getDescription());
			record.setEaareaname(item.getEaAreaName());
			record.setEaregionname(item.getEaRegionName());
			record.setFloodareaidurl(item.getFloodArea().getId());
			record.setFloodareacounty(item.getFloodArea().getCounty());
			record.setFloodareanotation(item.getFloodArea().getNotation());
			record.setFloodareariverorsea(item.getFloodArea().getRiverOrSea());
			record.setFloodareaid(item.getFloodAreaID());
			record.setIstidal(item.getIsTidal());
			record.setMessage(item.getMessage());
			record.setSeverity(item.getSeverity());
			record.setSeveritylevel((int) (long)item.getSeverityLevel());
			record.setTimemessagechanged(format.parse(item.getTimeMessageChanged()));
			record.setTimeraised(format.parse(item.getTimeSeverityChanged()));
			record.setTimeseveritychanged(format.parse(item.getTimeSeverityChanged()));			
			record.setCreationtime(Util.getSysDate());
			record.setIdflood(idflood);
			record.setIsdeleted(false);
			getUkfloodsMapper().insertSelective(record);
			getSession().commit();
		} catch (Exception e) {
			getSession().rollback();
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} 
	}
	
	public HashMap<Integer, Integer> readOldFlood(){
		HashMap<Integer, Integer> floodMap=new HashMap<Integer, Integer>();
		try{
			List<Ukfloods> floods=getUkfloodsMapper().selectByExample(null);
			if (floods!=null){
				for (Ukfloods flood:floods){
					floodMap.put(flood.getIdflood(), flood.getId());
				}
			}
		} catch (Exception e) {
		} 
		return floodMap;
	}
	
	public HashMap<Integer, Ukfloods> readOldFloodIdItem(){
		HashMap<Integer, Ukfloods> floodMap=new HashMap<Integer, Ukfloods>();
		try{
			List<Ukfloods> floods=getUkfloodsMapper().selectByExample(null);
			if (floods!=null){
				for (Ukfloods flood:floods){
					floodMap.put(flood.getId(), flood);
				}
			}
		} catch (Exception e) {
		} 
		return floodMap;
	}
	
	

}
