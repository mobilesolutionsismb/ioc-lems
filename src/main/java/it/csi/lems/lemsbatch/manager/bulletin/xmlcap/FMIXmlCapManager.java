package it.csi.lems.lemsbatch.manager.bulletin.xmlcap;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.client.ClientProtocolException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import it.csi.lems.lemsbatch.dto.ws.uk.Geojson;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson.Feature;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert;
import it.csi.lems.lemsbatch.helper.MailHelper;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;


public class FMIXmlCapManager extends XmlCapManager{

	final static String CLASSNAME = LemsWebConstant.CLASSNAME_FMI_XML;
	
	List<Alert> alertList = new ArrayList<>();
	long minOnset = 10000000000000000L; 
	XMLGregorianCalendar minOnsetCalendar = null;
	long maxExpires = 0;
	XMLGregorianCalendar maxExpiresCalendar = null;
	int t = 0; 
		
	public void process() {
		String mailSubject = CLASSNAME+"::process";
		String hazardGlideName = "";
		Alert alert = null;
		String jsonInString = null;
		ZonedDateTime now = ZonedDateTime.now();			
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
		    Document doc;
		    InputStream url;			
			url = new URL(LemsWebConstant.URL_FMI_ALERT).openConnection().getInputStream();			
		    doc = db.parse(url);		    
		    NodeList node = (NodeList)doc.getElementsByTagName("alert");		    
		    if (node!=null && node.getLength()>0){		    	
		    	String[] leadTime=new String[1];
		    	String[] getOnset = new String[node.getLength()];
		    	String[] getExpires = new String[node.getLength()];			    	
			    JAXBContext jaxbContext = JAXBContext.newInstance(Alert.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();			    
				int i = 0;			
				HashMap<Integer,List<Alert.Info>> alertInfo=new HashMap<Integer,List<Alert.Info>>();				
				for (int j=0;j<node.getLength();j++){					
					JAXBElement<Alert> element =  jaxbUnmarshaller.unmarshal(node.item(j),Alert.class);					
					alert = element.getValue();		
					if (alert==null || alert.getInfo()==null) continue;										
					for (Alert.Info info:alert.getInfo()){									
						if (!LemsWebConstant.LANGUAGE_ENGLISH_FMI.equals(info.getLanguage())){
							continue;
						}						
						if(LemsWebConstant.FILTERFMIEXPONSET && (info.getExpires()==null || info.getOnset()==null)) 
						{
							if(info.getExpires()==null){
								continue;
							}else{
								if(info.getExpires()==null){
									continue;
								}
							}
							
						}
						
						if (info.getExpires()!=null && info.getOnset()!=null){
							long millSec=info.getExpires().toGregorianCalendar().getTimeInMillis() - info.getOnset().toGregorianCalendar().getTimeInMillis();
							
							if (LemsWebConstant.FILTERFMI1H && (millSec<=LemsWebConstant.HOUR)){
								continue;
							}							
							if(ExpiresBeforeNow(info.getExpires())){
								continue;
							}								
						}						
						if (LemsWebConstant.FILTERFMI && (Util.isAfterTomorrow(info.getExpires()))){
							continue;
						}						
						if(LemsWebConstant.FILTERFMIPAST && info.getUrgency().toLowerCase().equals("past")){
							continue;
						}				
						for (Alert.Info.Parameter param:info.getParameter()){							
							if ("awareness_type".equals(param.getValueName())){
								hazardGlideName = param.getValue();
								switch (param.getValue().toUpperCase()){
									case LemsWebConstant.FMI_WIND_ALERT:
										putInfo(alertInfo, info, LemsWebConstant.TASK_FMI_WIND_ALERT);
									break;
									case LemsWebConstant.FMI_SNOW_ICE_ALERT:
										putInfo(alertInfo, info, LemsWebConstant.TASK_FMI_SNOW_ICE_ALERT);
									break;
									case LemsWebConstant.FMI_THUNDERSTORMS_ALERT:
										putInfo(alertInfo, info, LemsWebConstant.TASK_FMI_THUNDERSTORMS_ALERT);
									break;
									case LemsWebConstant.FMI_HIGH_TEMPERATURE_ALERT:
										putInfo(alertInfo, info, LemsWebConstant.TASK_FMI_HIGH_TEMPERATURE_ALERT);
									break;
									case LemsWebConstant.FMI_LOW_TEMPERATURE_ALERT:
										putInfo(alertInfo, info, LemsWebConstant.TASK_FMI_LOW_TEMPERATURE_ALERT);
									break;
									case LemsWebConstant.FMI_FOREST_FIRE_ALERT:
										putInfo(alertInfo, info, LemsWebConstant.TASK_FMI_FOREST_FIRE_ALERT);
									break;
									case LemsWebConstant.FMI_RAIN_ALERT:
										putInfo(alertInfo, info, LemsWebConstant.TASK_FMI_RAIN_ALERT);
									break;
									default: 
										String mailText="IdTask not found: "+param.getValue()+" event: "+info.getEvent();
										MailHelper.postMail(mailSubject, mailText);
									break;
								}								
							}
						}
					}
				}				
				Map<Integer, List<Alert.Info>> globalMap = new HashMap<>();				
				if(alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_WIND_ALERT))!=null){
					List<Alert.Info> listaWind=alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_WIND_ALERT));
					globalMap.put(new Integer(LemsWebConstant.TASK_FMI_WIND_ALERT), listaWind);
				}
				if(alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_SNOW_ICE_ALERT))!=null){
					List<Alert.Info> listaSnowIce=alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_SNOW_ICE_ALERT));
					globalMap.put(new Integer(LemsWebConstant.TASK_FMI_SNOW_ICE_ALERT), listaSnowIce);
				}
				if(alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_THUNDERSTORMS_ALERT))!=null){
					List<Alert.Info> listaThundestorm=alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_THUNDERSTORMS_ALERT));
					globalMap.put(new Integer(LemsWebConstant.TASK_FMI_THUNDERSTORMS_ALERT), listaThundestorm);
				}
				if(alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_HIGH_TEMPERATURE_ALERT))!=null){
					List<Alert.Info> listaHighTemperature=alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_HIGH_TEMPERATURE_ALERT));
					globalMap.put(new Integer(LemsWebConstant.TASK_FMI_HIGH_TEMPERATURE_ALERT), listaHighTemperature);
				}
				if(alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_LOW_TEMPERATURE_ALERT))!=null){
					List<Alert.Info> listaLowTemperature=alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_LOW_TEMPERATURE_ALERT));
					globalMap.put(new Integer(LemsWebConstant.TASK_FMI_LOW_TEMPERATURE_ALERT), listaLowTemperature);
				}
				if(alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_FOREST_FIRE_ALERT))!=null){
					List<Alert.Info> listaForestFire=alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_FOREST_FIRE_ALERT));
					globalMap.put(new Integer(LemsWebConstant.TASK_FMI_FOREST_FIRE_ALERT), listaForestFire);
				}
				if(alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_RAIN_ALERT))!=null){
					List<Alert.Info> listaRain=alertInfo.get(new Integer(LemsWebConstant.TASK_FMI_RAIN_ALERT));
					globalMap.put(new Integer(LemsWebConstant.TASK_FMI_RAIN_ALERT), listaRain);
				}				
				ArrayList<String> emmaIdAlreadyColoured = new ArrayList<>();				
				for (Map.Entry<Integer, List<Alert.Info>> entry : globalMap.entrySet()) {	
					emmaIdAlreadyColoured = new ArrayList<>();					
					this.setIdTask(entry.getKey());					
					List<Alert.Info> listInUse=entry.getValue();					
					ArrayList<Feature> featuresList=new ArrayList<>();
					long infoGetExpiresEpoch = 0;
					long infoGetOnSetEpoch = 0;					
					Geojson jsonCap=new Geojson();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
					sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
					if(listInUse.get(0).getExpires()!=null){Date dateExpires = sdf.parse(listInUse.get(0).getExpires().toString());} 
					else{Date dateExpires = sdf.parse(sdf.format(new Date())); infoGetExpiresEpoch = dateExpires.getTime();}
					if(listInUse.get(0).getOnset()!=null){Date dateOnSet = sdf.parse(listInUse.get(0).getOnset().toString());}
					else{Date dateOnSet = sdf.parse(sdf.format(new Date())); infoGetOnSetEpoch = dateOnSet.getTime();}
					for (Alert.Info info:listInUse){
						HashMap <String, String> geoCodeMap=new HashMap<>();
						boolean emmaIdFound=false;					
						leadTime[i]=Util.getStringTimeForIDI(info.getExpires());
						getExpires[i]=Util.getStringTimeForIDI(info.getExpires());
						getOnset[i]=Util.getStringTimeForIDI(info.getOnset());
						setTemporalreferenceDateofcreation(now.format(DateTimeFormatter.ISO_INSTANT)); 
						setTemporalreferenceDateofpublication(now.format(DateTimeFormatter.ISO_INSTANT)); 
						setTemporalreferenceDateoflastrevision(now.format(DateTimeFormatter.ISO_INSTANT)); 						
						for (Alert.Info.Area area:info.getArea()){
							for (Alert.Info.Area.Geocode geocode:area.getGeocode()){
								geoCodeMap.put(geocode.getValue(), geocode.getValue());
								emmaIdFound=true;
							}
						}
						if (!emmaIdFound){							
							continue;
						}					
						if(entry.getKey()==3411){
							if (featuresList.isEmpty()){								
								jsonCap=getGeojson(LemsWebConstant.RESOURCE_FMI_SEA_ALERT);
								Feature[] features =jsonCap.getFeatures();								
								getOnlyAreaThahMatchWithEmmaId(emmaIdAlreadyColoured,features,geoCodeMap,featuresList,info,alert,false,true, entry.getKey(), hazardGlideName);
							} else {
							Feature[] features = featuresList.toArray(new Feature[0]);
							
							jsonCap.setType("FeatureCollection");							
							getOnlyAreaThahMatchWithEmmaId(emmaIdAlreadyColoured, features,geoCodeMap,featuresList,info,alert,true,false, entry.getKey(), hazardGlideName);							
													
							//***Aggiungo le features al geojson
							//jsonCap.setFeatures(featuresListGreenRegions.toArray(new Feature[0]));
							jsonCap.setFeatures(featuresList.toArray(new Feature[0]));
							}
						}
						if(entry.getKey()==3412 || entry.getKey()==3416){
							if (featuresList.isEmpty()){								
								jsonCap=getGeojson(LemsWebConstant.RESOURCE_FMI_LAND_ALERT);
								Feature[] features =jsonCap.getFeatures();								
								getOnlyAreaThahMatchWithEmmaId(emmaIdAlreadyColoured,features,geoCodeMap,featuresList,info,alert,false,true, entry.getKey(), hazardGlideName);
							} else {
							Feature[] features = featuresList.toArray(new Feature[0]);
							
							jsonCap.setType("FeatureCollection");							
							getOnlyAreaThahMatchWithEmmaId(emmaIdAlreadyColoured, features,geoCodeMap,featuresList,info,alert,true,false, entry.getKey(), hazardGlideName);							
													
							jsonCap.setFeatures(featuresList.toArray(new Feature[0]));
							}
						}
						if(entry.getKey()==3413 || entry.getKey()==3414 || entry.getKey()==3415 || entry.getKey()==3417){
							if (featuresList.isEmpty()){
								jsonCap=getGeojson(LemsWebConstant.RESOURCE_FMI_LAND_ALERT);
								Feature[] features =jsonCap.getFeatures();
								getOnlyAreaThahMatchWithEmmaId(emmaIdAlreadyColoured,features,geoCodeMap,featuresList,info,alert,false,true, entry.getKey(), hazardGlideName);
								jsonCap=getGeojson(LemsWebConstant.RESOURCE_FMI_SEA_ALERT);
								features =jsonCap.getFeatures();	
								getOnlyAreaThahMatchWithEmmaId(emmaIdAlreadyColoured, features,geoCodeMap,featuresList,info,alert,true,true, entry.getKey(), hazardGlideName);
								jsonCap.setFeatures(featuresList.toArray(new Feature[0]));
							} else {
								Feature[] features = featuresList.toArray(new Feature[0]);
								jsonCap.setType("FeatureCollection");							
								getOnlyAreaThahMatchWithEmmaId(emmaIdAlreadyColoured, features,geoCodeMap,featuresList,info,alert,true,false, entry.getKey(), hazardGlideName);							
								jsonCap.setFeatures(featuresList.toArray(new Feature[0]));
							}
						}
					}
					
					jsonInString = getGeojson(jsonCap);
					byte ptext[] = jsonInString.getBytes(StandardCharsets.UTF_8);  
					jsonInString = new String(ptext, StandardCharsets.UTF_8);
					BigDecimal diffTime = BigDecimal.ZERO;
					BigDecimal[] timeSpan = new BigDecimal[1];
					diffTime = new BigDecimal(infoGetExpiresEpoch-infoGetOnSetEpoch);
					diffTime = diffTime.divide(new BigDecimal(1000*3600),RoundingMode.HALF_UP);
					leadTime[0]=Util.getStringTimeForIDI(listInUse.get(0).getExpires());
					timeSpan[0]=diffTime;
					List<String> geojsonList=new ArrayList<>();
					geojsonList.add(jsonInString);
					this.setLeadTime(leadTime);
					this.setTimeSpan(timeSpan);
					setTemporalReferenceStart(Util.getStringTimeForIDI(listInUse.get(0).getOnset()));
					setTemporalReferenceEnd(Util.getStringTimeForIDI(listInUse.get(0).getExpires()));
					isfmi = true;
					String result=writeDBAndsendToIDI(alert, geojsonList, now);
				}
				
		    }
		    
		} catch (ClientProtocolException e) {
			String mailText="ClientProtocolException message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (IOException e) {
			String mailText="IOException message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (Exception e) {
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		}
		finally{
			closeDBSession();
		}
	}
	
	


	private void adjustGreenRegions(Feature f, Alert.Info info, Alert alert, int idTask, String hazardGlideName, String creationDate) {
		f.getProperties().put("service", LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_service"));
		f.getProperties().put("hazardCode", LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_hazardCode"));

		if (idTask != LemsWebConstant.TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN
				&& idTask != LemsWebConstant.TASK_SAVA_ALERT_BULLETIN) {
			f.getProperties().put("hazardGLIDECode",
					LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_hazardGLIDECode"));
			f.getProperties().put("hazardName", LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_hazardName"));
		} else {
			f.getProperties().put("hazardGLIDECode", "OT");
			f.getProperties().put("hazardName", hazardGlideName);		}

		f.getProperties().put("hazardLevel", 1);
		f.getProperties().put("hazardLevelDescription", "Minor");				
		f.getProperties().put("creationDate", creationDate);
		f.getProperties().put("country", LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_country"));
		f.getProperties().put("hazardLevel", 1);
		HashMap<String, Object> properties = f.getProperties();
		if (info.getExpires()!=null) {
			properties.put("expires",Util.getStringTimeForIDI(info.getExpires()));
		}else {
			properties.put("expires","");
		}			
		if(info.getCategory().size()!=0) {
			properties.put("category", String.join(";", info.getCategory()));
		}else {
			properties.put("category", "");
		}	
		if (info.getUrgency()!=null) {
			properties.put("urgency",info.getUrgency());
		}else {
			properties.put("urgency","");
		}
		properties.put("severity","Minor");
		
			
		if (info.getCertainty()!=null) {
			properties.put("certainty",info.getCertainty());
		}else {
			properties.put("certainty","");
		}	
		if (alert.getIdentifier()!=null) {
			properties.put("identifier",alert.getIdentifier());
		}else {
			properties.put("identifier","");
		}
		if(alert.getMsgType()!=null) {
			properties.put("msgType", alert.getMsgType());
		}else {
			properties.put("msgType", "");
		}									
		addGeneralProperties(f);		
	}


	private boolean ExpiresBeforeNow(XMLGregorianCalendar expires) throws DatatypeConfigurationException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date());
		long difference = expires.toGregorianCalendar().getTimeInMillis() - c.getTimeInMillis();
		if(difference>=0){ return false;}
		else {return true;}
	}
	
	private boolean getOnlyAreaThahMatchWithEmmaId(ArrayList<String> emmaIdAlreadyColoured, Feature[] features,HashMap <String, String> geoCodeMap, ArrayList<Feature> featuresList, Alert.Info info,
			Alert alert, boolean sea, boolean addList, int idTask, String hazardGlideName){
		for (int i=0;i<features.length;i++){
			HashMap<String, Object> properties=features[i].getProperties();
			String emmaId=(String)properties.get("area_cod");
						
			if(!emmaIdAlreadyColoured.contains(emmaId)){
				if (geoCodeMap.get(emmaId)!=null){
					emmaIdAlreadyColoured.add(emmaId);
					for (Alert.Info.Parameter param:info.getParameter()){
						properties.put(param.getValueName(), param.getValue());
						if ("awareness_level".equals(param.getValueName())){
								if (param.getValue().contains("green") ){
									this.setHazardLevel("1");
									this.setHazardLevelDescription("Minor");
									properties.put("fill", LemsWebConstant.COLOR_GREEN);
								} 
								if (param.getValue().contains("yellow") ){
									this.setHazardLevel("2");
									this.setHazardLevelDescription("Moderate");
									properties.put("fill", LemsWebConstant.COLOR_YELLOW);
								}
								if (param.getValue().contains("orange") ){
									this.setHazardLevel("3");
									this.setHazardLevelDescription("Severe");
									properties.put("fill", LemsWebConstant.COLOR_ORANGE);
								}
								if (param.getValue().contains("red") ){
									this.setHazardLevel("4");
									this.setHazardLevelDescription("Extreme");
									properties.put("fill", LemsWebConstant.COLOR_RED);
								}
						}
					}
					
					setDateStart(Util.getStringTimeForIDI(info.getEffective()));
					
					
					if (info.getExpires()!=null)
						setDateEnd(Util.getStringTimeForIDI(info.getExpires()));
					else
						setDateEnd(Util.stringPlusDayToDateTimePrint(info.getEffective(),5));

					setCreationDate(Util.getStringTimeForIDI(alert.getSent()));
					setDateLastRevision(Util.getStringTimeForIDI(alert.getSent()));
					addCommonProperties(features[i]);
					addXmlCapAndStyleProperties(features[i],properties,info,alert);
					if (addList)
						featuresList.add(features[i]);
				} else {
					this.setHazardLevel("1");
					this.setHazardLevelDescription("Minor");
					properties.put("fill", LemsWebConstant.COLOR_GREEN);
					adjustGreenRegions(features[i], info, alert, idTask, hazardGlideName, Util.getStringTimeForIDI(alert.getSent()));					
					if (addList)
						featuresList.add(features[i]);
				}
			
			}
				
		}
		return false;
	}

	
	
	
	@Override
	public void insertDataDB(Object data, int serviceId, Date today) {
		try {
			if (data != null) {
				super.insertDataDB(data, serviceId, today);
				super.commit();
			}	
		}catch(Exception e) {
			getSession().rollback();
			String mailSubject = CLASSNAME+"::insertDataDB";
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		}
	}
	
	private void putInfo(HashMap<Integer,List<Alert.Info>> mapTaskAlert, Alert.Info info,int idTask){
		
		List<Alert.Info> listaInfo=mapTaskAlert.get(new Integer(idTask));
		if (listaInfo==null)  {
			listaInfo=new ArrayList<Alert.Info>();
			mapTaskAlert.put(new Integer(idTask), listaInfo);
		}
		listaInfo.add(info);
	}

}
