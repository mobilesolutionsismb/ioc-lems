package it.csi.lems.lemsbatch.manager.bulletin.xmlcap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.http.client.ClientProtocolException;

import it.csi.lems.lemsbatch.dto.ws.uk.Geojson;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson.Feature;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;

public class SAVAXmlCapManager extends XmlCapManager {

	final static String CLASSNAME = LemsWebConstant.CLASSNAME_SAVA;
	final static String REGULAR_WATER = LemsWebConstant.SAVA_REGULAR_WATER;
	final static String MINOR_FLOODS = LemsWebConstant.SAVA_MINOR_FLOODS;
	final static String FLOODS = LemsWebConstant.SAVA_FLOODS;
	final static String SEVERE_FLOODS = LemsWebConstant.SAVA_SEVERE_FLOODS;

	public void process() {
		Alert alert = null;
		String jsonInString = null;
		try {
			ZonedDateTime now = ZonedDateTime.now();

			JAXBContext jaxbContext;
			jaxbContext = JAXBContext.newInstance(Alert.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			HashMap<String, Alert.Info> infoArea = new HashMap<>();
			File file = new File(LemsWebConstant.SAVA_LOCAL_FILE);

			FileInputStream fis = new FileInputStream(file);

			alert = (Alert) jaxbUnmarshaller.unmarshal(fis);

			for (Alert.Info info : alert.getInfo()) {
				String codArea = info.getArea().get(0).getAreaDesc();
				infoArea.put(codArea, info);
			}

			Geojson jsonCap = getGeojson(LemsWebConstant.RESOURCE_SAVA_ALERT);

			Feature[] features = jsonCap.getFeatures();

			ArrayList<Feature> featuresList = new ArrayList<>();

			for (int i = 0; i < features.length; i++) {
				HashMap<String, Object> properties = features[i].getProperties();
				String codArea = ((Integer) properties.get("HW_ID")).toString();

				if (codArea != null) {
					Alert.Info info = infoArea.get(codArea);
					if (info != null && info.getParameter() != null) {

						switch (info.getEvent().toUpperCase()) {
						case "REGULAR WATER CONDITIONS":
							this.setHazardLevel("1");
							this.setHazardLevelDescription(info.getEvent().toUpperCase());
							properties.put("fill", LemsWebConstant.COLOR_GREEN);
							break;
						case "MINOR FLOODS":
							this.setHazardLevel("2");
							this.setHazardLevelDescription(info.getEvent().toUpperCase());
							properties.put("fill", LemsWebConstant.COLOR_YELLOW);
							break;
						case "FLOODS":
							this.setHazardLevel("3");
							this.setHazardLevelDescription(info.getEvent().toUpperCase());
							properties.put("fill", LemsWebConstant.COLOR_ORANGE);
							break;
						case "EXTENSIVE, SEVERE FLOODS":
							this.setHazardLevel("4");
							this.setHazardLevelDescription(info.getEvent().toUpperCase());
							properties.put("fill", LemsWebConstant.COLOR_RED);
							break;

						}

						StringBuilder parameterUnexpected = new StringBuilder();
						for (Alert.Info.Parameter param : info.getParameter()) {

							properties.put(param.getValueName(), param.getValue());
						}

					}

					setIdTask(LemsWebConstant.TASK_SAVA_ALERT_BULLETIN);
					setDateStart(Util.getStringTimeForIDI(info.getOnset()));
					setDateEnd(Util.getStringTimeForIDI(info.getExpires()));
					setCreationDate(now.format(DateTimeFormatter.ISO_INSTANT));
					setDateLastRevision(now.format(DateTimeFormatter.ISO_INSTANT));

					setTemporalReferenceStart(Util.getStringTimeForIDI(info.getOnset()));
					setTemporalReferenceEnd(Util.getStringTimeForIDI(info.getExpires()));

					setTemporalreferenceDateofcreation(now.format(DateTimeFormatter.ISO_INSTANT));
					setTemporalreferenceDateofpublication(now.format(DateTimeFormatter.ISO_INSTANT));
					setTemporalreferenceDateoflastrevision(now.format(DateTimeFormatter.ISO_INSTANT));

					this.setHazardGLIDECode("FL");
					this.setHazardName("flood");

					String[] leadTime = new String[1];
					leadTime[0] = Util.getStringTimeForIDI(info.getExpires());

					this.setLeadTime(leadTime);

					addCommonProperties(features[i]);

					addXmlCapAndStyleProperties(features[i], properties, info, alert);

					featuresList.add(features[i]);
					;
				}
			}

			jsonCap.setFeatures(featuresList.toArray(new Feature[0]));

			jsonInString = getGeojson(jsonCap);

			List<String> geojsonList = new ArrayList<>();

			geojsonList.add(jsonInString);

			String result = writeDBAndsendToIDI(alert, geojsonList, now);

		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		} catch (Exception e) {
		} finally {
			closeDBSession();
		}
	}

}
