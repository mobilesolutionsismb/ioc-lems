package it.csi.lems.lemsbatch.manager;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.codehaus.jackson.map.ObjectMapper;

import it.csi.lems.lemsbatch.dto.mybatis.Anagraficastazioniarpa;
import it.csi.lems.lemsbatch.dto.mybatis.Arpaalert;
import it.csi.lems.lemsbatch.dto.mybatis.Catalunyaalert;
import it.csi.lems.lemsbatch.dto.mybatis.Catalunyaincomingjson;
import it.csi.lems.lemsbatch.dto.mybatis.Categorymapping;
import it.csi.lems.lemsbatch.dto.mybatis.Certaintymapping;
import it.csi.lems.lemsbatch.dto.mybatis.Fmialert;
import it.csi.lems.lemsbatch.dto.mybatis.Geojsonforidi;
import it.csi.lems.lemsbatch.dto.mybatis.Msgtypemapping;
import it.csi.lems.lemsbatch.dto.mybatis.MsgtypemappingExample;
import it.csi.lems.lemsbatch.dto.mybatis.Responsetypemapping;
import it.csi.lems.lemsbatch.dto.mybatis.Scopemapping;
import it.csi.lems.lemsbatch.dto.mybatis.Services;
import it.csi.lems.lemsbatch.dto.mybatis.ServicesExample;
import it.csi.lems.lemsbatch.dto.mybatis.Severitymapping;
import it.csi.lems.lemsbatch.dto.mybatis.Statusmapping;
import it.csi.lems.lemsbatch.dto.mybatis.Urgencymapping;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson.Feature;
import it.csi.lems.lemsbatch.exception.BadErrorNeedsEmailSendingException;
import it.csi.lems.lemsbatch.helper.MailHelper;
import it.csi.lems.lemsbatch.helper.MetaDataIdiHelper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.AnagraficastazioniarpaMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.ArpaalertMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CapalertsMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CapinfoalertareageocodesMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CapinfoalertareasMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CapinfoalertparametersMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CapinfoalertsMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CatalunyaalertMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CatalunyaincomingjsonMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CategorymappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.CertaintymappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.FmialertMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.GeojsonforidiMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.MsgtypemappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.RelationalcategorymappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.RelationalresponsetypemappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.ResponsetypemappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.ScopemappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.ServicesMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.SeveritymappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.StatusmappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.UrgencymappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.ottimizzati.XmlCapMapper;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;

public abstract class AlertManager {

	final 		static 	String 									CLASSNAME = "AlertManager";

	private 			ServicesMapper 							servicesMapper;
	protected 			CapalertsMapper 						capalertsMapper;
	protected 			CapinfoalertsMapper 					capinfoalertsMapper;
	protected 			CapinfoalertparametersMapper 			capinfoalertparametersMapper;
	protected 			CapinfoalertareasMapper 				capinfoalertareasMapper;
	protected 			CapinfoalertareageocodesMapper 			capinfoalertareageocodesMapper;
	private 			CategorymappingMapper 					categorymappingMapper;
	private 			CertaintymappingMapper 					certaintymappingMapper;
	private				MsgtypemappingMapper 					msgtypemappingMapper;
	private 			RelationalresponsetypemappingMapper 	relationalresponsetypemappingMapper;
	private 			RelationalcategorymappingMapper 		relationalcategorymappingMapper;
	private 			ResponsetypemappingMapper 				responsetypemappingMapper;
	private 			ScopemappingMapper 						scopemappingMapper;
	private 			SeveritymappingMapper 					severitymappingMapper;
	private 			StatusmappingMapper 					statusmappingMapper;
	private 			UrgencymappingMapper 					urgencymappingMapper;	
	private 			GeojsonforidiMapper 					geojsonforidiMapper;	
	private 			XmlCapMapper 							xmlcapMapper;	
	private 			CatalunyaincomingjsonMapper 			catalunyaincomingjsonMapper;	
	private 			AnagraficastazioniarpaMapper 			anagraficastazioniarpaMapper;	
	private 			CatalunyaalertMapper 					catalunyaalertMapper;	
	protected 			FmialertMapper 							fmialertMapper;
	protected			Map<String, String>						mapNameGeojsonFromDb;
	protected			ArpaalertMapper							arpaalertMapper;
	protected			HashMap<String, Arpaalert>				mapArpaAlert;
	private 			SqlSession 								session;
	protected			String									ArpaIdentifier;
	private 			int 									idTask;
	private 			String 									dateStart;
	private 			String 									dateEnd;
	private 			String 									dateLastRevision;
	private 			String 									creationDate;
	private 			String 									hazardGLIDECode; 
	private 			String 									hazardName; 
	private 			String 									hazardLevel;
	private				String 									hazardLevelDescription;
	private 			String 									temporalReferenceEnd;
	private 			String 									temporalReferenceStart;
	private				String 									temporalreferenceDateofcreation;
	private 			String 									temporalreferenceDateofpublication;
	private 			String 									temporalreferenceDateoflastrevision;
	protected 			HashMap<String, Integer> 				mapMsgType;
	protected 			HashMap<String, Integer> 				mapStatus;
	protected 			HashMap<String, Integer> 				mapScope;
	protected 			HashMap<String, Integer> 				mapUrgency;
	protected 			HashMap<String, Integer> 				mapSeverity;
	protected 			HashMap<String, Integer> 				mapCertainty;
	protected 			HashMap<String, Integer> 				mapParameterType;
	protected 			HashMap<String, Integer> 				mapCategory;
	protected 			HashMap<String, Integer> 				mapResponseType;
	protected 			HashMap<String, String> 				mapGeojsonForIdi;
	protected 			HashMap<String, Anagraficastazioniarpa> mapAnagraficaStazioniARPA;	
	protected 			List<String> 							listCatalunyaJsonOnDb;	
	protected 			String[] 								leadTime;
	protected 			BigDecimal[] 							timeSpan;
	private 			StringBuilder 							errorStringBuilder = new StringBuilder();
	protected 			boolean 								iscatalunya = false; 
	protected 			boolean 								isfmi = false; 
	protected 			boolean 								isArpa = false; 
	 
	
	public void appendToErrorStringBuilder(String s) {
		if (errorStringBuilder.length() > 0) {
			errorStringBuilder.append("\n");
		}
		errorStringBuilder.append(s);
	}

	public AlertManager() {
		try {
			InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
			session 							= factory.openSession(false);
			servicesMapper						= session.getMapper(ServicesMapper.class);
			capalertsMapper 					= session.getMapper(CapalertsMapper.class);
			capinfoalertsMapper 				= session.getMapper(CapinfoalertsMapper.class);
			capinfoalertparametersMapper 		= session.getMapper(CapinfoalertparametersMapper.class);
			capinfoalertareasMapper 			= session.getMapper(CapinfoalertareasMapper.class);
			capinfoalertareageocodesMapper 		= session.getMapper(CapinfoalertareageocodesMapper.class);
			relationalcategorymappingMapper 	= session.getMapper(RelationalcategorymappingMapper.class);
			relationalresponsetypemappingMapper = session.getMapper(RelationalresponsetypemappingMapper.class);
			categorymappingMapper 				= session.getMapper(CategorymappingMapper.class);
			createHashMapCategory();
			responsetypemappingMapper 			= session.getMapper(ResponsetypemappingMapper.class);
			createHashMapResponseType();
			msgtypemappingMapper 				= session.getMapper(MsgtypemappingMapper.class);
			createHashMapMsgType();
			statusmappingMapper 				= session.getMapper(StatusmappingMapper.class);
			createHashMapStatus();
			scopemappingMapper 					= session.getMapper(ScopemappingMapper.class);
			createHashMapScope();
			urgencymappingMapper 				= session.getMapper(UrgencymappingMapper.class);
			createHashMapUrgency();
			severitymappingMapper 				= session.getMapper(SeveritymappingMapper.class);
			createHashMapSeverity();
			certaintymappingMapper 				= session.getMapper(CertaintymappingMapper.class);
			createHashMapCertainty();
			xmlcapMapper 						= session.getMapper(XmlCapMapper.class);
			geojsonforidiMapper 				= session.getMapper(GeojsonforidiMapper.class);
			createHashMapGeojsonForIdi();
			catalunyaincomingjsonMapper 		= session.getMapper(CatalunyaincomingjsonMapper.class);
			createListCatalunyaDbJson();
			anagraficastazioniarpaMapper 		= session.getMapper(AnagraficastazioniarpaMapper.class);
			//createMapAnagraficaStazioniARPA();
			catalunyaalertMapper 				= session.getMapper(CatalunyaalertMapper.class);
			fmialertMapper 						= session.getMapper(FmialertMapper.class);
			arpaalertMapper 					= session.getMapper(ArpaalertMapper.class);
			populateArpaAlertMap();
		} catch (Exception e) {
		} finally {
		}
	}
	
	
	private void populateArpaAlertMap() {
		mapArpaAlert = new HashMap<>();
		List<Arpaalert> allAlerts = arpaalertMapper.selectByExample(null);
		for(Arpaalert a:allAlerts){
			if(a.getIdentifierbulletin()!=null){
				mapArpaAlert.put(a.getIdentifierbulletin(), a);
			}
		}
	}

	@SuppressWarnings("unused")
	private void createMapAnagraficaStazioniARPA() {
		List<Anagraficastazioniarpa> listAnagraficaStazioniARPA = new ArrayList<>();
		listAnagraficaStazioniARPA = anagraficastazioniarpaMapper.selectByExample(null);
		for(Anagraficastazioniarpa a: listAnagraficaStazioniARPA){
			mapAnagraficaStazioniARPA.put(a.getCodicestazione(), a);
		}
	}

	private void createListCatalunyaDbJson() {
		listCatalunyaJsonOnDb = new ArrayList<>();
		List<Catalunyaincomingjson> list = catalunyaincomingjsonMapper.selectByExample(null);
		if (list == null || list.size() == 0) {
		} else {
			for (Catalunyaincomingjson m : list) {
				listCatalunyaJsonOnDb.add(m.getJson());
			}
		}
	}


	
	public HashMap<String, String> getHashMapGeojsonForIdi(){
		return this.mapGeojsonForIdi;
	}
		

	private void createHashMapResponseType() {
		mapResponseType = new HashMap<>();
		List<Responsetypemapping> list = responsetypemappingMapper.selectByExample(null);
		if (list == null || list.size() == 0) {
			String s = "La tabella lems.ResponseTypeMapping non contiene decodifiche";
			appendToErrorStringBuilder(s);
		} else {
			for (Responsetypemapping m : list) {
				mapResponseType.put(m.getValue(), m.getId());
			}
		}
	}

	private void createHashMapCategory() {
		mapCategory = new HashMap<>();
		List<Categorymapping> list = categorymappingMapper.selectByExample(null);
		if (list == null || list.size() == 0) {
			String s = "La tabella lems.CategoryMapping non contiene decodifiche";
			appendToErrorStringBuilder(s);
		} else {
			for (Categorymapping m : list) {
				mapCategory.put(m.getValue(), m.getId());
			}
		}
	}

	private void createHashMapCertainty() {
		mapCertainty = new HashMap<>();
		List<Certaintymapping> list = certaintymappingMapper.selectByExample(null);
		if (list == null || list.size() == 0) {
			String s = "La tabella lems.CertaintyMapping non contiene decodifiche";
			appendToErrorStringBuilder(s);
		} else {
			for (Certaintymapping m : list) {
				mapCertainty.put(m.getValue(), m.getId());
			}
		}
	}

	private void createHashMapSeverity() {
		mapSeverity = new HashMap<>();
		List<Severitymapping> list = severitymappingMapper.selectByExample(null);
		if (list == null || list.size() == 0) {
			String s = "La tabella lems.SeverityMapping non contiene decodifiche";
			appendToErrorStringBuilder(s);
		} else {
			for (Severitymapping m : list) {
				mapSeverity.put(m.getValue(), m.getId());
			}
		}
	}

	private void createHashMapUrgency() {
		mapUrgency = new HashMap<>();
		List<Urgencymapping> list = urgencymappingMapper.selectByExample(null);
		if (list == null || list.size() == 0) {
			String s = "La tabella lems.UrgencyMapping non contiene decodifiche";
			appendToErrorStringBuilder(s);
		} else {
			for (Urgencymapping m : list) {
				mapUrgency.put(m.getValue(), m.getId());
			}
		}
	}

	private void createHashMapScope() {
		mapScope = new HashMap<>();
		List<Scopemapping> list = scopemappingMapper.selectByExample(null);
		if (list == null || list.size() == 0) {
			String s = "La tabella lems.ScopeMapping non contiene decodifiche";
			appendToErrorStringBuilder(s);
		} else {
			for (Scopemapping m : list) {
				mapScope.put(m.getValue(), m.getId());
			}
		}
	}

	private void createHashMapStatus() {
		mapStatus = new HashMap<>();
		List<Statusmapping> list = statusmappingMapper.selectByExample(null);
		if (list == null || list.size() == 0) {
			String s = "La tabella lems.StatusMapping non contiene decodifiche";
			appendToErrorStringBuilder(s);
		} else {
			for (Statusmapping m : list) {
				mapStatus.put(m.getValue(), m.getId());
			}
		}
	}

	private void createHashMapMsgType() {
		mapMsgType = new HashMap<>();
		List<Msgtypemapping> list = msgtypemappingMapper.selectByExample(new MsgtypemappingExample());
		if (list == null || list.size() == 0) {
			String s = "La tabella lems.MsgTypeMapping non contiene decodifiche";
			appendToErrorStringBuilder(s);
		} else {
			for (Msgtypemapping m : list) {
				mapMsgType.put(m.getValue(), m.getId());
			}
		}
	}
	
	protected HashMap<String, String> createHashMapGeojsonForIdi() {
		mapNameGeojsonFromDb = new HashMap<>();
		List<Geojsonforidi> list = geojsonforidiMapper.selectByExample(null);
		for(Geojsonforidi g:list){
			if(g.getFilename()!=null && g.getValue()!=null){
				mapNameGeojsonFromDb.put(g.getFilename(), g.getValue());
			}			
		}		
		return mapGeojsonForIdi;

	}

	protected abstract void process() throws BadErrorNeedsEmailSendingException;

	public void processBatch(int task) {
		try {
			setIdTask(task);
			process();
			if (errorStringBuilder.length() > 0) {
				throw new BadErrorNeedsEmailSendingException(errorStringBuilder.toString());
			}

		} catch (BadErrorNeedsEmailSendingException bad) {
			String mailSubject  = CLASSNAME+"i-react trial";
			String mailText = "eccezione gestita: " + bad.getMessage();
			MailHelper.postMail(mailSubject, mailText);
		} catch (Exception ecc) {

			String mailSubject = "i-react trial";
			String mailText = "eccezione non prevista: " + ecc.toString();
			MailHelper.postMail(mailSubject, mailText);
		}
	}

	public abstract void insertDataDB(Object data, int serviceId, Date today);

	public HashMap<Long, Feature> getFeaturesMapByLong(Feature[] features, String key) {
		HashMap<Long, Feature> featuresMap = new HashMap<Long, Feature>();
		if (features != null)
			for (int i = 0; i < features.length; i++) {
				featuresMap.put(Long.parseLong((String) features[i].getProperties().get(key)), features[i]);
			}
		return featuresMap;
	}

	public HashMap<String, Feature> getFeaturesMap(Feature[] features, String key) {
		HashMap<String, Feature> featuresMap = new HashMap<String, Feature>();
		if (features != null)
			for (int i = 0; i < features.length; i++) {
				featuresMap.put((String) features[i].getProperties().get(key), features[i]);
			}
		return featuresMap;
	}

	@SuppressWarnings("unused")
	public String writeDBAndsendToIDI(Object data, List<String> geojsonList, ZonedDateTime now) {
		String mailSubject = CLASSNAME+"::writeDBAndsendToIDI with idTask:"+idTask;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try {
			ServicesExample serviceExample = new ServicesExample();
			ServicesExample.Criteria serviceCriteria = serviceExample.createCriteria();
			serviceCriteria.andIdtaskEqualTo(idTask);
			List<Services> service = getServicesMapper().selectByExample(serviceExample);	
			HashMap<String, String> metadata = MetaDataIdiHelper.createIdiMetaData(service.get(0));	
			Date today = Util.getSysDate();	
			if(data!=null) {
				insertDataDB(data, service.get(0).getId(), today);
			}			
			HttpPost httpPost = null;	
			httpPost = new HttpPost(LemsWebConstant.URL_IDI_INVIO);		
			String auth = LemsWebConstant.USER_IDI + ":" + LemsWebConstant.PWD_IDI;
			byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("ISO-8859-1")));
			String authHeader = "Basic " + new String(encodedAuth);
			httpPost.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			httpPost.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
			HttpClient client = HttpClientBuilder.create().build();
			metadata.put("metadataonmetadata_date", now.format(DateTimeFormatter.ISO_INSTANT));			
			metadata.put("acquisitiondate", temporalReferenceStart);
			metadata.put("temporalreference_start", temporalReferenceStart);			
			metadata.put("temporalreference_end", temporalReferenceEnd);
			metadata.put("temporalreference_dateoflastrevision", temporalreferenceDateoflastrevision);
			metadata.put("temporalreference_dateofcreation", temporalreferenceDateofcreation);
			metadata.put("temporalreference_dateofpublication", temporalreferenceDateofpublication);			
			String fileName = getFileName();
			StringBuilder metadataFilenames = new StringBuilder();
			StringBuilder metadataVisualizationOrder = new StringBuilder();
			StringBuilder metadataLeadtimes = new StringBuilder();
			StringBuilder metadataTimeSpan = new StringBuilder();	
			metadataFilenames.append("[");
			metadataVisualizationOrder.append("[");
			metadataLeadtimes.append("[");
			metadataTimeSpan.append("[");	
			int i = 0;
			for (String jsonInString : geojsonList) {
				if (i != 0) {
					metadataFilenames.append(",");
					metadataVisualizationOrder.append(",");
					metadataLeadtimes.append(",");
					metadataTimeSpan.append(",");
				}
				metadataFilenames.append("\"").append(fileName).append(++i).append(".geojson").append("\"");
				metadataVisualizationOrder.append("\"").append(i).append("\"");
				metadataLeadtimes.append("\"").append(leadTime[i - 1]).append("\"");
				if (timeSpan == null || timeSpan.length == 0) {
					metadataTimeSpan.append("\"").append("1").append("\"");
				} else {
					metadataTimeSpan.append("\"").append(timeSpan[i - 1]).append("\"");
				}	
			}
			metadataFilenames.append("]");
			metadataVisualizationOrder.append("]");
			metadataLeadtimes.append("]");
			metadataTimeSpan.append("]");	
			metadata.put("filenames", 						metadataFilenames.toString());
			metadata.put("visualization_order", 			metadataVisualizationOrder.toString());
			metadata.put("leadtimes", 						metadataLeadtimes.toString());
			metadata.put("timespans", 						metadataTimeSpan.toString());	
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setBoundary("----WebKitFormBoundarypUkY3YHXFOVqZA2Q");
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);		
			Charset chars = Charset.forName("UTF-8");			
			for (String key : metadata.keySet()) {
				String value = null;
				if(metadata.get(key)==null) {
					value = "";
				}else{
					value = metadata.get(key);
				}
				@SuppressWarnings("deprecation")
				StringBody stringB = new StringBody(value,chars);  
				builder.addPart(key, stringB);
			}	
			i = 0;
			for (String jsonInString : geojsonList) {
				i++;
				builder.addBinaryBody(fileName + i + ".geojson", new ByteArrayInputStream(jsonInString.getBytes("UTF-8")),
						ContentType.DEFAULT_BINARY, fileName);
			}
			HttpEntity httpEntity = builder.build();
			BufferedHttpEntity multipartEntity = new BufferedHttpEntity(httpEntity);			
			httpPost.setEntity(multipartEntity);			
			client = HttpClientBuilder.create().build();						
			String line;
			BufferedReader br = new BufferedReader(new InputStreamReader(httpPost.getEntity().getContent()));			
			if(iscatalunya){
				Catalunyaalert ca = new Catalunyaalert();
				ca.setTimespans(metadataTimeSpan.toString());
				ca.setLeadtimes(metadataLeadtimes.toString());
				ca.setVisualizationOrder(metadataVisualizationOrder.toString());
				ca.setFilenames(metadataFilenames.toString());
				ca.setTemporalereferenceDateofpubblication(temporalreferenceDateofpublication);
				ca.setTemporalereferenceDateofcreation(temporalreferenceDateofcreation);
				ca.setTemporalerefenceDateoflastrevision(temporalreferenceDateoflastrevision);
				ca.setTemporalreferenceEnd(temporalReferenceEnd);
				ca.setTemporalereferenceStart(temporalReferenceStart);
				ca.setAcquisitiondate(temporalReferenceStart);
				ca.setMetadataonmetadataDate(now.format(DateTimeFormatter.ISO_INSTANT));
				ca.setGeographicBoundingBoxes(metadata.get("geographic_bounding_boxes"));
				ca.setIdtask(Integer.toString(idTask));
				ca.setIdentificationResourcetitle(metadata.get("identification_resourcetitle"));
				ca.setIdentificationResourcetype(metadata.get("identification_resourcetype"));
				ca.setIdentificationResourcelanguage(metadata.get("identification_resourcelanguage"));
				ca.setIdentificationResourceabstract(metadata.get("identification_resourceabstract"));
				ca.setConformityDegree(metadata.get("conformity_degree"));
				ca.setConformitySpecification(metadata.get("conformity_specification"));
				ca.setClassificationSpatialdataservicetype(metadata.get("classification_spatialdataservicetype"));
				ca.setClassificationTopiccategory(metadata.get("classification_topiccategory"));
				ca.setKeywordKeywordvalue(metadata.get("keyword_keywordvalue"));
				ca.setKeywordOriginatingcontrolledvocabulary(metadata.get("keyword_originatingcontrolledvocabulary"));
				ca.setCoordinatesystemreferenceCode(metadata.get("coordinatesystemreference_code"));
				ca.setCoordinatesystemreferenceCodespace(metadata.get("coordinatesystemreference_codespace"));
				ca.setResponsibleorganizationResponsiblepartyEmail(metadata.get("responsibleorganization_responsibleparty_email"));
				ca.setResponsibleorganizationResponsiblepartyrole(metadata.get("responsibleorganization_responsiblepartyrole"));
				ca.setResponsibleorganizationResponsiblepartyOrganizationname(metadata.get("responsibleorganization_responsibleparty_organizationname"));
				ca.setMetadataonmetadataDate(metadata.get("metadataonmetadata_date"));
				ca.setMetadataonmetadataLanguage(metadata.get("metadataonmetadata_language"));
				ca.setMetadataonmetadataPointofcontactEmail(metadata.get("metadataonmetadata_pointofcontact_email"));
				ca.setMetadataonmetadataPointofcontactOrganizationname(metadata.get("metadataonmetadata_pointofcontact_organizationname"));
				ca.setQualityandvaliditySpatialresolutionScale(metadata.get("qualityandvalidity_spatialresolution_scale"));
				ca.setQualityandvaliditySpatialresolutionMeasureunit(metadata.get("qualityandvalidity_spatialresolution_measureunit"));
				ca.setQualityandvaliditySpatialresolutionLatitude(metadata.get("qualityandvalidity_spatialresolution_latitude"));
				ca.setQualityandvaliditySpatialresolutionLongitude(metadata.get("qualityandvalidity_spatialresolution_longitude"));
				ca.setFilenames(fileName);			
				ca.setDateofcreation(format.format(new Date()));
				int p =0;
				for (String jsonInString : geojsonList) {
					p++;
					if(p==1) ca.setFile1(jsonInString);
					else if(p==2) ca.setFile2(jsonInString);
					else if(p==3) ca.setFile3(jsonInString);
					else if(p==4) ca.setFile4(jsonInString);
					else if(p==5) ca.setFile5(jsonInString);
					else if(p==6) ca.setFile6(jsonInString);
					else if(p==7) ca.setFile7(jsonInString);
				}
				
				getCatalunyaalertMapper().insertSelective(ca);
				getSession().commit();
				iscatalunya=false;
			}
			if(isfmi){
				Fmialert fa = new Fmialert();
				if(metadataTimeSpan.toString()!=null) fa.setTimespans(metadataTimeSpan.toString());
				if(metadataLeadtimes!=null) fa.setLeadtimes(metadataLeadtimes.toString());
				if(metadataVisualizationOrder!=null) fa.setVisualizationOrder(metadataVisualizationOrder.toString());
				if(metadataFilenames!=null) fa.setFilenames(metadataFilenames.toString());
				if(temporalreferenceDateofcreation!=null) fa.setTemporalereferenceDateofpubblication(temporalreferenceDateofpublication);
				if(temporalreferenceDateofcreation!=null) fa.setTemporalereferenceDateofcreation(temporalreferenceDateofcreation);
				if(temporalreferenceDateoflastrevision!=null) fa.setTemporalerefenceDateoflastrevision(temporalreferenceDateoflastrevision);
				if(temporalReferenceEnd!=null) fa.setTemporalreferenceEnd(temporalReferenceEnd);
				if(temporalReferenceStart!=null) fa.setTemporalereferenceStart(temporalReferenceStart);
				if(temporalReferenceStart!=null) fa.setAcquisitiondate(temporalReferenceStart);
				if(now.format(DateTimeFormatter.ISO_INSTANT)!=null) fa.setMetadataonmetadataDate(now.format(DateTimeFormatter.ISO_INSTANT));
				if(metadata.get("geographic_bounding_boxes")!=null) fa.setGeographicBoundingBoxes(metadata.get("geographic_bounding_boxes"));
				fa.setIdtask(Integer.toString(idTask));
				if(metadata.get("identification_resourcetitle")!=null) fa.setIdentificationResourcetitle(metadata.get("identification_resourcetitle"));
				if(metadata.get("identification_resourcetype")!=null) fa.setIdentificationResourcetype(metadata.get("identification_resourcetype"));
				if(metadata.get("identification_resourcelanguage")!=null) fa.setIdentificationResourcelanguage(metadata.get("identification_resourcelanguage"));
				if(metadata.get("identification_resourceabstract")!=null) fa.setIdentificationResourceabstract(metadata.get("identification_resourceabstract"));
				if(metadata.get("conformity_degree")!=null) fa.setConformityDegree(metadata.get("conformity_degree"));
				if(metadata.get("conformity_specification")!=null) fa.setConformitySpecification(metadata.get("conformity_specification"));
				if(metadata.get("classification_spatialdataservicetype")!=null) fa.setClassificationSpatialdataservicetype(metadata.get("classification_spatialdataservicetype"));
				if(metadata.get("classification_topiccategory")!=null) fa.setClassificationTopiccategory(metadata.get("classification_topiccategory"));
				if(metadata.get("keyword_keywordvalue")!=null) fa.setKeywordKeywordvalue(metadata.get("keyword_keywordvalue"));
				if(metadata.get("keyword_originatingcontrolledvocabulary")!=null) fa.setKeywordOriginatingcontrolledvocabulary(metadata.get("keyword_originatingcontrolledvocabulary"));
				if(metadata.get("coordinatesystemreference_code")!=null) fa.setCoordinatesystemreferenceCode(metadata.get("coordinatesystemreference_code"));
				if(metadata.get("coordinatesystemreference_codespace")!=null) fa.setCoordinatesystemreferenceCodespace(metadata.get("coordinatesystemreference_codespace"));
				if(metadata.get("responsibleorganization_responsibleparty_email")!=null) fa.setResponsibleorganizationResponsiblepartyEmail(metadata.get("responsibleorganization_responsibleparty_email"));
				if(metadata.get("responsibleorganization_responsiblepartyrole")!=null) fa.setResponsibleorganizationResponsiblepartyrole(metadata.get("responsibleorganization_responsiblepartyrole"));
				if(metadata.get("responsibleorganization_responsibleparty_organizationname")!=null) fa.setResponsibleorganizationResponsiblepartyOrganizationname(metadata.get("responsibleorganization_responsibleparty_organizationname"));
				if(metadata.get("metadataonmetadata_date")!=null) fa.setMetadataonmetadataDate(metadata.get("metadataonmetadata_date"));
				if(metadata.get("metadataonmetadata_language")!=null) fa.setMetadataonmetadataLanguage(metadata.get("metadataonmetadata_language"));
				if(metadata.get("metadataonmetadata_pointofcontact_email")!=null) fa.setMetadataonmetadataPointofcontactEmail(metadata.get("metadataonmetadata_pointofcontact_email"));
				if(metadata.get("metadataonmetadata_pointofcontact_organizationname")!=null) fa.setMetadataonmetadataPointofcontactOrganizationname(metadata.get("metadataonmetadata_pointofcontact_organizationname"));
				if(metadata.get("qualityandvalidity_spatialresolution_scale")!=null) fa.setQualityandvaliditySpatialresolutionScale(metadata.get("qualityandvalidity_spatialresolution_scale"));
				if(metadata.get("qualityandvalidity_spatialresolution_measureunit")!=null) fa.setQualityandvaliditySpatialresolutionMeasureunit(metadata.get("qualityandvalidity_spatialresolution_measureunit"));
				if(metadata.get("qualityandvalidity_spatialresolution_latitude")!=null) fa.setQualityandvaliditySpatialresolutionLatitude(metadata.get("qualityandvalidity_spatialresolution_latitude"));
				if(metadata.get("qualityandvalidity_spatialresolution_longitude")!=null) {fa.setQualityandvaliditySpatialresolutionLongitude(metadata.get("qualityandvalidity_spatialresolution_longitude"));} else fa.setQualityandvaliditySpatialresolutionLongitude(Integer.toString(0));
				if(fileName!=null) fa.setFilenames(fileName);			
				fa.setDateofcreation(format.format(new Date()));
				if(geojsonList.get(0)!=null) fa.setGeojson(geojsonList.get(0));
				getFmialertMapper().insertSelective(fa);
				getSession().commit();
				isfmi=false;
			}
			if(isArpa){
				Arpaalert aa = new Arpaalert();
				if(ArpaIdentifier!=null) aa.setIdentifierbulletin(ArpaIdentifier); else aa.setIdentifierbulletin("");
				if(metadataTimeSpan.toString()!=null) aa.setTimespans(metadataTimeSpan.toString());
				if(metadataLeadtimes!=null) aa.setLeadtimes(metadataLeadtimes.toString());
				if(metadataVisualizationOrder!=null) aa.setVisualizationOrder(metadataVisualizationOrder.toString());
				if(metadataFilenames!=null) aa.setFilenames(metadataFilenames.toString());
				if(temporalreferenceDateofcreation!=null) aa.setTemporalereferenceDateofpubblication(temporalreferenceDateofpublication);
				if(temporalreferenceDateofcreation!=null) aa.setTemporalereferenceDateofcreation(temporalreferenceDateofcreation);
				if(temporalreferenceDateoflastrevision!=null) aa.setTemporalerefenceDateoflastrevision(temporalreferenceDateoflastrevision); 
				if(temporalReferenceEnd!=null) aa.setTemporalreferenceEnd(temporalReferenceEnd);
				if(temporalReferenceStart!=null) aa.setTemporalereferenceStart(temporalReferenceStart);
				if(temporalReferenceStart!=null) aa.setAcquisitiondate(temporalReferenceStart);
				if(now.format(DateTimeFormatter.ISO_INSTANT)!=null) aa.setMetadataonmetadataDate(now.format(DateTimeFormatter.ISO_INSTANT));
				if(metadata.get("geographic_bounding_boxes")!=null) aa.setGeographicBoundingBoxes(metadata.get("geographic_bounding_boxes"));
				aa.setIdtask(Integer.toString(idTask));
				if(metadata.get("identification_resourcetitle")!=null) aa.setIdentificationResourcetitle(metadata.get("identification_resourcetitle"));
				if(metadata.get("identification_resourcetype")!=null) aa.setIdentificationResourcetype(metadata.get("identification_resourcetype"));
				if(metadata.get("identification_resourcelanguage")!=null) aa.setIdentificationResourcelanguage(metadata.get("identification_resourcelanguage"));
				if(metadata.get("identification_resourceabstract")!=null) aa.setIdentificationResourceabstract(metadata.get("identification_resourceabstract"));
				if(metadata.get("conformity_degree")!=null) aa.setConformityDegree(metadata.get("conformity_degree"));
				if(metadata.get("conformity_specification")!=null) aa.setConformitySpecification(metadata.get("conformity_specification"));
				if(metadata.get("classification_spatialdataservicetype")!=null) aa.setClassificationSpatialdataservicetype(metadata.get("classification_spatialdataservicetype"));
				if(metadata.get("classification_topiccategory")!=null) aa.setClassificationTopiccategory(metadata.get("classification_topiccategory"));
				if(metadata.get("keyword_keywordvalue")!=null) aa.setKeywordKeywordvalue(metadata.get("keyword_keywordvalue"));
				if(metadata.get("keyword_originatingcontrolledvocabulary")!=null) aa.setKeywordOriginatingcontrolledvocabulary(metadata.get("keyword_originatingcontrolledvocabulary"));
				if(metadata.get("coordinatesystemreference_code")!=null) aa.setCoordinatesystemreferenceCode(metadata.get("coordinatesystemreference_code"));
				if(metadata.get("coordinatesystemreference_codespace")!=null) aa.setCoordinatesystemreferenceCodespace(metadata.get("coordinatesystemreference_codespace"));
				if(metadata.get("responsibleorganization_responsibleparty_email")!=null) aa.setResponsibleorganizationResponsiblepartyEmail(metadata.get("responsibleorganization_responsibleparty_email"));
				if(metadata.get("responsibleorganization_responsiblepartyrole")!=null) aa.setResponsibleorganizationResponsiblepartyrole(metadata.get("responsibleorganization_responsiblepartyrole"));
				if(metadata.get("responsibleorganization_responsibleparty_organizationname")!=null) aa.setResponsibleorganizationResponsiblepartyOrganizationname(metadata.get("responsibleorganization_responsibleparty_organizationname"));
				if(metadata.get("metadataonmetadata_date")!=null) aa.setMetadataonmetadataDate(metadata.get("metadataonmetadata_date"));
				if(metadata.get("metadataonmetadata_language")!=null) aa.setMetadataonmetadataLanguage(metadata.get("metadataonmetadata_language"));
				if(metadata.get("metadataonmetadata_pointofcontact_email")!=null) aa.setMetadataonmetadataPointofcontactEmail(metadata.get("metadataonmetadata_pointofcontact_email"));
				if(metadata.get("metadataonmetadata_pointofcontact_organizationname")!=null) aa.setMetadataonmetadataPointofcontactOrganizationname(metadata.get("metadataonmetadata_pointofcontact_organizationname"));
				if(metadata.get("qualityandvalidity_spatialresolution_scale")!=null) aa.setQualityandvaliditySpatialresolutionScale(metadata.get("qualityandvalidity_spatialresolution_scale"));
				if(metadata.get("qualityandvalidity_spatialresolution_measureunit")!=null) aa.setQualityandvaliditySpatialresolutionMeasureunit(metadata.get("qualityandvalidity_spatialresolution_measureunit"));
				if(metadata.get("qualityandvalidity_spatialresolution_latitude")!=null) aa.setQualityandvaliditySpatialresolutionLatitude(metadata.get("qualityandvalidity_spatialresolution_latitude"));
				if(metadata.get("qualityandvalidity_spatialresolution_longitude")!=null) {aa.setQualityandvaliditySpatialresolutionLongitude(metadata.get("qualityandvalidity_spatialresolution_longitude"));} else aa.setQualityandvaliditySpatialresolutionLongitude(Integer.toString(0));
				if(fileName!=null) aa.setFilenames(fileName);			
				aa.setDateofcreation(format.format(new Date()));
				if(geojsonList.get(0)!=null) {
					if(geojsonList.size()==1){
						aa.setGeojson(geojsonList.get(0));
					}else if(geojsonList.size()==2){
						aa.setGeojson(geojsonList.get(0)+" - "+geojsonList.get(1));
					}
				}
				getArpaalertMapper().insertSelective(aa);
				getSession().commit();
				isArpa=false;
			}
			HttpResponse  response = client.execute(httpPost);			
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				String body = null;
				try{
					body = EntityUtils.toString( response.getEntity(), "UTF-8");					
					return "OK";
				} catch (Exception e){					
					if (body!=null)
						return "KO:"+body;
					else return "KO:"+e.getMessage();
				}				
			} else {
				String messageError=EntityUtils.toString(response.getEntity(), "UTF-8");
				if(messageError==null)	messageError = "";
				String mailText = "Http statusCode = " + statusCode + ", Message = " + messageError;
				MailHelper.postMail(mailSubject, mailText);							
				return mailText;	
			}
		}catch (UnsupportedEncodingException e){
			String mailText="UnsupportedEncodingException message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
			return mailText;
		}catch (ClientProtocolException e){
			String mailText="ClientProtocolException message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
			return mailText;
		}catch (IOException e){
			String mailText="IOException message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
			return mailText;
		}catch (Throwable e){
			String mailText="Throwable message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
			return mailText;
		}		
	}

	private void addStyleProperties(HashMap<String, Object> properties) {
		properties.put("stroke-width", "2");
		properties.put("stroke-opacity", "1");
		properties.put("fill-opacity", "0.4");
		properties.put("layertype", "isoband");
		properties.put("shape-rendering", "crispEdges");
	}

	public void addCommonProperties(Feature feature) {
		feature.getProperties().put("service", LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_service"));
		feature.getProperties().put("hazardCode", LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_hazardCode"));
		if (idTask != LemsWebConstant.TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN
				&& idTask != LemsWebConstant.TASK_SAVA_ALERT_BULLETIN) {
			feature.getProperties().put("hazardGLIDECode",
					LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_hazardGLIDECode"));
			feature.getProperties().put("hazardName", LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_hazardName"));
		} else {
			feature.getProperties().put("hazardGLIDECode", hazardGLIDECode);
			feature.getProperties().put("hazardName", hazardName);
		}
		feature.getProperties().put("hazardLevel", hazardLevel);
		feature.getProperties().put("hazardLevelDescription", hazardLevelDescription);
		feature.getProperties().put("creationDate", creationDate);
		feature.getProperties().put("country", LemsWebConstant.BULLETIN_HAZARD.get(idTask + "_country"));
		feature.getProperties().put("hazardLevel", hazardLevel);
	}

	public void addGeneralProperties(Feature feature) {
		addStyleProperties(feature.getProperties());
	}

	public void closeDBSession() {
		try {
			session.close();
		} catch (Exception e) {
		}
	}

	public String getGeojson(Geojson geojson) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(geojson);
	}

	public Geojson getGeojson(String resource) throws Exception {
		InputStream in = null;
		if(resource!=null && resource.contains("geojson") && mapNameGeojsonFromDb.get(resource)!=null){
			in = new ByteArrayInputStream(mapNameGeojsonFromDb.get(resource).getBytes(StandardCharsets.UTF_16));
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(in, Geojson.class);
	}

	private String getFileName() {

		switch (idTask) {
		case LemsWebConstant.TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN:
			return LemsWebConstant.ARPA_ALERT_FILE_NAME;
		case LemsWebConstant.TASK_SAVA_ALERT_BULLETIN:
			return LemsWebConstant.SAVA_FILE_NAME;
		case LemsWebConstant.TASK_UK_5_DAY_BULLETIN:
			return LemsWebConstant.UK_ALERT_STATEMENTS_FILE_NAME;

		/* FMI file name start */
		case LemsWebConstant.TASK_FMI_WIND_ALERT:
			return LemsWebConstant.FMI_ALERT_FILE_NAME_WIND;
		case LemsWebConstant.TASK_FMI_SNOW_ICE_ALERT:
			return LemsWebConstant.FMI_ALERT_FILE_NAME_SNOW_ICE;
		case LemsWebConstant.TASK_FMI_THUNDERSTORMS_ALERT:
			return LemsWebConstant.FMI_ALERT_FILE_NAME_THUNDERSTORMS;
		case LemsWebConstant.TASK_FMI_HIGH_TEMPERATURE_ALERT:
			return LemsWebConstant.FMI_ALERT_FILE_NAME_HIGH_TEMPERATURE;
		case LemsWebConstant.TASK_FMI_LOW_TEMPERATURE_ALERT:
			return LemsWebConstant.FMI_ALERT_FILE_NAME_LOW_TEMPERATURE;
		case LemsWebConstant.TASK_FMI_FOREST_FIRE_ALERT:
			return LemsWebConstant.FMI_ALERT_FILE_NAME_FOREST_FIRE;
		case LemsWebConstant.TASK_FMI_RAIN_ALERT:
			return LemsWebConstant.FMI_ALERT_FILE_NAME_RAIN;
		/* FMI file name end */

		/* CATALUNYA file name start */
		case LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT:
			return LemsWebConstant.CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT_FILE_NAME;
		case LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT:
			return LemsWebConstant.CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT_FILE_NAME;
		case LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT:
			return LemsWebConstant.CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT_FILE_NAME;
		case LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT:
			return LemsWebConstant.CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT_FILE_NAME;
		case LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT:
			return LemsWebConstant.CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT_FILE_NAME;
		case LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT:
			return LemsWebConstant.CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT_FILE_NAME;
		case LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT:
			return LemsWebConstant.CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT_FILE_NAME;
		/* CATALUNYA file name end */
		}
		return null;
	}

	
	public ServicesMapper getServicesMapper() {
		return servicesMapper;
	}

	public void setServicesMapper(ServicesMapper servicesMapper) {
		this.servicesMapper = servicesMapper;
	}

	public CapalertsMapper getCapalertsMapper() {
		return capalertsMapper;
	}

	public void setCapalertsMapper(CapalertsMapper capalertsMapper) {
		this.capalertsMapper = capalertsMapper;
	}

	public CapinfoalertsMapper getCapinfoalertsMapper() {
		return capinfoalertsMapper;
	}

	public void setCapinfoalertsMapper(CapinfoalertsMapper capinfoalertsMapper) {
		this.capinfoalertsMapper = capinfoalertsMapper;
	}

	public CapinfoalertparametersMapper getCapinfoalertparametersMapper() {
		return capinfoalertparametersMapper;
	}

	public void setCapinfoalertparametersMapper(CapinfoalertparametersMapper capinfoalertparametersMapper) {
		this.capinfoalertparametersMapper = capinfoalertparametersMapper;
	}

	public CapinfoalertareasMapper getCapinfoalertareasMapper() {
		return capinfoalertareasMapper;
	}

	public void setCapinfoalertareasMapper(CapinfoalertareasMapper capinfoalertareasMapper) {
		this.capinfoalertareasMapper = capinfoalertareasMapper;
	}

	public CapinfoalertareageocodesMapper getCapinfoalertareageocodesMapper() {
		return capinfoalertareageocodesMapper;
	}

	public void setCapinfoalertareageocodesMapper(CapinfoalertareageocodesMapper capinfoalertareageocodesMapper) {
		this.capinfoalertareageocodesMapper = capinfoalertareageocodesMapper;
	}

	public void setRelationalcategorymappingMapper(RelationalcategorymappingMapper relationalcategorymappingMapper) {
		this.relationalcategorymappingMapper = relationalcategorymappingMapper;
	}

	public RelationalcategorymappingMapper getRelationalcategorymappingMapper() {
		return relationalcategorymappingMapper;
	}

	public void setRelationalresponsetypemappingMapper(
			RelationalresponsetypemappingMapper relationalresponsetypemappingMapper) {
		this.relationalresponsetypemappingMapper = relationalresponsetypemappingMapper;
	}

	public RelationalresponsetypemappingMapper getRelationalresponsetypemappingMapper() {
		return relationalresponsetypemappingMapper;
	}
	
	public void setXmlCapMapper(XmlCapMapper xmlCapMapper) {
		this.xmlcapMapper = xmlCapMapper;
	}
	
	public XmlCapMapper getXmlCapMapper() {
		return xmlcapMapper;
	}
	
	public void setGeojsonForIdiMapper(GeojsonforidiMapper geojsonforidiMapper) {
		this.geojsonforidiMapper = geojsonforidiMapper;
	}
	
	public GeojsonforidiMapper getGeojsonforidiMapper() {
		return geojsonforidiMapper;
	}

	public SqlSession getSession() {
		return session;
	}

	public void setSession(SqlSession session) {
		this.session = session;
	}

	public String getHazardLevel() {
		return hazardLevel;
	}

	public void setHazardLevel(String hazardLevel) {
		this.hazardLevel = hazardLevel;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getDateLastRevision() {
		return dateLastRevision;
	}

	public void setDateLastRevision(String dateLastRevision) {
		this.dateLastRevision = dateLastRevision;
	}

	public int getIdTask() {
		return idTask;
	}

	public void setIdTask(int idTask) {
		this.idTask = idTask;
	}

	public String getHazardGLIDECode() {
		return hazardGLIDECode;
	}

	public void setHazardGLIDECode(String hazardGLIDECode) {
		this.hazardGLIDECode = hazardGLIDECode;
	}

	public String getHazardName() {
		return hazardName;
	}

	public void setHazardName(String hazardName) {
		this.hazardName = hazardName;
	}

	public String getHazardLevelDescription() {
		return hazardLevelDescription;
	}

	public void setHazardLevelDescription(String hazardLevelDescription) {
		this.hazardLevelDescription = hazardLevelDescription;
	}

	public String getTemporalReferenceEnd() {
		return temporalReferenceEnd;
	}

	public void setTemporalReferenceEnd(String temporalReferenceEnd) {
		this.temporalReferenceEnd = temporalReferenceEnd;
	}

	public String getTemporalReferenceStart() {
		return temporalReferenceStart;
	}

	public void setTemporalReferenceStart(String temporalReferenceStart) {
		this.temporalReferenceStart = temporalReferenceStart;
	}

	public String getTemporalreferenceDateofcreation() {
		return temporalreferenceDateofcreation;
	}

	public void setTemporalreferenceDateofcreation(String temporalreferenceDateofcreation) {
		this.temporalreferenceDateofcreation = temporalreferenceDateofcreation;
	}

	public String getTemporalreferenceDateofpublication() {
		return temporalreferenceDateofpublication;
	}

	public void setTemporalreferenceDateofpublication(String temporalreferenceDateofpublication) {
		this.temporalreferenceDateofpublication = temporalreferenceDateofpublication;
	}

	public String getTemporalreferenceDateoflastrevision() {
		return temporalreferenceDateoflastrevision;
	}

	public void setTemporalreferenceDateoflastrevision(String temporalreferenceDateoflastrevision) {
		this.temporalreferenceDateoflastrevision = temporalreferenceDateoflastrevision;
	}

	public String[] getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(String[] leadTime) {
		this.leadTime = leadTime;
	}

	public BigDecimal[] getTimeSpan() {
		return timeSpan;
	}

	public void setTimeSpan(BigDecimal[] timeSpans) {
		this.timeSpan = timeSpans;
	}
	
	public CatalunyaincomingjsonMapper getCatalunyaincomingjsonMapper() {
		return catalunyaincomingjsonMapper;
	}

	public void setCatalunyaincomingjsonMapper(CatalunyaincomingjsonMapper catalunyaincomingjsonMapper) {
		this.catalunyaincomingjsonMapper = catalunyaincomingjsonMapper;
	}
	
	public CatalunyaalertMapper getCatalunyaalertMapper() {
		return catalunyaalertMapper;
	}

	public void setCatalunyaalertMapper(CatalunyaalertMapper catalunyaalertMapper) {
		this.catalunyaalertMapper = catalunyaalertMapper;
	}
	
	public FmialertMapper getFmialertMapper() {
		return fmialertMapper;
	}

	public void setFmialertMapper(FmialertMapper fmialertMapper) {
		this.fmialertMapper = fmialertMapper;
	}
	
	public ArpaalertMapper getArpaalertMapper() {
		return arpaalertMapper;
	}

	public void setArpaalertMapper(ArpaalertMapper arpaalertMapper) {
		this.arpaalertMapper = arpaalertMapper;
	}
	

}
