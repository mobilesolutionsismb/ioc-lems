package it.csi.lems.lemsbatch.manager;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import it.csi.lems.lemsbatch.dto.mybatis.Catalunyaincomingjson;
import it.csi.lems.lemsbatch.dto.ws.es.EpisodisDto;
import it.csi.lems.lemsbatch.dto.ws.es.EpisodisDto.Afectacions;
import it.csi.lems.lemsbatch.dto.ws.es.EpisodisDto.Avisos;
import it.csi.lems.lemsbatch.dto.ws.es.EpisodisDto.Periodes;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson.Feature;
import it.csi.lems.lemsbatch.helper.MailHelper;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;


public class CatalunyaAlertManager extends AlertManager{

	final static String CLASSNAME = LemsWebConstant.CLASSNAME_CATALUNYA;
	
	public void insertDataDB(Object data, int serviceId, Date today){		
	}
	
	@SuppressWarnings("unused")
	public void process() {
		int cat = 0; 		
		Geojson geojson = null;
		Geojson coastalGeojson = null;		
		try {
			geojson = getGeojson(LemsWebConstant.RESOURCE_CATALUNYA_ALERT);
			coastalGeojson = getGeojson(LemsWebConstant.RESOURCE_CATALUNYA_COASTAL_ALERT);
		} catch (Exception e1) {
			e1.printStackTrace();
		}				
		HashMap<Long, Feature> featuresMap = getFeaturesMapByLong(geojson.getFeatures(),"area_cod");	
		HashMap<Long, Feature> featuresMapCoastalAreas = getFeaturesMapByLong(coastalGeojson.getFeatures(),"area_cod");
		List<String> geojsonList=new ArrayList<>();
		try {
			EpisodisDto[] episodis =callWsEpisodis();			
			if(episodis.length==0){
				return;
			}		
			ZonedDateTime now = ZonedDateTime.now();			
			if (episodis!=null) {				
				if(episodis.length == 0) {
					List<Long> listOfGreenRegions = new ArrayList<>();
					populateGlobalIdList(listOfGreenRegions);
					ArrayList<Feature> featuresListGreenRegions=new ArrayList<>();
					for(Long idGreen:listOfGreenRegions){
						featuresListGreenRegions.add(getFutureGreenRegions(featuresMap,featuresMapCoastalAreas, idGreen));
										
					}
					Geojson g = new Geojson();
					g.setFeatures(featuresListGreenRegions.toArray(new Feature[0]));						
					String jsonInString = "";
					jsonInString = getGeojson(g);
					geojsonList.add(jsonInString);	
					iscatalunya = true;
					String result= writeDBAndsendToIDI(null, geojsonList, now);
				}
			}else {
			}		
			
			for (int i=0;i<episodis.length;i++){				
				geojsonList=new ArrayList<>();
				String[] leadTimes=new String[100];
				BigDecimal[] timeSpan = new BigDecimal[100];
				List<Long> listOfGreenRegions = new ArrayList<>();
				List<Long> listOfglobalId = new ArrayList<>();
				List<Long> listOfRegionsWithAfectaciones = new ArrayList<>();				
				populateGlobalIdList(listOfglobalId);
				listOfGreenRegions = listOfglobalId;				
				if (episodis[i].getAvisos()==null || episodis[i].getAvisos().length==0) {
					continue;
				}
				Avisos aviso=getLastAvisos(episodis[i].getAvisos());	
				if (aviso.getEvolucions()==null || aviso.getEvolucions().length==0) {
					continue;
				}
				setIdTask(getIdTask(episodis[i].getMeteor().getIdMeteor().toString()));	
				for (int ind = 0; ind<aviso.getEvolucions().length;ind++){						
					setCreationDate(aviso.getDataEmisio());  
					setDateLastRevision(aviso.getDataEmisio()); 					
					if (aviso.getEvolucions()[ind].getPeriodes()==null || aviso.getEvolucions()[ind].getPeriodes().length==0) {
						continue;
					}								
					LocalDateTime diaDate=(Util.stringToDateTime(aviso.getEvolucions()[ind].getDia()).with(LocalTime.of(0, 0)));
					for (Periodes periode: aviso.getEvolucions()[ind].getPeriodes()){
						listOfGreenRegions = new ArrayList<>();
						populateGlobalIdList(listOfGreenRegions);						
						if (periode.getAfectacions()==null || periode.getAfectacions().length==0) {							
							continue;
						}							
						LocalDateTime leadTime=getLeadTime(diaDate, periode);			            
						setDateStart(leadTime.minusHours(6).toString()); 			            
						setDateEnd(leadTime.toString());
						ArrayList<Feature> featuresList=new ArrayList<>();						
						for (int k=0;k<periode.getAfectacions().length;k++){
							featuresList.add(getFuture(featuresMap,featuresMapCoastalAreas, periode.getAfectacions()[k], listOfGreenRegions, listOfRegionsWithAfectaciones));
						}	
						for(Long idGreen:listOfGreenRegions){
							featuresList.add(getFutureGreenRegions(featuresMap,featuresMapCoastalAreas, idGreen));									
						}
						
						geojson.setFeatures(featuresList.toArray(new Feature[0]));						
						String jsonInString = getGeojson(geojson);
						geojsonList.add(jsonInString);						
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
						sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
						Date dateExpires = sdf.parse(aviso.getDataFi().replace('/', '-')); 
						long infoGetExpiresEpoch = dateExpires.getTime();						
						Date dateOnSet = sdf.parse(aviso.getDataInici().replace('/', '-'));
						long infoGetOnSetEpoch = dateOnSet.getTime();						
						BigDecimal diffTime = new BigDecimal(infoGetExpiresEpoch-infoGetOnSetEpoch);
						diffTime = diffTime.divide(new BigDecimal(1000*3600),RoundingMode.HALF_UP);
						timeSpan[cat] = diffTime;
						cat++;						
						leadTimes[cat] = (Util.getLeadTimeFromDate(leadTime));
					}
					
				}			
				this.setLeadTime(removeNull(leadTimes));
				this.setTimeSpan(removeNullBD(timeSpan));
				setTemporalReferenceStart(Util.stringToDateTime(aviso.getDataPublicacio()).toString());				
				setTemporalReferenceEnd((selectMaxLeadTime(leadTimes)));				
				setTemporalreferenceDateofcreation(Util.stringToDateTime(aviso.getDataEmisio()).toString()); 				
				setTemporalreferenceDateofpublication(Util.stringToDateTime(aviso.getDataPublicacio()).toString());				
				setTemporalreferenceDateoflastrevision(Util.stringToDateTime(aviso.getDataEmisio()).toString()); 				
				iscatalunya = true;
				String result= writeDBAndsendToIDI(null, geojsonList, now);
			}						
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			closeDBSession();
		}
	}
	
	private Feature getFutureGreenRegions(HashMap<Long, Feature> featuresMap, HashMap<Long, Feature> featuresMapCoastalAreas, Long idGreen) {
		
		Feature result= new Feature();
		result = featuresMap.get((long)idGreen);	
		String areaName = "";
		if (result==null){
			result = featuresMapCoastalAreas.get((long)idGreen);
			areaName=(String) featuresMapCoastalAreas.get((long)idGreen).getProperties().get("area_name");		
		}else {
			result = featuresMap.get((long)idGreen);
			areaName=(String) featuresMap.get((long)idGreen).getProperties().get("area_name");			
		}
		Afectacions afectacion = new Afectacions();		
		afectacion.setAuxiliar(false);
		afectacion.setIdLlindar((long) 0);
		afectacion.setIdRegio((long) idGreen);
		afectacion.setNivell((long) 0);
		afectacion.setNomLlindar("");
		afectacion.setNomRegio(areaName);
		afectacion.setPerill((long) 4);		
		addFutureProperties(result,afectacion);
		return result;
	}

	private void populateGlobalIdList(List<Long> listOfglobalId) {
		for(int i=0; i<42;i++){
			listOfglobalId.add((long) (i+1));
		}
		for(int i=87; i<99;i++){
			listOfglobalId.add((long) (i+1));
		}
	}

	private String selectMaxLeadTime(String[] leadTimes) throws ParseException {
		if(leadTimes.length == 0 || leadTimes == null) return null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		String maxLeadTime = "";
		long maxExpires = 0L;		
		for(int i=0; i<leadTimes.length; i++) {
			if(leadTimes[i] == null) continue;
			Date dateExpires = sdf.parse(leadTimes[i]); 
			long infoGetExpiresEpoch = dateExpires.getTime();
			if(infoGetExpiresEpoch>maxExpires){
				maxExpires = infoGetExpiresEpoch;
				maxLeadTime = leadTimes[i];
			}
		}
		return maxLeadTime;
		
	}
	
	private int getIdTask(String idMeteor){
		switch(idMeteor){
				case LemsWebConstant.CATALUNYA_RAIN_ACCUMULATION_ALERT: return LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_ACCUMULATION_ALERT;
				case LemsWebConstant.CATALUNYA_RAIN_INTENSITY_ALERT: return LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_RAIN_INTENSITY_ALERT;
				case LemsWebConstant.CATALUNYA_WIND_ALERT: return LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_WIND_ALERT;
				case LemsWebConstant.CATALUNYA_HEAT_WAVE_ALERT: return LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_HEAT_WAVE_ALERT;
				case LemsWebConstant.CATALUNYA_STORM_SURGES_ALERT: return LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_STORM_SURGES_ALERT;
				case LemsWebConstant.CATALUNYA_COLD_WAVE_ALERT: return LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_COLD_WAVE_ALERT;
				case LemsWebConstant.CATALUNYA_SNOW_ALERT: return LemsWebConstant.TASK_CATALUNYA_WEATHER_DANGER_FORECAST_SNOW_ALERT;
				default: 
					return -1;
		}
	}
	
		
	private EpisodisDto[] callWsEpisodis(){				
		HttpGet httpGet = new HttpGet(LemsWebConstant.ENDPOINT_WS_CATALUNYA_ALTER);
		HttpClient client = HttpClientBuilder.create().build();		
	    ObjectMapper mapper = new ObjectMapper();
		try{
			String body=null;
			client = HttpClientBuilder.create().build();
			HttpResponse response = client.execute(httpGet);
			body = EntityUtils.toString( response.getEntity(), "UTF-8");
			return mapper.readValue(body, EpisodisDto[].class);
		}
		catch (ClientProtocolException e){
			return null;
		}
		catch (IOException e){
			return null;
		}
		finally{
		}
	}
	
	
	public void insertIncomingJsonToDb(String json){
		String mailSubject = CLASSNAME+"::insertIncomingJsonToDb: "+json;
		if(!listCatalunyaJsonOnDb.contains(json)){
			try{
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				Catalunyaincomingjson catalunyaIncomingJson = new Catalunyaincomingjson();			    
				catalunyaIncomingJson.setReadingdate(format.format(new Date()));
				catalunyaIncomingJson.setJson(json);
				catalunyaIncomingJson.setJsoninbytes(json.getBytes());
				getCatalunyaincomingjsonMapper().insertSelective(catalunyaIncomingJson);
				getSession().commit();
			} catch (Exception e) {
				getSession().rollback();
				String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
				MailHelper.postMail(mailSubject, mailText);
			} 
		} else{
		}
	}
	
	private Avisos getLastAvisos(Avisos[] avisos){
		Avisos aviso=avisos[0];
		for (int i=0;i<avisos.length;i++){
			if (avisos[i].getIdAvis().longValue() >aviso.getIdAvis().longValue())
				aviso=avisos[i];
		}
		return aviso;
	}
	
	
	private Feature getFuture(HashMap<Long, Feature> featuresMap,HashMap<Long, Feature> featuresMapCoastalAreas,Afectacions afectacions, List<Long> listOfGreenRegions, List<Long> listOfRegionsWithAfectaciones){
		Feature result=featuresMap.get(afectacions.getIdRegio());		
		if (result==null){
			result=featuresMapCoastalAreas.get(afectacions.getIdRegio());
			if(result==null) {
				return null;
			}		
		}else {
			result=featuresMap.get(afectacions.getIdRegio());			
		}
		if(listOfGreenRegions.contains(new Long(afectacions.getIdRegio()))) {
			Boolean removed = listOfGreenRegions.remove(new Long(afectacions.getIdRegio()));
		}
		if(listOfRegionsWithAfectaciones.contains(afectacions.getIdRegio().intValue())){
			listOfRegionsWithAfectaciones.add(afectacions.getIdRegio());
		}
		addFutureProperties(result,afectacions);
		return result;
	}
	
	private void addFutureProperties(Feature feature,Afectacions afectacions){
		switch (afectacions.getPerill().intValue()) {
			case 1: feature.getProperties().put("fill",LemsWebConstant.COLOR_YELLOW);
					this.setHazardLevel("2");
					this.setHazardLevelDescription("peril 1");
					break;
			case 2: feature.getProperties().put("fill",LemsWebConstant.COLOR_ORANGE);
					this.setHazardLevel("3");
					this.setHazardLevelDescription("peril 2");
					break;
			case 3: feature.getProperties().put("fill",LemsWebConstant.COLOR_RED);
					this.setHazardLevel("4");
					this.setHazardLevelDescription("peril 3");
					break;
			case 4: feature.getProperties().put("fill",LemsWebConstant.COLOR_GREEN);
					this.setHazardLevel("1");
					this.setHazardLevelDescription("no peril");
					break;
		}
		addCommonProperties(feature);
		addGeneralProperties(feature);
	}
	
	private LocalDateTime getLeadTime(LocalDateTime dateTime, Periodes periode) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		LocalDateTime date=null;
		switch(periode.getIdPeriode().intValue()){
			case 1: date=dateTime.with(LocalTime.of(6, 0)); 
				break;
			case 2: date=dateTime.with(LocalTime.of(12, 0)); 
				break;
			case 3: date=dateTime.with(LocalTime.of(18, 0)); 
				break;
			case 4: date=dateTime.with(LocalTime.of(23, 59));
				break;
			default:date=dateTime;	
				break;
		}		
		return date;
	}
	
	private String[] removeNull(String[] initialString) {
		if(initialString.length<1) return null;
		List<String> list = new ArrayList<String>();
	    for(String s : initialString) {
	       if(s != null && s.length() > 0) {
	          list.add(s);
	       }
	     }
	    initialString = list.toArray(new String[list.size()]);	  
		return initialString;
	}
	
	private BigDecimal[] removeNullBD(BigDecimal[] initialString) {
		if (initialString.length < 1)
			return null;
		List<BigDecimal> list = new ArrayList<BigDecimal>();
		for (BigDecimal s : initialString) {
			if (s != null) {
				list.add(s);
			}
		}
		initialString = list.toArray(new BigDecimal[list.size()]);
		return initialString;
	}
	
}
