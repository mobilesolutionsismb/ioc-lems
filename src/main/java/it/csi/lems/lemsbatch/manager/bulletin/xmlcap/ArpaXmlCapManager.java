package it.csi.lems.lemsbatch.manager.bulletin.xmlcap;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.http.client.ClientProtocolException;

import it.csi.lems.lemsbatch.dto.ws.uk.Geojson;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson.Feature;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert;
import it.csi.lems.lemsbatch.helper.MailHelper;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;

public class ArpaXmlCapManager extends XmlCapManager {

	final static String CLASSNAME 	= LemsWebConstant.CLASSNAME_ARPA_XML;
	final static String NOWARNINGS 	= LemsWebConstant.ARPA_NO_WARNINGS;
	final static String NOVALUES 	= LemsWebConstant.ARPA_NO_VALUES;
	String severity = null;

	@Override
	protected void process() {

		String mailSubject = CLASSNAME + "::process";
		Alert alert = null;
		String jsonInString = null;
		String jsonInString36 = null;
		boolean isReplicationOfAnEqualBulletin = Boolean.FALSE;

		try {
			ZonedDateTime now = ZonedDateTime.now();
			int idLocal = getIdTask();
			JAXBContext jaxbContext;
			jaxbContext = JAXBContext.newInstance(Alert.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			HashMap<String, Alert.Info> infoArea = new HashMap<>();
			if (LemsWebConstant.TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN == idLocal) {
				URLConnection urlConnection;
				urlConnection = new URL(LemsWebConstant.URL_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN).openConnection();
				alert = (Alert) jaxbUnmarshaller.unmarshal(urlConnection.getInputStream());
				if (alert.getIdentifier() != null) {
					ArpaIdentifier = alert.getIdentifier();
				}
			} else if (idLocal == LemsWebConstant.TASK_ARPA_HYDROMETRIC_OVERTOP_ALERT) {
				URLConnection urlConnection;
				urlConnection = new URL(LemsWebConstant.URL_ARPA_HYDROMETRIC_ALERT_BULLETIN).openConnection();
				alert = (Alert) jaxbUnmarshaller.unmarshal(urlConnection.getInputStream());
			}

			for (Alert.Info info : alert.getInfo()) {
				String codArea = info.getArea().get(0).getAreaDesc();
				infoArea.put(codArea, info);
			}
			Geojson jsonCap = new Geojson();
			Geojson jsonCap36 = new Geojson();
			if (LemsWebConstant.TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN == idLocal) {
				jsonCap = getGeojson(LemsWebConstant.RESOURCE_ARPA_ALERT);
				jsonCap36 = getGeojson(LemsWebConstant.RESOURCE_ARPA_ALERT);
			} else if (LemsWebConstant.TASK_ARPA_HYDROMETRIC_OVERTOP_ALERT == idLocal) {
				jsonCap = getGeojson(LemsWebConstant.RESOURCE_ARPA_HYDROMETRIC_ALERT);
			}

			Feature[] features = jsonCap.getFeatures();
			Feature[] features36 = jsonCap36.getFeatures();

			ArrayList<Feature> featuresList = new ArrayList<>();
			HashMap<String, String> warningMap = new HashMap<>();
			List<HashMap<String, String>> listWarningMap = new ArrayList<>();

			ArrayList<Feature> featuresList36 = new ArrayList<>();
			HashMap<String, String> warningMap36 = new HashMap<>();
			List<HashMap<String, String>> listWarningMap36 = new ArrayList<>();

			if (idLocal == LemsWebConstant.TASK_ARPA_HYDROGEOLOGICAL_ALERT_BULLETIN) {
				for (int i = 0; i < features.length; i++) {
					severity = null;
					HashMap<String, Object> properties = features[i].getProperties();
					HashMap<String, Object> properties36 = features36[i].getProperties();

					String codArea = (String) properties.get("area_cod");

					String hazardName = null;
					hazardName = "ExtremeWeather";

					if (codArea != null) {
						Alert.Info info = infoArea.get(codArea);
						boolean surveillanceLevelRed = Boolean.FALSE;
						if (info != null) {

							if (info.getEvent().equalsIgnoreCase("NESSUNA ALLERTA")
									|| info.getEvent().equalsIgnoreCase("VERDE")) {
								this.setHazardLevel("1");
								this.setHazardLevelDescription("NESSUNA ALLERTA");
								properties.put("fill", LemsWebConstant.COLOR_GREEN);
								properties36.put("fill", LemsWebConstant.COLOR_GREEN);
							} else if (info.getEvent().equalsIgnoreCase("ALLERTA BIANCA")) {
								this.setHazardLevel("1");
								this.setHazardLevelDescription("NESSUNA ALLERTA");
								properties.put("fill", LemsWebConstant.COLOR_GREEN);
								properties36.put("fill", LemsWebConstant.COLOR_GREEN);
							} else if (info.getEvent().equalsIgnoreCase("ALLERTA VERDE")) {
								this.setHazardLevel("1");
								this.setHazardLevelDescription("ALLERTA VERDE");
								properties.put("fill", LemsWebConstant.COLOR_GREEN);
								properties36.put("fill", LemsWebConstant.COLOR_GREEN);
							} else if (info.getEvent().equalsIgnoreCase("ALLERTA GIALLA")
									|| info.getEvent().equalsIgnoreCase("GIALLO")) {
								this.setHazardLevel("2");
								this.setHazardLevelDescription("ALLERTA GIALLA");
								properties.put("fill", LemsWebConstant.COLOR_YELLOW);
								properties36.put("fill", LemsWebConstant.COLOR_YELLOW);
							} else if (info.getEvent().equalsIgnoreCase("ALLERTA ARANCIONE")
									|| info.getEvent().equalsIgnoreCase("ARANCIONE")) {
								this.setHazardLevel("3");
								this.setHazardLevelDescription("ALLERTA ARANCIONE");
								properties.put("fill", LemsWebConstant.COLOR_ORANGE);
								properties36.put("fill", LemsWebConstant.COLOR_ORANGE);
							} else if (info.getEvent().equalsIgnoreCase("ALLERTA ROSSA")
									|| info.getEvent().equalsIgnoreCase("ROSSO")) {
								this.setHazardLevel("4");
								this.setHazardLevelDescription("ALLERTA ROSSA");
								properties.put("fill", LemsWebConstant.COLOR_RED);
								properties36.put("fill", LemsWebConstant.COLOR_RED);
							}

							if (info.getParameter() != null) {
								listWarningMap = new ArrayList<>();
								listWarningMap36 = new ArrayList<>();
								properties.put("following_36h", "-");
								properties.put("criticality_type", "-");
								properties.put("description", "-");
								properties.put("relevant_events", "-");
								properties.put("surveillance", "-");
								properties36.put("following_36h", "-");
								properties36.put("criticality_type", "-");
								properties36.put("description", "-");
								properties36.put("relevant_events", "-");
								properties36.put("surveillance", "-");

								for (Alert.Info.Parameter param : info.getParameter()) {
									warningMap = new HashMap<>();
									warningMap36 = new HashMap<>();

									switch (param.getValueName().toUpperCase()) {
									case "EFFETTI SUL TERRITORIO":
										properties.put("description", param.getValue().toLowerCase());
										break;
									case "IDRAULICO_1224":
										warningMap.put("type", "RainFlood");
										warningMap.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap.add(warningMap);
										}
										break;
									case "IDRAULICO_2436":
										warningMap36.put("type", "RainFlood");
										warningMap36.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap36.add(warningMap36);
										}
										break;
									case "IDROGEOLOGICO_1224":
										warningMap.put("type", "Landslide");
										warningMap.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap.add(warningMap);
										}
										break;
									case "IDROGEOLOGICO_2436":
										warningMap36.put("type", "Landslide");
										warningMap36.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap36.add(warningMap36);
										}
										break;
									case "TEMPORALI_1224":
										warningMap.put("type", "Storm");
										warningMap.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap.add(warningMap);
										}
										break;
									case "TEMPORALI_2436":
										warningMap36.put("type", "Storm");
										warningMap36.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap36.add(warningMap36);
										}
										break;
									case "NEVE_1224":
										warningMap.put("type", "Snow");
										warningMap.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap.add(warningMap);
										}
										break;
									case "NEVE_2436":
										warningMap36.put("type", "Snow");
										warningMap36.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap36.add(warningMap36);
										}
										break;
									case "VALANGHE_1224":
										warningMap.put("type", "Avalanches");
										warningMap.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap.add(warningMap);
										}
										break;
									case "VALANGHE_2436":
										warningMap36.put("type", "Avalanches");
										warningMap36.put("value",
												translateWarningNewBulletin(param.getValue().toLowerCase()));
										if (!translateWarningNewBulletin(param.getValue().toLowerCase())
												.equalsIgnoreCase(NOVALUES)
												&& !translateWarningNewBulletin(param.getValue().toLowerCase())
														.equalsIgnoreCase(NOWARNINGS)) {
											listWarningMap36.add(warningMap36);
										}
										break;
									}
								}

								warningMap = new HashMap<>();
								warningMap36 = new HashMap<>();

								if (!listWarningMap.isEmpty()) {
									properties.put("warning_levels", orderListByWarningValue(listWarningMap));
									properties36.put("warning_levels", orderListByWarningValue(listWarningMap36));
								} else {
									properties.put("warning_levels", orderListByWarningValue(listWarningMap));
									properties36.put("warning_levels", orderListByWarningValue(listWarningMap36));
								}								
							}
						}
						if (mapArpaAlert.get(alert.getIdentifier()) != null) {
							isReplicationOfAnEqualBulletin = Boolean.TRUE;
							if (mapArpaAlert.get(alert.getIdentifier()).getTemporalerefenceDateoflastrevision() != null
									&& mapArpaAlert.get(alert.getIdentifier()).getTemporalerefenceDateoflastrevision()
											.equals(alert.getSent())) {
								setDateLastRevision(Util.getStringTimeForIDI(alert.getSent()));
								isReplicationOfAnEqualBulletin = Boolean.TRUE;
							} else {
								setDateLastRevision(now.format(DateTimeFormatter.ISO_INSTANT));
							}
						} else {
							setDateLastRevision(Util.getStringTimeForIDI(alert.getSent()));
						}
						StringBuilder start_dates = new StringBuilder();
						start_dates.append("[\"");
						GregorianCalendar calendar_ = info.getOnset().toGregorianCalendar();
						XMLGregorianCalendar leadtimeDate_ = DatatypeFactory.newInstance()
								.newXMLGregorianCalendar(calendar_);
						start_dates.append(leadtimeDate_.normalize().toString()).append("\", \"");
						calendar_.add(Calendar.HOUR, 24);
						leadtimeDate_ = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar_);
						start_dates.append(leadtimeDate_.normalize().toString()).append("\"]");

						StringBuilder end_dates = new StringBuilder();
						calendar_ = info.getExpires().toGregorianCalendar();
						XMLGregorianCalendar leadtimeDate1 = DatatypeFactory.newInstance()
								.newXMLGregorianCalendar(calendar_);
						calendar_.add(Calendar.HOUR, -12);
						XMLGregorianCalendar leadtimeDate2 = DatatypeFactory.newInstance()
								.newXMLGregorianCalendar(calendar_);
						end_dates.append("[\"").append(leadtimeDate2.normalize().toString()).append("\", \"")
								.append(leadtimeDate1.normalize().toString()).append("\"]");
						setTemporalReferenceStart(Util.getStringTimeForIDI(info.getOnset()));
						setTemporalReferenceEnd(Util.getStringTimeForIDI(info.getExpires()));
						setTemporalreferenceDateofcreation(getCreationDate());
						setTemporalreferenceDateofpublication(getDateLastRevision());
						setTemporalreferenceDateoflastrevision(getDateLastRevision());
						setDateStart(getTemporalReferenceStart());
						setDateEnd(getTemporalReferenceEnd());
						setCreationDate(Util.getStringTimeForIDI(alert.getSent()));
						if (hazardName != null) {
							switch (hazardName.toLowerCase()) {
							case "piogge":
								this.setHazardGLIDECode("OT");
								break;
							case "temporali":
								this.setHazardGLIDECode("ST");
								break;
							case "nevicate":
								this.setHazardGLIDECode("OT");
								break;
							case "caldo anomalo":
								this.setHazardGLIDECode("HW");
								break;
							case "freddo anomalo":
								this.setHazardGLIDECode("CW");
								break;
							case "vento":
								this.setHazardGLIDECode("VW");
								break;
							case "extremeweather":
								this.setHazardGLIDECode("OT");
								break;
							default:
								this.setHazardGLIDECode("FL");
							}
						}
						this.setHazardName(hazardName);
						String[] leadTime = new String[2];
						BigDecimal[] timeSpan = new BigDecimal[2];
						GregorianCalendar calendar = info.getOnset().toGregorianCalendar();
						calendar.add(Calendar.HOUR, 24);
						XMLGregorianCalendar leadtimeDate = DatatypeFactory.newInstance()
								.newXMLGregorianCalendar(calendar);
						leadTime[0] = Util.getStringTimeForIDI(leadtimeDate);
						calendar.add(Calendar.HOUR, 12);
						leadtimeDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
						leadTime[1] = Util.getStringTimeForIDI(leadtimeDate);
						this.setLeadTime(leadTime);
						if (info.getExpires() != null && info.getOnset() != null) {
							timeSpan[0] = new BigDecimal(24);
							timeSpan[1] = new BigDecimal(12);
						} else {
							timeSpan[0] = BigDecimal.ZERO;
							timeSpan[0] = BigDecimal.ZERO;
						}
						this.setTimeSpan(timeSpan);
						addCommonProperties(features[i]);
						addCommonProperties(features36[i]);
						DatatypeFactory df = DatatypeFactory.newInstance();
						GregorianCalendar calendar_1 = info.getOnset().toGregorianCalendar();
						calendar_1.add(Calendar.HOUR_OF_DAY, 23);
						XMLGregorianCalendar xgc = df.newXMLGregorianCalendar(
								new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(calendar_1.getTime()));
						properties.put("onset", Util.getStringTimeForIDI(info.getOnset()));
						properties.put("expires", Util.getStringTimeForIDI(xgc));
						properties36.put("onset", Util.getStringTimeForIDI(xgc));
						properties36.put("expires", Util.getStringTimeForIDI(info.getExpires()));
						properties.put("dateOfAcquisition", "");
						properties36.put("dateOfAcquisition", "");

						if (alert.getSent() != null) {
							properties.put("dateOfCreation", Util.getStringTimeForIDI(alert.getSent()));
							properties.put("dateOfPubblication", Util.getStringTimeForIDI(alert.getSent()));
							properties.put("dateOfLastRevision", Util.getStringTimeForIDI(alert.getSent()));
							properties36.put("dateOfCreation", Util.getStringTimeForIDI(alert.getSent()));
							properties36.put("dateOfPubblication", Util.getStringTimeForIDI(alert.getSent()));
							properties36.put("dateOfLastRevision", Util.getStringTimeForIDI(alert.getSent()));
						} else {
							properties.put("dateOfCreation", "");
							properties.put("dateOfPubblication", "");
							properties.put("dateOfLastRevision", "");
							properties36.put("dateOfCreation", "");
							properties36.put("dateOfPubblication", "");
							properties36.put("dateOfLastRevision", "");
						}

						if (info.getCategory().size() != 0) {
							properties.put("category", String.join(";", info.getCategory()));
							properties36.put("category", String.join(";", info.getCategory()));
						} else {
							properties.put("category", "");
							properties36.put("category", "");
						}

						if (info.getUrgency() != null) {
							properties.put("urgency", "Expected");
							properties36.put("urgency", "Future");
						} else {
							properties.put("urgency", "Expected");
							properties36.put("urgency", "Future");
						}

						if (info.getSeverity() != null) {
							if (severity != null) {
								properties.put("severity", severity);
								properties36.put("severity", severity);
							} else {
								if (info.getSeverity().equals("Unknown")) {
									properties.put("severity", "Minor");
									properties36.put("severity", "Minor");
								} else {
									properties.put("severity", info.getSeverity());
									properties36.put("severity", info.getSeverity());
								}
							}

						} else {
							properties.put("severity", "");
							properties36.put("severity", "");
						}

						if (info.getCertainty() != null) {
							properties.put("certainty", info.getCertainty());
							properties36.put("certainty", info.getCertainty());
						} else {
							properties.put("certainty", "");
							properties36.put("certainty", "");
						}

						if (alert.getIdentifier() != null) {
							properties.put("identifier", alert.getIdentifier());
							properties36.put("identifier", alert.getIdentifier());
						} else {
							properties.put("identifier", "");
							properties36.put("identifier", "");
						}

						if (alert.getMsgType() != null) {
							properties.put("msgType", alert.getMsgType());
							properties36.put("msgType", alert.getMsgType());
						} else {
							properties.put("msgType", "");
							properties36.put("msgType", "");
						}

						addGeneralProperties(features[i]);
						addGeneralProperties(features36[i]);

						if ("#ff0000".equals(properties.get("marker-color"))) {
							if (surveillanceLevelRed) {
								featuresList.add(features[i]);
								featuresList36.add(features36[i]);
							}
						} else {
							featuresList.add(features[i]);
							featuresList36.add(features36[i]);
						}

					}
				}

			} else if (idLocal == LemsWebConstant.TASK_ARPA_HYDROMETRIC_OVERTOP_ALERT) {
				for (int i = 0; i < infoArea.size(); i++) {
					HashMap<String, Object> properties = features[i].getProperties();
					String codArea = (String) properties.get("area_cod");
					String hazardName = null;
					hazardName = "ExtremeWeather";
					if (codArea != null) {
						Alert.Info info = infoArea.get(codArea);
						boolean surveillanceLevelRed = Boolean.FALSE;
						if (info != null) {
							if (info.getEvent().equalsIgnoreCase("SUPERAMENTO LIVELLO IDROMETRICO")) {
								this.setHazardLevel("1");
								this.setHazardLevelDescription("SUPERAMENTO LIVELLO IDROMETRICO");
								properties.put("fill", LemsWebConstant.COLOR_RED);
							}

							setDateStart(Util.getStringTimeForIDI(info.getOnset()));
							setDateEnd(Util.getStringTimeForIDI(info.getExpires()));
							setCreationDate(now.format(DateTimeFormatter.ISO_INSTANT));
							setDateLastRevision(now.format(DateTimeFormatter.ISO_INSTANT));

							setTemporalReferenceStart(Util.getStringTimeForIDI(info.getOnset()));
							setTemporalReferenceEnd(Util.getStringTimeForIDI(info.getExpires()));

							setTemporalreferenceDateofcreation(now.format(DateTimeFormatter.ISO_INSTANT));
							setTemporalreferenceDateofpublication(now.format(DateTimeFormatter.ISO_INSTANT));
							setTemporalreferenceDateoflastrevision(now.format(DateTimeFormatter.ISO_INSTANT));

							this.setHazardName(hazardName);

							String[] leadTime = new String[1];
							BigDecimal[] timeSpan = new BigDecimal[1];

							leadTime[0] = Util.getStringTimeForIDI(info.getExpires());
							this.setLeadTime(leadTime);
							if (info.getExpires() != null && info.getOnset() != null) {
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								Date dateExpires = sdf.parse(info.getExpires().toString());
								long infoGetExpiresEpoch = dateExpires.getTime();
								Date dateOnSet = sdf.parse(info.getOnset().toString());
								long infoGetOnSetEpoch = dateOnSet.getTime();
								BigDecimal diffTime = new BigDecimal(infoGetExpiresEpoch - infoGetOnSetEpoch);
								diffTime = diffTime.divide(new BigDecimal(1000 * 3600), RoundingMode.HALF_UP);
								timeSpan[0] = diffTime;
							} else {
								timeSpan[0] = BigDecimal.ZERO;
							}
							this.setTimeSpan(timeSpan);
							addCommonProperties(features[i]);

							addXmlCapAndStyleProperties(features[i], properties, info, alert);

							if ("#ff0000".equals(properties.get("marker-color"))) {
								if (surveillanceLevelRed) {
									featuresList.add(features[i]);
								}
							} else
								featuresList.add(features[i]);
							;
						}

					}
				}

			}
			jsonCap.setFeatures(featuresList.toArray(new Feature[0]));
			jsonCap36.setFeatures(featuresList36.toArray(new Feature[0]));
			jsonInString = getGeojson(jsonCap);
			jsonInString36 = getGeojson(jsonCap36);
			List<String> geojsonList = new ArrayList<>();
			geojsonList.add(jsonInString);
			geojsonList.add(jsonInString36);
			if (isReplicationOfAnEqualBulletin) {} 
			else {
				if (jsonCap.getFeatures().length != 0) {
					isArpa = Boolean.TRUE;
					String result = writeDBAndsendToIDI(alert, geojsonList, now);
				} else {}
			}
		} catch (ClientProtocolException e) {
			String mailText = "ClientProtocolException message:" + e.getMessage() + " stacktrace:"
					+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (IOException e) {
			String mailText = "IOException message:" + e.getMessage() + " stacktrace:" + Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (Exception e) {
			String mailText = "Exception message:" + e.getMessage() + " stacktrace:" + Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} finally {
			closeDBSession();
		}
	}

	private List<HashMap<String, String>> orderListByWarningValue(List<HashMap<String, String>> listWarningMap) {
		if (listWarningMap.size() == 0 || listWarningMap.size() == 1) {
			return listWarningMap;
		} else {
			List<HashMap<String, String>> orderedList = new ArrayList<>();
			List<HashMap<String, String>> yellowList = new ArrayList<>();
			List<HashMap<String, String>> orangeList = new ArrayList<>();
			List<HashMap<String, String>> redList = new ArrayList<>();
			for (HashMap<String, String> h : listWarningMap) {
				if (h.get("type") != null && h.get("value") != null) {
					if (h.get("value").equals("moderate")) {
						yellowList.add(h);
					} else if (h.get("value").equals("severe")) {
						orangeList.add(h);
					} else if (h.get("value").equals("extreme")) {
						redList.add(h);
					}
				}
			}
			if (yellowList.size() > 0)
				severity = "Moderate";
			if (orangeList.size() > 0)
				severity = "Severe";
			if (redList.size() > 0)
				severity = "Extreme";

			for (HashMap<String, String> r : redList) {
				orderedList.add(r);
			}
			for (HashMap<String, String> o : orangeList) {
				orderedList.add(o);
			}
			for (HashMap<String, String> y : yellowList) {
				orderedList.add(y);
			}
			if (orderedList.size() == listWarningMap.size()) {
				return orderedList;
			}
			return listWarningMap;
		}
	}

	private String translateWarningNewBulletin(String warning) {
		String result = "";
		if ("bianco".equalsIgnoreCase(warning))
			result = NOVALUES;
		if ("verde".equalsIgnoreCase(warning))
			result = NOWARNINGS;
		if ("giallo".equalsIgnoreCase(warning))
			result = "moderate";
		if ("arancione".equalsIgnoreCase(warning))
			result = "severe";
		if ("rosso".equalsIgnoreCase(warning))
			result = "extreme";
		return result;
	}

	@Override
	public void insertDataDB(Object data, int serviceId, Date today) {
		try {
			if (data != null) {
				super.insertDataDB(data, serviceId, today);
				super.commit();
			}
		} catch (Exception e) {
			getSession().rollback();
			String mailSubject = CLASSNAME + "::insertDataDB";
			String mailText = "Exception message:" + e.getMessage() + " stacktrace:" + Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		}
	}
}
