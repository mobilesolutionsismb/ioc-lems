package it.csi.lems.lemsbatch.manager.bulletin.xmlcap;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import it.csi.lems.lemsbatch.dto.mybatis.Capalerts;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalertareageocodes;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalertareas;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalertparameters;
import it.csi.lems.lemsbatch.dto.mybatis.Capinfoalerts;
import it.csi.lems.lemsbatch.dto.mybatis.Relationalcategorymapping;
import it.csi.lems.lemsbatch.dto.mybatis.Relationalresponsetypemapping;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson.Feature;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert;
import it.csi.lems.lemsbatch.helper.ApiAlertsHelper;
import it.csi.lems.lemsbatch.manager.AlertManager;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;

public abstract class XmlCapManager extends AlertManager {

	final static String CLASSNAME = LemsWebConstant.CLASSNAME_XML_CAP;

	public XmlCapManager() {
		super();
	}

	public void insertDataDB(Object data, int serviceId, Date today) {

		if (data != null && today != null) {
			Alert alert = (Alert) data;

			Capalerts capalerts = ApiAlertsHelper.xmlCapToCapalerts(alert, serviceId, today, mapMsgType, mapStatus,
					mapScope);
			getCapalertsMapper().insertSelective(capalerts);

			if (alert.getInfo() != null)
				for (Alert.Info info : alert.getInfo()) {

					Capinfoalerts capinfoalerts = ApiAlertsHelper.xmlCapToCapinfoalerts(info, capalerts.getId(), today,
							mapUrgency, mapSeverity, mapCertainty);
					getCapinfoalertsMapper().insertSelective(capinfoalerts);

					ArrayList<Capinfoalertparameters> listCapinfoalertparameters = new ArrayList<>();
					if (info.getParameter() != null) {
						for (Alert.Info.Parameter parameter : info.getParameter()) {
							Capinfoalertparameters record = ApiAlertsHelper.xmlCapToCapinfoalertparameters(parameter,
									capinfoalerts.getId(), today);
							if (record != null)
								listCapinfoalertparameters.add(record);
						}
						getXmlCapMapper().insertCapinfoalertparameters(listCapinfoalertparameters);
					}

					if (info.getArea() != null) {

						for (Alert.Info.Area area : info.getArea()) {
							Capinfoalertareas record = ApiAlertsHelper.xmlCapToCapinfoalertareas(area,
									capinfoalerts.getId(), today);
							getCapinfoalertareasMapper().insertSelective(record);

							if (area.getGeocode() != null && area.getGeocode().size() > 0) {
								for (Alert.Info.Area.Geocode geocode : area.getGeocode()) {
									Capinfoalertareageocodes geocodeRecord = ApiAlertsHelper
											.xmlCapToCapinfoalertareageocodes(geocode, record.getId(), today);
									getCapinfoalertareageocodesMapper().insertSelective(geocodeRecord);
								}
							}
						}
					}

					if (info.getCategory().size() > 0) {
						ArrayList<Relationalcategorymapping> list = ApiAlertsHelper
								.xmlCapInfoAlertToRelationalCategoryMapping(info, capinfoalerts.getId(), today,
										mapCategory);
						getXmlCapMapper().insertRelationalCategoryMapping(list);
					}

					if (info.getResponseType().size() > 0) {
						ArrayList<Relationalresponsetypemapping> list = ApiAlertsHelper
								.xmlCapInfoAlertToRelationalResponseTypeMapping(info, capinfoalerts.getId(), today,
										mapResponseType);
						getXmlCapMapper().insertRelationalResponseTypeMapping(list);

					}

				}
		}
	}

	protected void commit() {
		getSession().commit();
	}

	public void addXmlCapAndStyleProperties(Feature feature, HashMap<String, Object> properties, Alert.Info info,
			Alert alert) {

		if (info.getExpires() != null) {
			properties.put("expires", Util.getStringTimeForIDI(info.getExpires()));
		} else {
			properties.put("expires", "");
		}
		if (info.getOnset() != null) {
			properties.put("onset", Util.getStringTimeForIDI(info.getOnset()));
		} else {
			properties.put("onset", "");
		}

		properties.put("dateOfAcquisition", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()));

		if (alert.getSent() != null) {
			properties.put("dateOfCreation", Util.getStringTimeForIDI(alert.getSent()));
			properties.put("dateOfPubblication", Util.getStringTimeForIDI(alert.getSent()));
			properties.put("dateOfLastRevision", Util.getStringTimeForIDI(alert.getSent()));
		} else {
			properties.put("dateOfCreation", "");
			properties.put("dateOfPubblication", "");
			properties.put("dateOfLastRevision", "");
		}

		if (info.getCategory().size() != 0) {
			properties.put("category", String.join(";", info.getCategory()));
		} else {
			properties.put("category", "");
		}

		if (info.getUrgency() != null) {
			properties.put("urgency", info.getUrgency());
		} else {
			properties.put("urgency", "");
		}

		if (info.getSeverity() != null) {
			properties.put("severity", info.getSeverity());
		} else {
			properties.put("severity", "");
		}

		if (info.getCertainty() != null) {
			properties.put("certainty", info.getCertainty());
		} else {
			properties.put("certainty", "");
		}

		if (alert.getIdentifier() != null) {
			properties.put("identifier", alert.getIdentifier());
		} else {
			properties.put("identifier", "");
		}

		if (alert.getMsgType() != null) {
			properties.put("msgType", alert.getMsgType());
		} else {
			properties.put("msgType", "");
		}

		// add common property && style
		addGeneralProperties(feature);
	}

}
