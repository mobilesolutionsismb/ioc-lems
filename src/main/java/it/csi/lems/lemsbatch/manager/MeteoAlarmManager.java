package it.csi.lems.lemsbatch.manager;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.client.ClientProtocolException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmactivealert;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmalert;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmurl;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmxmlid;
import it.csi.lems.lemsbatch.dto.ws.CreateAlertAndWarning;
import it.csi.lems.lemsbatch.dto.ws.Token;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert.Info;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert.Info.Area;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert.Info.Area.Geocode;
import it.csi.lems.lemsbatch.dto.ws.xmlcap.Alert.Info.Parameter;
import it.csi.lems.lemsbatch.helper.MailHelper;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;

public class MeteoAlarmManager extends ApiManager {

	final static String CLASSNAME = "MeteoAlarmManager";
	final static String ERROR_IN_THE_WKT = "The WKT shows some syntax issues: The alert is skipped to avoid application errors";
	public Boolean isMeteoAlarm = true;

	public void process() {

		String mailSubject = CLASSNAME + "::process";
		try {
			Token token = getApiToken();
			List<Meteoalarmurl> listNationsUrlTot = this.getXmlcapMapper().selectMeteoAlarm();
			List<Meteoalarmurl> listNationsUrl = new ArrayList<>();
			for (Meteoalarmurl m : listNationsUrlTot) {
				if (m != null && m.getIsvalid() == Boolean.TRUE && m.getAtomurl() != null) {
					listNationsUrl.add(m);
				}
			}
			List<Meteoalarmxmlid> meteoList = getMeteoalarmxmlidMapper().selectByExample(null);

			for (Meteoalarmurl meteoUrl : listNationsUrl) {
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc;
				InputStream url;

				if (new URL(meteoUrl.getAtomurl()).openConnection().getInputStream() == null)
					break;

				url = new URL(meteoUrl.getAtomurl()).openConnection().getInputStream();

				doc = db.parse(url);

				NodeList nodeListAlert = (NodeList) doc.getElementsByTagName("entry");

				for (int i = 0; i < nodeListAlert.getLength(); i++) {
					String CapId = "";
					NodeList child = nodeListAlert.item(i).getChildNodes();
					CapId = nodeListAlert.item(i).getTextContent().split("\n")[1].replace("\\s", "").replace("\t", "");

					for (int j = 1; j < child.getLength(); j++) {
						if ("link".equals(child.item(j).getNodeName())) {

							NamedNodeMap nodeMap = child.item(j).getAttributes();

							Node node = nodeMap.getNamedItem("type");

							if (node != null && "application/cap+xml".equals(node.getTextContent())) {
								node = nodeMap.getNamedItem("href");
								String univoqueId = node.getTextContent().toString().split("POLY/")[1].split("\\.")[0];
								String countryId = univoqueId.split("_")[0];
								if (validCountryId.contains(countryId)) {

									if (!listMeteoAlarmUniqueId.contains(univoqueId)) {
										if (!listMeteoAlarmCAPIdentifier.contains(CapId)) {
											if (!listMeteoAlarmAlertCapIdFromMeteoAlarmAlert.contains(CapId)) {
												insertUniqueIdToDb(countryId, univoqueId, CapId, meteoList);
												letturaXmlCap(node.getTextContent(), token);
											}
										} else {}
									} else {}
								} else {}
							}
						}
					}
				}
				url.close();
			}

		} catch (JAXBException e) {
			String mailText = "JAXBException message:" + e.getMessage() + " stacktrace:" + Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (ClientProtocolException e) {
			String mailText = "ClientProtocolException message:" + e.getMessage() + " stacktrace:"
					+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (IOException e) {
			String mailText = "IOException message:" + e.getMessage() + " stacktrace:" + Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (Exception e) {
			String mailText = "Exception message:" + e.getMessage() + " stacktrace:" + Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} finally {
		}
	}

	private boolean ExpiresBeforeNow(XMLGregorianCalendar expires) throws DatatypeConfigurationException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date());
		long difference = expires.toGregorianCalendar().getTimeInMillis() - c.getTimeInMillis();
		if (difference >= 0) {
			return false;
		} else {
			return true;
		}
	}

	public void insertUniqueIdToDb(String nationCode, String uniqueId, String CAPId, List<Meteoalarmxmlid> meteoList) {
		String mailSubject = CLASSNAME + "::insertUniqueIdToDb: " + nationCode + "_" + uniqueId;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Meteoalarmxmlid meteoalarmxmlid = new Meteoalarmxmlid();

			meteoalarmxmlid.setReadingdate(format.format(new Date()));
			meteoalarmxmlid.setCountryid(nationCode);
			meteoalarmxmlid.setXmluniqueid(uniqueId);
			meteoalarmxmlid.setCapidentifier(CAPId);

			getMeteoalarmxmlidMapper().insertSelective(meteoalarmxmlid);
			getSession().commit();
			meteoList.add(meteoalarmxmlid);

			createMeteoAlarmXmlIdHashMap(meteoList);

		} catch (Exception e) {
			getSession().rollback();
			String mailText = "Exception message:" + e.getMessage() + " stacktrace:" + Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		}
	}

	private void letturaXmlCap(String url, Token token) throws Exception {

		JAXBContext jaxbContext;
		jaxbContext = JAXBContext.newInstance(Alert.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		URLConnection urlConnection = new URL(url).openConnection();
		incomingCharset = usedCharset;
		Alert alert = (Alert) jaxbUnmarshaller.unmarshal(urlConnection.getInputStream());

		String lan = "";
		int size = alert.getInfo().size();

		for (Alert.Info info : alert.getInfo()) {
			if (ExpiresBeforeNow(info.getExpires())) {
				continue;
			}
			lan = info.getLanguage();
			if (countryWithPolygon.contains(url.toString().split("POLY/")[1].split("_")[0])) {
				if (info.getArea() == null || info.getArea().isEmpty() || info.getArea().size() > 1
						&& !url.toString().split("POLY/")[1].split("_")[0].toLowerCase().equals("ro")) {
				} else {
					String descArea = info.getArea().get(0).getAreaDesc();

					if (info.getArea().get(0).getPolygon() == null || info.getArea().get(0).getPolygon().isEmpty()) {
					} else {
						meteoAlarmExitVal = false;
						checkForDuplicate = true;
						calculateCentroid = true;
						manageLanguageAndSend(sendOnlyOneLanguageForEachAlert, size, info, url, alert, token, lan,
								true);
					}
				}
			} else {
				meteoAlarmExitVal = false;
				checkForDuplicate = true;
				calculateCentroid = true;
				manageLanguageAndSend(sendOnlyOneLanguageForEachAlert, size, info, url, alert, token, lan, false);

			}
		}
	}

	private void manageLanguageAndSend(Boolean sendOnlyOneLanguageForEachAlert, int size, Info info, String url,
			Alert alert, Token token, String lan, Boolean hasPolygon) throws Exception {
		if (hasPolygon) {
			if (sendOnlyOneLanguageForEachAlert) {
				if (size > 1) {
					if (("en-gb").equals(lan.toLowerCase()) || ("en-uk").equals(lan.toLowerCase())) {
						createTheAlert(info, url, alert, token);
					}
				} else if (size == 1) {
					createTheAlert(info, url, alert, token);
				}
			} else {
				createTheAlert(info, url, alert, token);
			}
		} else {
			if (sendOnlyOneLanguageForEachAlert) {
				if (size > 1) {

					if (("en-gb").equals(lan.toLowerCase()) || ("en-uk").equals(lan.toLowerCase())) {
						for (Geocode geocode : info.getArea().get(0).getGeocode()) {
							createTheAlertWithNoPolygon(geocode, info, url, alert, token);
						}
					}
				} else if (size == 1) {
					for (Geocode geocode : info.getArea().get(0).getGeocode()) {
						createTheAlertWithNoPolygon(geocode, info, url, alert, token);
					}
				}
			} else {
				for (Geocode geocode : info.getArea().get(0).getGeocode()) {
					createTheAlertWithNoPolygon(geocode, info, url, alert, token);
				}
			}
		}
	}

	public void createTheAlert(Info info, String url, Alert alert, Token token) throws Exception {
		String polygonWkt = convertWgsToWkt(info.getArea().get(0).getPolygon());
		if (ERROR_IN_THE_WKT.equals(polygonWkt)) {
			return;
		}
		if (polygonWkt.toLowerCase().equals("polygon((") || polygonWkt.toLowerCase().contains("multipolygon((()))")) {
			meteoAlarmExitVal = true;
			checkForDuplicate = false;
			calculateCentroid = false;
		}
		Point centroid = null;
		if (calculateCentroid) {
			centroid = getGeometryCentroid(polygonWkt);
		}
		if (meteoAlarmUpdate) {
			updateOrNot(alert, info, polygonWkt, token);
		}
		if (checkForDuplicate) {
			meteoAlarmExitVal = checkForDuplicate(alert, info, polygonWkt);
		}
		if (!meteoAlarmExitVal) {
			setValuesToCreateAlert(alert, info, polygonWkt, url.toString().split("POLY/")[1].split("\\.")[0], centroid);
			String result = sendCreateAlertMeteoAlarm(token);
		}
	}

	public void createTheAlertWithNoPolygon(Geocode geocode, Info info, String url, Alert alert, Token token)
			throws Exception {
		if (geocode.getValueName().toLowerCase().contains("emma")) {
			if (EmmaIdPolygon.containsKey(geocode.getValue())) {
				if (EmmaIdPolygon.get(geocode.getValue()).getPolygonasstring() != null) {
					String polygonWkt = EmmaIdPolygon.get(geocode.getValue()).getPolygonasstring();
					String pointString = EmmaIdPolygon.get(geocode.getValue()).getPoint();
					com.vividsolutions.jts.geom.Coordinate c = new com.vividsolutions.jts.geom.Coordinate();
					c.x = Double.parseDouble(pointString.split(" ")[1].replace("(", ""));
					c.y = Double.parseDouble(pointString.split(" ")[2].replace(")", ""));
					Point centroid = new GeometryFactory().createPoint(c);
					if (meteoAlarmUpdate) {
						updateOrNot(alert, info, polygonWkt, token);
					}
					if (checkForDuplicate) {
						meteoAlarmExitVal = checkForDuplicate(alert, info, polygonWkt);
					}
					if (!meteoAlarmExitVal) {
						setValuesToCreateAlert(alert, info, polygonWkt,
								url.toString().split("POLY/")[1].split("\\.")[0], centroid);
						String result = sendCreateAlertMeteoAlarm(token);
					}
				}
			} else {}
		} else if (geocode.getValueName().toLowerCase().contains("nut")) {
			if (NutPolygon.containsKey(geocode.getValue())) {
				if (NutPolygon.get(geocode.getValue()).getPolygonasstring() != null) {
					String polygonWkt = NutPolygon.get(geocode.getValue()).getPolygonasstring();
					String pointString = EmmaIdPolygon.get(geocode.getValue()).getPoint();
					com.vividsolutions.jts.geom.Coordinate c = new com.vividsolutions.jts.geom.Coordinate();
					c.x = Double.parseDouble(pointString.split(" ")[1].replace("(", ""));
					c.y = Double.parseDouble(pointString.split(" ")[2].replace(")", ""));
					Point centroid = new GeometryFactory().createPoint(c);
					if (meteoAlarmUpdate) {
						updateOrNot(alert, info, polygonWkt, token);
					}
					if (checkForDuplicate) {
						meteoAlarmExitVal = checkForDuplicate(alert, info, polygonWkt);
					}
					if (!meteoAlarmExitVal) {
						setValuesToCreateAlert(alert, info, polygonWkt,
								url.toString().split("POLY/")[1].split("\\.")[0], centroid);
						String result = sendCreateAlertMeteoAlarm(token);
					}
				}
			} else {}
		}

	}

	private boolean checkForDuplicate(Alert alert, Info info, String polygonWkt) {
		for (String key : mapPolygonSentMeteoAlarmAlert.keySet()) {
			String pol = key.split("-.-.-")[1];
			if (pol.equals(polygonWkt)) {
				Meteoalarmalert allerta = mapPolygonSentMeteoAlarmAlert.get(key);
				if (allerta.getEnddate() != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					try {
						Date endDateOld = format.parse(allerta.getEnddate());
						Date startDateOld = format.parse(allerta.getStartdate());
						Date startDateNew = format.parse(info.getOnset().toString());
						Date endDateNew = format.parse(info.getExpires().toString());

						if (startDateNew.equals(startDateOld)) {								
							if (endDateNew.equals(endDateOld)) {
								if (allerta.getTitle() != null) {
									String oldTitle = allerta.getTitle();
									String oldHazard = oldTitle;

									if (info.getHeadline() != null) {
										String newHazard = info.getHeadline();
										if (oldHazard.equals(newHazard)) {
											if (allerta.getLevelofthealarm() != null
													&& !(allerta.getLevelofthealarm().equals(setLevel(
															mapSeverity.get(info.getSeverity()).longValue())))) {
											} else {
												String newLang = null;
												for (String code : mapIsoLanguage.keySet()) {
													String lang = info.getLanguage().split("-")[0];
													if (lang.equals("ne"))
														lang = "nl";
													if (code.equals(lang)) {
														newLang = code;
														break;
													}
												}
												if (allerta.getLanguage() != null && newLang != null
														&& !allerta.getLanguage().equals(newLang)) {
												} else {													
													return true;
												}
											}
										}
									} else {
										if (info.getParameter() != null) {
											for (Parameter p : info.getParameter()) {
												if (p.getValueName().equals("awareness_type")) {
													String[] parts = p.getValue().split("\\; ");
													if (parts[1] != null) {
														String newHazard1 = parts[1].substring(0, 1).toUpperCase()
																+ parts[1].substring(1);
														if (oldHazard.equals(newHazard1)) {
															if (allerta.getLevelofthealarm() != null && !(allerta
																	.getLevelofthealarm().equals(setLevel(mapSeverity
																			.get(info.getSeverity()).longValue())))) {
															} else {																
																return true;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return false;
	}

	private void updateOrNot(Alert alert, Info info, String polygonWkt, Token token) {
		for (String key : mapPolygonMeteoAlarmActiveAlert.keySet()) {
			String pol = key.split("-.-.-")[1];
			if (pol.equals(polygonWkt)) {
				if (mapPolygonMeteoAlarmActiveAlert.get(key).getEnddate() != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					try {
						Date endDateOld = format.parse(mapPolygonMeteoAlarmActiveAlert.get(key).getEnddate());
						Date startDateOld = format.parse(mapPolygonMeteoAlarmActiveAlert.get(key).getStartdate());
						Date startDateNew = format.parse(info.getOnset().toString());
						Date endDateNew = format.parse(info.getExpires().toString());

						if (endDateOld.after(startDateNew)) {
							if (mapPolygonMeteoAlarmActiveAlert.get(key).getTitle() != null) {
								String oldTitle = mapPolygonMeteoAlarmActiveAlert.get(key).getTitle();
								String oldHazard = oldTitle;

								if (info.getHeadline() != null) {
									String newHazard = info.getHeadline();
									if (oldHazard.equals(newHazard)) {
										if (mapPolygonMeteoAlarmActiveAlert.get(key).getLevelofthealarm() != null
												&& !(mapPolygonMeteoAlarmActiveAlert.get(key).getLevelofthealarm()
														.equals(setLevel(
																mapSeverity.get(info.getSeverity()).longValue())))) {
											
											if (startDateOld.before(new Date())) {
												updateOldAlert(mapPolygonMeteoAlarmActiveAlert.get(key), token, false);
											} else {
												updateOldAlert(mapPolygonMeteoAlarmActiveAlert.get(key), token, true);
											}

										} else {
											if (startDateOld.before(new Date())) {
												updateOldAlert(mapPolygonMeteoAlarmActiveAlert.get(key), token, false);
											} else {
												updateOldAlert(mapPolygonMeteoAlarmActiveAlert.get(key), token, true);
											}
										}
									}
								} else {
									if (info.getParameter() != null) {
										for (Parameter p : info.getParameter()) {
											if (p.getValueName().equals("awareness_type")) {
												String[] parts = p.getValue().split("\\; ");
												if (parts[1] != null) {
													String newHazard1 = parts[1].substring(0, 1).toUpperCase()
															+ parts[1].substring(1);
													if (oldHazard.equals(newHazard1)) {
														
														if (mapPolygonMeteoAlarmActiveAlert.get(key)
																.getLevelofthealarm() != null
																&& !(mapPolygonMeteoAlarmActiveAlert.get(key)
																		.getLevelofthealarm()
																		.equals(setLevel(
																				mapSeverity.get(info.getSeverity())
																						.longValue())))) {
															if (startDateOld.before(new Date())) {
																updateOldAlert(mapPolygonMeteoAlarmActiveAlert.get(key),
																		token, false);
															} else {
																updateOldAlert(mapPolygonMeteoAlarmActiveAlert.get(key),
																		token, true);
															}
														} else {
															if (startDateOld.before(new Date())) {
																updateOldAlert(mapPolygonMeteoAlarmActiveAlert.get(key),
																		token, false);
															} else {
																updateOldAlert(mapPolygonMeteoAlarmActiveAlert.get(key),
																		token, true);
															}
														}
													}
												}
											}
										}
									}
								}

							} else {
							}
						} else {
						}
					} catch (java.text.ParseException e) {
						e.printStackTrace();
					}
				}
			}
		}

	}

	private String setLevel(Long level) {
		int realValue = 6 - level.intValue();
		String finalstring = "";
		switch (realValue) {
		case 2:
			finalstring = "noSpecialAwarenessRequired";
			break;
		case 3:
			finalstring = "bePrepared";
			break;
		case 4:
			finalstring = "actionRequired";
			break;
		case 5:
			finalstring = "dangerToLife";
			break;

		}
		return finalstring;
	}

	private void updateOldAlert(Meteoalarmactivealert mapPolygonMeteoAlarmActiveAlert, Token token,
			Boolean forceDateStart) {
		try {
			String result = closeMeteoAlarmActiveAlert(token, ZonedDateTime.now(), mapPolygonMeteoAlarmActiveAlert,
					forceDateStart);					
			getMeteoalarmactivealertMapper().deleteByPrimaryKey(mapPolygonMeteoAlarmActiveAlert.getId());
			getSession().commit();
			createMapPolygonActiveMeteoAlarmAlert();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setValuesToCreateAlert(Alert alert, Info info, String polygonWkt, String id, Point centroid)
			throws UnsupportedEncodingException {
		HashMap<String, String> parameterMap = new HashMap<>();
		if (info.getParameter() != null) {
			for (int i = 0; i < info.getParameter().size(); i++) {
				parameterMap.put(info.getParameter().get(i).getValueName(), info.getParameter().get(i).getValue());
			}
		}

		List<Meteoalarmxmlid> meteoList = getMeteoalarmxmlidMapper().selectByExample(null);
		for (Meteoalarmxmlid m : meteoList) {
			if (m.getCapidentifier() != null && m.getCapidentifier().equals(alert.getIdentifier())) {
				mapCapIdentifierId.put(m.getCapidentifier(), m.getId());
			}
		}
		CreateAlertAndWarning alertAndWarning = new CreateAlertAndWarning();

		if (mapCapIdentifierId.get(alert.getIdentifier()) != null) {
			alertAndWarning.setId(mapCapIdentifierId.get(alert.getIdentifier()));
		}
		idForUpdate = mapCapIdentifierId.get(alert.getIdentifier());
		if (info.getOnset().toString() != null) {
			alertAndWarning.setStart(info.getOnset().toString());
		}
		if (info.getExpires().toString() != null) {
			alertAndWarning.setEnd(info.getExpires().toString());
		}
		int receiver[] = { 5 };
		alertAndWarning.setReceivers(receiver);
		alertAndWarning.setCommunicationStatus("Validated");
		if (mapSeverity.get(info.getSeverity()) != null) {
			alertAndWarning.setLevel(mapSeverity.get(info.getSeverity()).longValue());
		} else {
			alertAndWarning.setLevel(new Long(5));
		}
		if (polygonWkt != null) {
			alertAndWarning.setAreaOfInterest(polygonWkt);
		}
		if (centroid != null) {
			alertAndWarning.setLocation(centroid.toString());
		}
		if (alert.getStatus() != null) {
			alertAndWarning.setCAPStatus(alert.getStatus());
		}
		if (info.getCategory().get(0).toString() != null) {
			byte ptext[] = info.getCategory().get(0).toString().getBytes(incomingCharset);
			alertAndWarning.setCAPCategory(new String(ptext, usedCharset));
		}
		if ((info.getResponseType() != null) && (info.getResponseType().size() > 0)) {
			if (info.getResponseType().get(0).toString() != null) {
				alertAndWarning.setCAPResponseType(info.getResponseType().get(0).toString());
			}
		}
		if (info.getInstruction() != null) {
			byte ptext[] = info.getInstruction().toString().getBytes(usedCharset);
			alertAndWarning.setInstruction(new String(ptext, usedCharset));
		}
		if (info.getUrgency() != null) {
			alertAndWarning.setCAPUrgency(info.getUrgency());
		}
		if (info.getCertainty() != null) {
			alertAndWarning.setCAPCertainty(info.getCertainty());
		}
		if (alert.getMsgType() != null) {
			if (info.getCertainty().equals("Unknown") || info.getCertainty().equals("Unlikely")
					|| info.getCertainty().equals("Possible") || info.getCertainty().equals("Likely")) {
				alertAndWarning.setType("Alert");
			} else {
				if (!"Alert".equals(alert.getMsgType())) {
					alertAndWarning.setType("Warning");
				} else {
					alertAndWarning.setType("Alert");
				}
			}
		}
		if (alert.getIdentifier() != null) {
			alertAndWarning.setCAPIdentifier(alert.getIdentifier());
		}
		if (info.getWeb() != null) {
			alertAndWarning.setWeb(info.getWeb());
		}
		alertAndWarning.setSourceOfInformation(LemsWebConstant.SOURCEOFINFOMETEOALARM);

		if (info.getResource().toString() != null) {
			alertAndWarning.setResource(info.getResource().toString());
		}

		if (parameterMap.get("awareness_type") != null) {
			String noSpace = parameterMap.get("awareness_type").replace(" ", "");
			String[] array = noSpace.split(";");

			if (array.length > 1 && array[1] != null) {

				array[1] = array[1].toLowerCase();
				if (array[1].equals("snow-ice")) {
					array[1] = "snow/Ice";
				}
				if (array[1].equals("thunderstorm")) {
					array[1] = "thunderstorms";
				}
				if (array[1].equals("low-temperature")) {
					array[1] = "extreme low temperature";
				}

				if (array[1].equals("high-temperature")) {
					array[1] = "extreme high temperature";
				}

				if ((("Met").equals(alertAndWarning.getCAPCategory()))) {
					if (!("Flood").equals(array[1])) {
						if (!meteoAlarmHazardV2) {
							alertAndWarning.setHazard("ExtremeWeather");
						} else {
							if (MeteoAlarmHazardMap.get(array[1]) != null
									&& MeteoAlarmHazardMap.get(array[1]).getBackendhazard() != null) {
								alertAndWarning.setHazard(MeteoAlarmHazardMap.get(array[1]).getBackendhazard());
							} else {
								alertAndWarning.setHazard("ExtremeWeather");
							}

						}
					} else {
						if (MeteoAlarmHazardMap.get(array[1]) != null
								&& MeteoAlarmHazardMap.get(array[1]).getBackendhazard() != null) {
							alertAndWarning.setHazard(MeteoAlarmHazardMap.get(array[1]).getBackendhazard());
						} else {
							return;
						}
					}
				} else {
					if (MeteoAlarmHazardMap.get(array[1]) != null
							&& MeteoAlarmHazardMap.get(array[1]).getBackendhazard() != null) {
						alertAndWarning.setHazard(MeteoAlarmHazardMap.get(array[1]).getBackendhazard());
					} else {
						return;
					}
				}

			} else {
				alertAndWarning.setHazard("ExtremeWeather");
			}
		}
		if (parameterMap.get("awareness_level") != null) {
			String noSpace = parameterMap.get("awareness_level").replace(" ", "");
			String[] array = noSpace.split(";");
			if (array.length > 1 && array[2] != null) {
				alertAndWarning.setWarningLevel(array[2]);
			} else {
				alertAndWarning.setWarningLevel("unknown");
			}
		}

		if (info.getHeadline() != null && info.getHeadline().toString() != null) {
			byte ptext[] = info.getHeadline().getBytes(incomingCharset);
			alertAndWarning.setTitle(new String(ptext, usedCharset));
		} else {
			String composedTitle = composeTitle(info, alert.getIdentifier().split("\\.")[6]);
			byte ptext[] = composedTitle.getBytes(usedCharset);
			alertAndWarning.setTitle(new String(ptext, usedCharset));
		}

		if (info.getDescription() != null && info.getDescription().toString() != null) {
			String eventForDescription = new String(info.getEvent().getBytes(usedCharset), usedCharset);
			StringBuilder sb = new StringBuilder();
			int val = 0;
			for (Area a : info.getArea()) {
				if (val != 0) {
					sb.append(", ");
				}
				sb.append(new String(a.getAreaDesc().getBytes(usedCharset), usedCharset));
				val++;
			}

			if (("ing text!.").equals(info.getDescription().toLowerCase())
					|| ("ing text!").equals(info.getDescription().toLowerCase())) {
				alertAndWarning.setDescription(new String(eventForDescription.getBytes(usedCharset), usedCharset)
						+ " : " + new String(sb.toString().getBytes(usedCharset), usedCharset));			
			} else {
				alertAndWarning.setDescription(eventForDescription + " : " + sb.toString() + " - "
						+ new String(info.getDescription().getBytes(usedCharset), usedCharset));
			}

		} else {
			alertAndWarning.setDescription(alertAndWarning.getTitle());
		}

		for (String code : mapIsoLanguage.keySet()) {
			String lang = info.getLanguage().split("-")[0];
			if (lang.equals("ne"))
				lang = "nl";
			if (code.equals(lang)) {
				alertAndWarning.setLanguage(code);
				
				break;
			}
		}

		setAlertAndWarning(alertAndWarning);

	}

	private String composeTitle(Info info, String code) {
		StringBuilder composedTitle = new StringBuilder();
		if (info.getParameter() != null && info.getParameter().size() > 0) {
			for (Parameter p : info.getParameter()) {
				if (p.getValueName().equals("awareness_level")) {
					String[] parts = p.getValue().split("\\; ");
					if (parts[1] != null) {
						composedTitle.append(parts[1].substring(0, 1).toUpperCase() + parts[1].substring(1))
								.append(" ");
					}
				}
			}
			for (Parameter p : info.getParameter()) {
				if (p.getValueName().equals("awareness_type")) {
					String[] parts = p.getValue().split("\\; ");
					if (parts[1] != null) {
						composedTitle.append(parts[1].substring(0, 1).toUpperCase() + parts[1].substring(1))
								.append(" ");
					}
				}
			}
		}
		composedTitle.append("Warning for ");
		String nation = new String(mapCodeNations.get(code).getBytes(usedCharset), usedCharset);
		composedTitle.append(nation.substring(0, 1).toUpperCase() + nation.substring(1));
		composedTitle.append(" - ");
		if (info.getArea() != null && info.getArea().get(0) != null && info.getArea().get(0).getAreaDesc() != null) {
			composedTitle.append(info.getArea().get(0).getAreaDesc().substring(0, 1).toUpperCase()
					+ info.getArea().get(0).getAreaDesc().substring(1));
		}
		return new String(composedTitle.toString().getBytes(usedCharset), usedCharset);
	}

	private String convertWgsToWkt(List<String> list) throws ParseException {

		StringBuilder finalWkt = new StringBuilder();
		if (list.size() == 1 && list.get(0) != null) {
			finalWkt = new StringBuilder();
			finalWkt.append("POLYGON((");
			String geometry = list.get(0).replace("[", "").replace("]", "");
			geometry = geometry.replace("\n", "_").replace(" ", "_").replace(",", " ").replace("_", ",");
			if (String.valueOf(geometry.charAt(0)).equals(",")) {
				geometry.substring(1);
			}
			int c = 0;

			String[] array = geometry.split(",");

			if (Double.parseDouble(array[0].split(" ")[0]) > 25 && Double.parseDouble(array[0].split(" ")[0]) < 71
					&& Double.parseDouble(array[0].split(" ")[1]) > -33
					&& Double.parseDouble(array[0].split(" ")[1]) < 45) {
				for (String s : array) {
					if (c != 0)
						finalWkt.append(", ");
					finalWkt.append(s.split(" ")[1] + " " + s.split(" ")[0]);
					c++;
				}
				finalWkt.append("))");
				
			} else {
				return ERROR_IN_THE_WKT;
			}

		} else {
			finalWkt = new StringBuilder();
			finalWkt.append("MULTIPOLYGON((");
			int i = 0;
			for (String geometry : list) {
				if (geometry != null) {
					if (i == 0) {
						finalWkt.append("(");
					} else {
						finalWkt.append(", (");
					}

					String geo = geometry.replace("\n", "_").replace(" ", "_").replace(",", " ").replace("_", ",");
					if (String.valueOf(geo.charAt(0)).equals(",")) {
						geo = geo.substring(1);
					}
					int c = 0;
					String[] array = geo.split(",");

					if (Double.parseDouble(array[0].split(" ")[0]) > 25
							&& Double.parseDouble(array[0].split(" ")[0]) < 71
							&& Double.parseDouble(array[0].split(" ")[1]) > -33
							&& Double.parseDouble(array[0].split(" ")[1]) < 45) {

						if (i != 0)
							finalWkt.append(", (");

						for (String s : array) {
							if (c != 0)
								finalWkt.append(", ");
							finalWkt.append(s.split(" ")[1] + " " + s.split(" ")[0]);
							c++;
						}
					} else {
						return ERROR_IN_THE_WKT;
					}

					finalWkt.append(")");
					i++;
				}

			}
			finalWkt.append("))");
		}

		String finalToReturn = finalWkt.toString().replace(", ()", "").replace("\n", "");
		finalToReturn = finalToReturn.replace("(), ", "");

		return finalToReturn.replace("(, ", "").replace(" (, ", "");
	}

	private Point getGeometryCentroid(String wkt) throws ParseException {

		WKTReader wktr = new WKTReader();
		wkt.replace("(, ", "");
		wkt.replace(" (, ", "");
		Geometry imageGeom = wktr.read(wkt);
		Point centroid = imageGeom.getEnvelope().getCentroid();
		if (wkt.contains("MULTIPOLYGON")) {
			com.vividsolutions.jts.geom.Coordinate c = new com.vividsolutions.jts.geom.Coordinate();
			c.x = Double.parseDouble(wkt.split(" ")[2]);
			c.y = Double.parseDouble(wkt.split(" ")[3].replace(",", ""));
			return centroid;
		} else {

			com.vividsolutions.jts.geom.Coordinate c = new com.vividsolutions.jts.geom.Coordinate();
			c.x = Double.parseDouble(wkt.split(" ")[0].replace("MULTIPOLYGON(((", "").replace("POLYGON((", ""));
			c.y = Double.parseDouble(wkt.split(" ")[1].replace(",", ""));
			return centroid;
		}

	}
}
