package it.csi.lems.lemsbatch.manager;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.codehaus.jackson.map.ObjectMapper;

import it.csi.lems.lemsbatch.dto.mybatis.Hazard;
import it.csi.lems.lemsbatch.dto.mybatis.Isolanguage;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmactivealert;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmalert;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmpolygon;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmurl;
import it.csi.lems.lemsbatch.dto.mybatis.Meteoalarmxmlid;
import it.csi.lems.lemsbatch.dto.mybatis.Severitymapping;
import it.csi.lems.lemsbatch.dto.mybatis.Ukalert;
import it.csi.lems.lemsbatch.dto.mybatis.Ukfloods;
import it.csi.lems.lemsbatch.dto.mybatis.UkfloodsExample;
import it.csi.lems.lemsbatch.dto.ws.ApiAuthentication;
import it.csi.lems.lemsbatch.dto.ws.CreateAlertAndWarning;
import it.csi.lems.lemsbatch.dto.ws.Result;
import it.csi.lems.lemsbatch.dto.ws.ResultGetEmergency;
import it.csi.lems.lemsbatch.dto.ws.Token;
import it.csi.lems.lemsbatch.dto.ws.uk.FloodsAreaDto;
import it.csi.lems.lemsbatch.dto.ws.uk.FloodsDto.Item;
import it.csi.lems.lemsbatch.helper.ApiAlertsHelper;
import it.csi.lems.lemsbatch.helper.MailHelper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.HazardMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.IsolanguageMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.MeteoalarmactivealertMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.MeteoalarmalertMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.MeteoalarmpolygonMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.MeteoalarmurlMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.MeteoalarmxmlidMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.SeveritymappingMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.UkalertMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.generati.UkfloodsMapper;
import it.csi.lems.lemsbatch.mybatis.mapper.ottimizzati.XmlCapMapper;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;

public abstract class ApiManager {

	private 	final 	String 								CLASSNAME = "ApiManager";
	private		final 	String								EMERGENCY_COMMUNICATION_TAIL = "EmergencyCommunication/v2/UpdateAlertAndWarning";
	private 	final 	String 								content_type_v2 = "multipart/form-data; boundary=----WebKitFormBoundarypUkY3YHXFOVqZA2Q; charset=UTF-8";
	protected 	final 	Charset								usedCharset = StandardCharsets.UTF_8;
	protected 			Charset								incomingCharset;
	protected 	final	Boolean								sendOnlyOneLanguageForEachAlert = false;
	protected	final	Boolean								meteoAlarmHazardV2 = true;
	protected	final	Boolean								meteoAlarmUpdate = true;
	protected 			Boolean 							checkForDuplicate = true;
	protected 			Boolean 							calculateCentroid = true;
	protected 			Boolean								meteoAlarmExitVal = false;
	protected 			SqlSession 							session;
	protected	 		UkfloodsMapper 						ukfloodsMapper;
	protected 			XmlCapMapper 						xmlcapMapper;	
	protected 			MeteoalarmxmlidMapper 				meteoalarmxmlidMapper; 
	protected	 		List<String> 						listMeteoAlarmUniqueId;
	protected	 		List<String> 						listMeteoAlarmCAPIdentifier;
	protected	 		List<String> 						listMeteoAlarmAlertCapIdFromMeteoAlarmAlert;
	protected 			Map<String, Integer> 				mapCapIdentifierId;
	protected 			MeteoalarmurlMapper 				meteoalarmurlMapper;
	protected 			List<String> 						validCountryId = new ArrayList<>();
	protected 			List<String> 						countryWithPolygon = new ArrayList<>();
	protected 			List<String> 						countryWithoutPolygon = new ArrayList<>();
	protected 			List<String> 						countryWithNullPolygon = new ArrayList<>();
	protected 			MeteoalarmpolygonMapper 			meteoalarmpolygonMapper;
	protected 			Map<String, Meteoalarmpolygon> 		EmmaIdPolygon;
	protected 			Map<String, Meteoalarmpolygon> 		NutPolygon;
	protected 			List<String> 						IdOfFalseXmlAlarmPolygon;
	protected 			Map<String, String> 				mapCodeNations; 
	protected 			MeteoalarmalertMapper				meteoalarmalertMapper;
	protected 			MeteoalarmactivealertMapper 		meteoalarmactivealertMapper;
	protected 			SeveritymappingMapper 				severitymappingMapper;
	protected 			Map<String, Integer> 				mapSeverity;
	protected 			Map<String, Meteoalarmalert> 		mapPolygonSentMeteoAlarmAlert;
	protected 			Map<String, Meteoalarmactivealert> 	mapPolygonMeteoAlarmActiveAlert;
	protected 			Integer 							idForUpdate = 0;
	protected 			Map<Integer, Integer> 				mapMeteoAlarmCapIdIdApi;
	protected			IsolanguageMapper					isolanguageMapper;
	protected			Map<String, Isolanguage>			mapIsoLanguage;	
	protected			HazardMapper						hazardMapper;
	protected			Map<String, Hazard>					MeteoAlarmHazardMap;
	protected			Map<Integer, String>				MeteoAlarmAlertIdApiStartdate;
	protected			Map<Integer, String>				MeteoAlarmAlertIdApiLanguage;
	protected			UkalertMapper						ukalertMapper;
	
	
	
	public UkalertMapper getUkalertMapper() {
		return ukalertMapper;
	}
	public void setUkalertMapper(UkalertMapper ukalertMapper) {
		this.ukalertMapper = ukalertMapper;
	}
		
		
	public HazardMapper getHazardMapper() {
		return hazardMapper;
	}
	public void setHazardMapper(HazardMapper hazardMapper) {
		this.hazardMapper = hazardMapper;
	}
	
	public IsolanguageMapper getIsolanguageMapper() {
		return isolanguageMapper;
	}
	public void setIsolanguageMapper(IsolanguageMapper isolanguageMapper) {
		this.isolanguageMapper = isolanguageMapper;
	}
		
	public SeveritymappingMapper getseveritymappingMapper() {
		return severitymappingMapper;
	}
	public void setSeverityMapper(SeveritymappingMapper severitymappingMapper) {
		this.severitymappingMapper = severitymappingMapper;
	}
	
	public UkfloodsMapper getUkfloodsMapper() {
		return ukfloodsMapper;
	}
	public void setUkfloodsMapper(UkfloodsMapper ukfloodsMapper) {
		this.ukfloodsMapper = ukfloodsMapper;
	}	
	
	public MeteoalarmxmlidMapper getMeteoalarmxmlidMapper() {
		return meteoalarmxmlidMapper;
	}
	public void setMeteoalarmxmlidMapper(MeteoalarmxmlidMapper meteoalarmxmlidMapper) {
		this.meteoalarmxmlidMapper = meteoalarmxmlidMapper;
	}
	
	public MeteoalarmurlMapper getMeteoalarmurlMapper() {
		return meteoalarmurlMapper;
	}
	public void setMeteoalarmurlMapper(MeteoalarmurlMapper meteoalarmurlMapper) {
		this.meteoalarmurlMapper = meteoalarmurlMapper;
	}

	public XmlCapMapper getXmlcapMapper() {
		return xmlcapMapper;
	}
	public void setXmlcapMapper(XmlCapMapper xmlcapMapper) {
		this.xmlcapMapper = xmlcapMapper;
	}
	
	public MeteoalarmpolygonMapper getMeteoalarmpolygonMapper() {
		return meteoalarmpolygonMapper;
	}
	public void setMeteoalarmpolygonMapper(MeteoalarmpolygonMapper meteoalarmpolygonMapper) {
		this.meteoalarmpolygonMapper = meteoalarmpolygonMapper;
	}

	public MeteoalarmalertMapper getMeteoalarmalertMapper() {
		return meteoalarmalertMapper;
	}
	public void setmeteoalarmalertMapper(MeteoalarmalertMapper meteoalarmalertMapper) {
		this.meteoalarmalertMapper = meteoalarmalertMapper;
	}
		
	public MeteoalarmactivealertMapper getMeteoalarmactivealertMapper() {
		return meteoalarmactivealertMapper;
	}
	public void setMeteoalarmactivealert(MeteoalarmactivealertMapper meteoalarmactivealertMapper) {
		this.meteoalarmactivealertMapper = meteoalarmactivealertMapper;
	}
	
	public SqlSession getSession() {
		return session;
	}
	public void setSession(SqlSession session) {
		this.session = session;
	}

	
	public ApiManager() {
		try {
			InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
			SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);
			session 					= factory.openSession(false);
			ukfloodsMapper 				= session.getMapper(UkfloodsMapper.class);
			xmlcapMapper 				= session.getMapper(XmlCapMapper.class);
			meteoalarmxmlidMapper 		= session.getMapper(MeteoalarmxmlidMapper.class);
			meteoalarmurlMapper 		= session.getMapper(MeteoalarmurlMapper.class);
			meteoalarmpolygonMapper 	= session.getMapper(MeteoalarmpolygonMapper.class);
			meteoalarmalertMapper 		= session.getMapper(MeteoalarmalertMapper.class);
			meteoalarmactivealertMapper = session.getMapper(MeteoalarmactivealertMapper.class);
			severitymappingMapper 		= session.getMapper(SeveritymappingMapper.class);
			isolanguageMapper			= session.getMapper(IsolanguageMapper.class);
			hazardMapper				= session.getMapper(HazardMapper.class);
			ukalertMapper				= session.getMapper(UkalertMapper.class);
			
			createSeverityMap(); 
						
			cleanMeteoAlarmAlert();
			
			cleanMeteoAlarmXmlId();			
			
			createMapPolygonSentMeteoAlarmAlert();
			
			updateMeteoAlarmActiveAlert();
			
			createMapPolygonActiveMeteoAlarmAlert();
			
			createMeteoAlarmXmlIdHashMap(null);
			
			populateValidCountry();
			
			populateEmmaIdPolygonMap();
				
			populateMapIsoLanguage();
			
			populateMeteoAlarmHazardMap();
		
			
		} catch (Exception e) {
		} finally {
		}
	}
	
	
	private void populateMeteoAlarmHazardMap() {
		MeteoAlarmHazardMap = new HashMap<>();
		List<Hazard> all = hazardMapper.selectByExample(null);
		for(Hazard h:all){
			if(h.getMeteoalarm()!=null){
				MeteoAlarmHazardMap.put(h.getMeteoalarm(), h);
			}
		}
	}
	
	private void populateMapIsoLanguage() {
		mapIsoLanguage = new HashMap<>();
		List<Isolanguage> all = isolanguageMapper.selectByExample(null);
		for(Isolanguage i:all){
			if(i.getId()!=0 && i.getIsolanguagename()!=null && i.getIsosymbol()!=null){
				mapIsoLanguage.put(i.getIsosymbol(), i);
			}
		}
	}
	
	protected void createMapPolygonActiveMeteoAlarmAlert() {
		mapPolygonMeteoAlarmActiveAlert = new HashMap<>();
		List<Meteoalarmactivealert> allMeteoAlarmActiveAlert = meteoalarmactivealertMapper.selectByExample(null);
		for(Meteoalarmactivealert m:allMeteoAlarmActiveAlert){
			if(m!=null && m.getAreaofinterest()!=null){
				mapPolygonMeteoAlarmActiveAlert.put(m.getCapid()+"-.-.-"+m.getAreaofinterest(), m);
			}			
		}
	}

	private void updateMeteoAlarmActiveAlert() throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		List<Meteoalarmactivealert> listAllMeteoAlarmActiveAlert = meteoalarmactivealertMapper.selectByExample(null);
		for(Meteoalarmactivealert m:listAllMeteoAlarmActiveAlert){
			if(m!=null && m.getEnddate()!=null){
				Date date = format.parse(m.getEnddate());
				if(date.before(new Date())){
					try {
						meteoalarmactivealertMapper.deleteByPrimaryKey(m.getId());
						session.commit();					
					} catch (Exception e) {
						e.printStackTrace();
					}					
				}
			}
		}
	}

	private void createMapPolygonSentMeteoAlarmAlert() {
		mapPolygonSentMeteoAlarmAlert = new HashMap<>();
		mapMeteoAlarmCapIdIdApi = new HashMap<>();
		listMeteoAlarmAlertCapIdFromMeteoAlarmAlert = new ArrayList<>();
		MeteoAlarmAlertIdApiStartdate = new HashMap<>();
		MeteoAlarmAlertIdApiLanguage = new HashMap<>();
		List<Meteoalarmalert> allMeteoAlarmAlert = meteoalarmalertMapper.selectByExample(null);
		for(Meteoalarmalert m:allMeteoAlarmAlert){
			if(m!=null && m.getAreaofinterest()!=null){
				mapPolygonSentMeteoAlarmAlert.put(m.getCapid()+"-.-.-"+m.getAreaofinterest(), m);
				if(m.getCapid()!=null){
					listMeteoAlarmAlertCapIdFromMeteoAlarmAlert.add(m.getCapid());
					if(m.getIdapi()!=null){
						mapMeteoAlarmCapIdIdApi.put(m.getId(), m.getIdapi());
						MeteoAlarmAlertIdApiStartdate.put(m.getIdapi(), m.getStartdate());
						if(m.getLanguage()!=null) {
							MeteoAlarmAlertIdApiLanguage.put(m.getIdapi(), m.getLanguage());
						}
					}
				}
			}			
		}
	}

	private void createSeverityMap() {
		mapSeverity = new HashMap<>();
		List<Severitymapping> allSeverity = severitymappingMapper.selectByExample(null);
		for(Severitymapping s : allSeverity){
			if(s.getValue()!=null && s.getId()!=null){
				mapSeverity.put(s.getValue(), s.getId());
			}
		}
	}


	private void populateEmmaIdPolygonMap() {
		List<Meteoalarmpolygon> allMeteoAlarmPolygon = meteoalarmpolygonMapper.selectByExample(null);
		EmmaIdPolygon = new HashMap<>();
		NutPolygon = new HashMap<>();
		IdOfFalseXmlAlarmPolygon = new ArrayList<>();
		for(Meteoalarmpolygon m:allMeteoAlarmPolygon){
			EmmaIdPolygon.put(m.getEmmaid(), m);
			NutPolygon.put(m.getNut(), m);
		}
	}

	private void populateValidCountry() {
		mapCodeNations = new HashMap<>();
		List<Meteoalarmurl> allRecords = meteoalarmurlMapper.selectByExample(null);
		for(Meteoalarmurl m: allRecords){
			if(m.getIsvalid()){
				validCountryId.add(m.getCountryid());
			}
			if(m.getPolygon()!=null && m.getPolygon()==true){
				countryWithPolygon.add(m.getCountryid());
			}else if(m.getPolygon()!=null && m.getPolygon()==false){
				countryWithoutPolygon.add(m.getCountryid());
			}else{
				countryWithNullPolygon.add(m.getCountryid());
			}
			if(m.getCountryid() !=null &&  m.getCountrydescen()!=null){
				mapCodeNations.put(m.getCountryid(), m.getCountrydescen());
			}
		}
	}

	private void cleanMeteoAlarmXmlId() throws ParseException, SQLException {
			
		List<Meteoalarmxmlid> meteoList = meteoalarmxmlidMapper.selectByExample(null);
		if (meteoList == null || meteoList.size() == 0) {
					
		} else {
			for (Meteoalarmxmlid m : meteoList) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
				Date date = format.parse(m.getReadingdate());
				if(TimeUnit.DAYS.convert(new Date().getTime() - date.getTime(),TimeUnit.MILLISECONDS )>7){
					meteoalarmxmlidMapper.deleteByPrimaryKey(m.getId());
				}
			}
		}
	
		getSession().commit();
	}

	
	private void cleanMeteoAlarmAlert() throws ParseException, SQLException {
			
		List<Meteoalarmalert> meteoList = meteoalarmalertMapper.selectByExample(null);
		if (meteoList == null || meteoList.size() == 0) {
					
		} else {
			for (Meteoalarmalert m : meteoList) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
				Date date = format.parse(m.getEnddate());
				if(TimeUnit.DAYS.convert(new Date().getTime() - date.getTime(),TimeUnit.MILLISECONDS )>7){
					meteoalarmxmlidMapper.deleteByPrimaryKey(m.getId());	
				}
			}
		}
		getSession().commit();
	}

	
	
	protected void createMeteoAlarmXmlIdHashMap(List<Meteoalarmxmlid> meteoList) {
		listMeteoAlarmUniqueId = new ArrayList<>();
		listMeteoAlarmCAPIdentifier = new ArrayList<>();
		mapCapIdentifierId = new HashMap<>();
		if(meteoList==null){
			meteoList = meteoalarmxmlidMapper.selectByExample(null);
		}
		if (meteoList == null || meteoList.size() == 0) {
		} else {
			for (Meteoalarmxmlid m : meteoList) {
				listMeteoAlarmUniqueId.add(m.getXmluniqueid());
				if(m.getCapidentifier()!=null && m.getCapidentifier().length()>0) {
					listMeteoAlarmCAPIdentifier.add(m.getCapidentifier());
					mapCapIdentifierId.put(m.getCapidentifier(), m.getId());
				}
			}
		}
	}

	public void closeDBSession(){
		try{
			session.close();
		} catch(Exception e){
		}
	}
	
	private CreateAlertAndWarning alertAndWarning;
	
	public CreateAlertAndWarning getAlertAndWarning() {
		return alertAndWarning;
	}

	public void setAlertAndWarning(CreateAlertAndWarning alertAndWarning) {
		this.alertAndWarning = alertAndWarning;
	}

	public abstract void process();

	public Token getApiToken() throws Exception {
		HttpPost httpPost = new HttpPost();
		
		httpPost = new HttpPost(LemsWebConstant.URL_API_TOKEN);
		
		
		HttpClient client = HttpClientBuilder.create().build();
		
		ApiAuthentication auth=new ApiAuthentication();
		auth.setUser(LemsWebConstant.USER_API_TOKEN);
		auth.setPwd(LemsWebConstant.PWD_API_TOKEN);
		
		ObjectMapper mapper = new ObjectMapper();
				
		StringEntity entity = new StringEntity(mapper.writeValueAsString(auth));
		httpPost.setEntity(entity);
		httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		client = HttpClientBuilder.create().build();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		entity.writeTo(baos);
		
		List<Header> httpHeaders = Arrays.asList(httpPost.getAllHeaders());        
	    
		HttpResponse response = client.execute(httpPost);
		String body = EntityUtils.toString(response.getEntity(), usedCharset);
		Token token = mapper.readValue(body, Token.class);
		return token;
	}

	public String sendCreateAlert(Token token, int idflood, String json, Item flood, FloodsAreaDto floodsAreaDto) throws Exception {
		String mailSubject = CLASSNAME+"::sendCreateAlert with idflood:"+idflood;
		HttpPost httpPost = new HttpPost();
		try{
			httpPost = new HttpPost(LemsWebConstant.URL_API+"EmergencyCommunication/v2/CreateAlertAndWarning");
			String auth = "Bearer " + token.getResult().getAccessToken();
			
			httpPost.setHeader(HttpHeaders.CONNECTION, "keep-alive");
			httpPost.setHeader(HttpHeaders.CONTENT_TYPE, content_type_v2);
			httpPost.setHeader(HttpHeaders.ACCEPT, "*/*");
			httpPost.setHeader(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br");
			httpPost.setHeader(HttpHeaders.AUTHORIZATION, auth);

			HttpClient client = HttpClientBuilder.create().build();
			ObjectMapper mapper = new ObjectMapper();

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setBoundary("----WebKitFormBoundarypUkY3YHXFOVqZA2Q");
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);			
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "resource",alertAndWarning.getResource());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "type",alertAndWarning.getType());	
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "start",alertAndWarning.getStart());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "end",alertAndWarning.getEnd());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "areaOfInterest",alertAndWarning.getAreaOfInterest());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "location",alertAndWarning.getLocation());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "hazard",alertAndWarning.getHazard());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "warningLevel",alertAndWarning.getWarningLevel());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "receivers[0]", String.valueOf(alertAndWarning.getReceivers()[0]));
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "title",floodsAreaDto.getItems().getLabel());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "description",alertAndWarning.getDescription());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "sourceOfInformation",alertAndWarning.getSourceOfInformation());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "communicationStatus",alertAndWarning.getCommunicationStatus());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capIdentifier",alertAndWarning.getCAPIdentifier());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capStatus",alertAndWarning.getCAPStatus());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capResponseType",alertAndWarning.getCAPResponseType());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capUrgency",alertAndWarning.getCAPUrgency());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capCertainty",alertAndWarning.getCAPCertainty());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capCategory",alertAndWarning.getCAPCategory());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "instruction",alertAndWarning.getInstruction());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "web",alertAndWarning.getWeb());
			if(alertAndWarning.getExtensionData()!=null) {
				builder.addBinaryBody("extensionData", new ByteArrayInputStream(alertAndWarning.getExtensionData().toString().getBytes(usedCharset)));
			}				
			HttpEntity entity = builder.build();				
			httpPost.setEntity(entity);			
			client = HttpClientBuilder.create().build();			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();			
			entity.writeTo(baos);
			List<Header> httpHeaders = Arrays.asList(httpPost.getAllHeaders());        
		    insertToukAlert(alertAndWarning, "create");
			HttpResponse response = client.execute(httpPost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				Result result=mapper.readValue(EntityUtils.toString(response.getEntity(), usedCharset), Result.class);				
				insertToDbUkFlood(result,idflood,flood);
				return "OK";
			} else {				
				String messageError=EntityUtils.toString(response.getEntity(), usedCharset);
				String mailText = "Http statusCode = " + statusCode + ", Message = " + messageError;				
				if(mailText.toLowerCase().contains("already")){
					Result r = new Result();
					r.setId(Integer.parseInt(mailText.split("Http statusCode = 400, Message = {\"success\":false,\"error\":{\"code\":0,\"message\":\"An emergency communication with the same information already exists. Id: ")[1].split(". Try to update")[0]));
					insertToDbUkFlood(r,idflood,flood);
				}				
				return mailText;
			}
		}catch (Exception e) {
			getSession().rollback();
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
			return mailText;
		} 
	}
	
	private void insertToukAlert(CreateAlertAndWarning alertAndWarning, String createOrUpdate) {
		Ukalert ukalert = new Ukalert();
		if(alertAndWarning.getResource()!=null){ukalert.setResource(alertAndWarning.getResource());}
		else{ukalert.setResource("");}
		if(alertAndWarning.getType()!=null){ukalert.setType(alertAndWarning.getType());}
		else{ukalert.setType("");}
		if(alertAndWarning.getStart()!=null){ukalert.setStartvalue(alertAndWarning.getStart());}
		else{ukalert.setStartvalue("");}
		if(alertAndWarning.getEnd()!=null){ukalert.setEndvalue(alertAndWarning.getEnd());}
		else{ukalert.setEndvalue("");}
		if(alertAndWarning.getAreaOfInterest()!=null){ukalert.setAreaofinterest(alertAndWarning.getAreaOfInterest());}
		else{ukalert.setAreaofinterest("");}
		if(alertAndWarning.getLocation()!=null){ukalert.setLocation(alertAndWarning.getLocation());}
		else{ukalert.setLocation("");}
		if(alertAndWarning.getHazard()!=null){ukalert.setHazard(alertAndWarning.getHazard());}
		else{ukalert.setHazard("");}
		if(alertAndWarning.getWarningLevel()!=null){ukalert.setWarninglevel(alertAndWarning.getWarningLevel());}
		else{ukalert.setWarninglevel("");}
		if(alertAndWarning.getReceivers()!=null){			
				ukalert.setReceivers(Integer.toString(alertAndWarning.getReceivers()[0]));					
		}
		else{ukalert.setReceivers("");}		
		if(alertAndWarning.getTitle()!=null){ukalert.setTitle(alertAndWarning.getTitle());}
		else{ukalert.setTitle("");}
		if(alertAndWarning.getDescription()!=null){ukalert.setDescription(alertAndWarning.getDescription());}
		else{ukalert.setDescription("");}
		if(alertAndWarning.getSourceOfInformation()!=null){ukalert.setSourceofinformation(alertAndWarning.getSourceOfInformation());}
		else{ukalert.setSourceofinformation("");}
		if(alertAndWarning.getCommunicationStatus()!=null){ukalert.setCommunicationstatus(alertAndWarning.getCommunicationStatus());}
		else{ukalert.setCommunicationstatus("");}
		if(alertAndWarning.getCAPIdentifier()!=null){ukalert.setCapidentifier(alertAndWarning.getCAPIdentifier());}
		else{ukalert.setCapidentifier("");}		
		if(alertAndWarning.getCAPStatus()!=null){ukalert.setCapstatus(alertAndWarning.getCAPStatus());}
		else{ukalert.setCapstatus("");}
		if(alertAndWarning.getCAPResponseType()!=null){ukalert.setCapresponsetype(alertAndWarning.getCAPResponseType());}
		else{ukalert.setCapresponsetype("");}
		if(alertAndWarning.getCAPUrgency()!=null){ukalert.setCapurgency(alertAndWarning.getCAPUrgency());}
		else{ukalert.setCapurgency("");}
		if(alertAndWarning.getCAPCertainty()!=null){ukalert.setCapcertainty(alertAndWarning.getCAPCertainty());}
		else{ukalert.setCapcertainty("");}		
		if(alertAndWarning.getCAPCategory()!=null){ukalert.setCapcategory(alertAndWarning.getCAPCategory());}
		else{ukalert.setCapcategory("");}
		if(alertAndWarning.getInstruction()!=null){ukalert.setInstruction(alertAndWarning.getInstruction());}
		else{ukalert.setInstruction("");}
		if(alertAndWarning.getWeb()!=null){ukalert.setWeb(alertAndWarning.getWeb());}
		else{ukalert.setWeb("");}
		ukalert.setCreateorupdate(createOrUpdate);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");		
		ukalert.setCreationdate(format.format(new Date()));
		getUkalertMapper().insertSelective(ukalert);
		getSession().commit();
	}
	@SuppressWarnings("deprecation")
	private void checkIfNotNullAndInsertInMultiPartForm(CreateAlertAndWarning alertAndWarning, MultipartEntityBuilder builder, String label, String value) throws UnsupportedEncodingException {
		Charset chars = Charset.forName("UTF-8");		
		if(value!=null) {
			try {
			    StringBody stringB = new StringBody(value,chars);  
			    builder.addPart(label, stringB); 
			} catch (UnsupportedEncodingException e) {
			    e.printStackTrace();
			}
		}else{
			StringBody stringB = new StringBody("",chars); 
		    builder.addPart(label, stringB); 
		}		
	}
	
	private void checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(Meteoalarmactivealert mapPolygonMeteoAlarmActiveAlert, MultipartEntityBuilder builder, String label, String value) throws UnsupportedEncodingException {
		if(value!=null) {
			byte ptext[] = value.getBytes(usedCharset);  
			builder.addTextBody(label, new String(ptext, usedCharset));
		}
	}
	
	public static void main(String arg[]){
		try{
			HttpPost httpPost = new HttpPost();
			
			httpPost = new HttpPost(LemsWebConstant.URL_API_TOKEN);
			
			
			HttpClient client = HttpClientBuilder.create().build();
			
			ApiAuthentication auth=new ApiAuthentication();
			auth.setUser(LemsWebConstant.USER_API_TOKEN);
			auth.setPwd(LemsWebConstant.PWD_API_TOKEN);
			
			ObjectMapper mapper = new ObjectMapper();
					
			StringEntity entity = new StringEntity(mapper.writeValueAsString(auth));
			httpPost.setEntity(entity);
			httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			client = HttpClientBuilder.create().build();
			
			HttpResponse response = client.execute(httpPost);
			String body = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_16);
			Token token = mapper.readValue(body, Token.class);

			
			HttpGet httpGet=new HttpGet();
			httpGet = new HttpGet(LemsWebConstant.URL_API+"EmergencyCommunication/GetList?type=alert&type=warning&datestart=2018-05-20T00:00:00&organizationUnitId=5");
			
			String authstr = "Bearer " + token.getResult().getAccessToken();
			
			httpGet.setHeader(HttpHeaders.AUTHORIZATION, authstr);
			
			HttpClient client2 = HttpClientBuilder.create().build();
			
			response = client2.execute(httpGet);
			int statusCode = response.getStatusLine().getStatusCode();
			
			ResultGetEmergency resultGetEmergency=mapper.readValue(EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_16), ResultGetEmergency.class);
			
			List<it.csi.lems.lemsbatch.dto.ws.ResultGetEmergency.Item> itemnList=ApiAlertsHelper.getOnlyOpenItems(resultGetEmergency);
			
			
			for (it.csi.lems.lemsbatch.dto.ws.ResultGetEmergency.Item item: itemnList){
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public String updateAlert(Token token, ZonedDateTime now, Integer id, Integer idFlood, boolean isNoLongerInForce, Ukfloods ukf, FloodsAreaDto floodsAreaDto) throws Exception {
		
		String mailSubject = CLASSNAME+"::updateAlert with id:"+id+" idFlood:"+idFlood;
		
		HttpPut httpPut = new HttpPut(); 
		httpPut = new HttpPut(LemsWebConstant.URL_API+EMERGENCY_COMMUNICATION_TAIL);
		String auth = "Bearer " + token.getResult().getAccessToken();
		
		httpPut.setHeader(HttpHeaders.CONNECTION, "keep-alive");
		httpPut.setHeader(HttpHeaders.CONTENT_TYPE, content_type_v2);
		httpPut.setHeader(HttpHeaders.ACCEPT, "*/*");
		httpPut.setHeader(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br");
		httpPut.setHeader(HttpHeaders.AUTHORIZATION, auth);
		
		HttpClient client = HttpClientBuilder.create().build();
		ObjectMapper mapper = new ObjectMapper();
			
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setBoundary("----WebKitFormBoundarypUkY3YHXFOVqZA2Q");
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		builder.addTextBody("id", id.toString());
		if(isNoLongerInForce) {
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "end",DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").format(now));
		}else {
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "resource",alertAndWarning.getResource());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "type",alertAndWarning.getType());	
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "start",alertAndWarning.getStart());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "location",alertAndWarning.getLocation());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "hazard",alertAndWarning.getHazard());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "warningLevel",alertAndWarning.getWarningLevel());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "receivers[0]", String.valueOf(alertAndWarning.getReceivers()[0]));
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "title",floodsAreaDto.getItems().getLabel());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "description",alertAndWarning.getDescription());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "sourceOfInformation",alertAndWarning.getSourceOfInformation());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "communicationStatus",alertAndWarning.getCommunicationStatus());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capStatus",alertAndWarning.getCAPStatus());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capResponseType",alertAndWarning.getCAPResponseType());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capUrgency",alertAndWarning.getCAPUrgency());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capCategory",alertAndWarning.getCAPCategory());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "instruction",alertAndWarning.getInstruction());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "web",alertAndWarning.getWeb());
			if(alertAndWarning.getExtensionData()!=null) {
				builder.addBinaryBody("extensionData", new ByteArrayInputStream(alertAndWarning.getExtensionData().toString().getBytes(usedCharset)));
			}
		}	
		
		
		
		HttpEntity entity = builder.build();
		httpPut.setEntity(entity);
		
		
		
		
		
		client = HttpClientBuilder.create().build();
			
		if(isNoLongerInForce) {
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			entity.writeTo(bytes);
		}	
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		entity.writeTo(baos);
		List<Header> httpHeaders = Arrays.asList(httpPut.getAllHeaders());        
	   
		HttpResponse response = client.execute(httpPut);
		int statusCode = response.getStatusLine().getStatusCode();
		
		insertToukAlert(alertAndWarning, "update");
		
		if (statusCode==200){
			if(id!=null && isNoLongerInForce) {
				deleteClosedFloods(id);
			}else if(id!=null && !isNoLongerInForce){
				updateFlood(ukf);
			}
			return "OK";
		} else {			
			String messageError=EntityUtils.toString(response.getEntity(), usedCharset);
			String mailText = "Http statusCode = " + statusCode + ", Message = " + messageError;
			MailHelper.postMail(mailSubject, mailText);					
			return mailText;
		}
		
	}
	
	private void updateFlood(Ukfloods ukf) {
		String mailSubject = CLASSNAME+"::writeDBAndsendToIDI with ukf.getId():"+ukf.getId();
		try{
			UkfloodsExample ukfloodsExample = new UkfloodsExample();
			UkfloodsExample.Criteria ukFloodsCriteria = ukfloodsExample.createCriteria();
			ukFloodsCriteria.andIdEqualTo(ukf.getId());
			getUkfloodsMapper().updateByPrimaryKey(ukf);
			getSession().commit();
		} catch (Exception e) {
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} 		
	}

	private void deleteClosedFloods(int id) {
		String mailSubject = CLASSNAME+"::writeDBAndsendToIDI with id:"+id;
		try{
			UkfloodsExample ukfloodsExample = new UkfloodsExample();
			UkfloodsExample.Criteria ukFloodsCriteria = ukfloodsExample.createCriteria();
			ukFloodsCriteria.andIdEqualTo(id);
			getUkfloodsMapper().deleteByPrimaryKey(id);
			getSession().commit();
		} catch (Exception e) {
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} 
	}

	public void insertToDbUkFlood(Result result, int idflood, Item flood) {}
	
	public String sendCreateAlertMeteoAlarm(Token token) throws Exception {
		String mailSubject = CLASSNAME+"::sendCreateAlertMeteoAlarm";
		HttpPost httpPost = new HttpPost();
		
		try{
			httpPost = new HttpPost(LemsWebConstant.URL_API+"EmergencyCommunication/v2/CreateAlertAndWarning");
			httpPost.reset();
				
			String auth = "Bearer " + token.getResult().getAccessToken();
			
			httpPost.setHeader(HttpHeaders.CONNECTION, "keep-alive");
			httpPost.setHeader(HttpHeaders.CONTENT_TYPE, content_type_v2);
			httpPost.setHeader(HttpHeaders.ACCEPT, "*/*");
			httpPost.setHeader(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br");
			httpPost.setHeader(HttpHeaders.AUTHORIZATION, auth);
			httpPost.setHeader(HttpHeaders.VARY, "Content-Type");			
			HttpClient client = HttpClientBuilder.create().build();
			ObjectMapper mapper = new ObjectMapper();
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setCharset(usedCharset);			
			builder.setBoundary("----WebKitFormBoundarypUkY3YHXFOVqZA2Q");
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);			
			
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "resource",alertAndWarning.getResource());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "type",alertAndWarning.getType());	
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "start",alertAndWarning.getStart());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "end",alertAndWarning.getEnd());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "areaOfInterest",alertAndWarning.getAreaOfInterest());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "location",alertAndWarning.getLocation());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "hazard",alertAndWarning.getHazard());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "warningLevel",alertAndWarning.getWarningLevel());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "receivers[0]", String.valueOf(alertAndWarning.getReceivers()[0]));
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "description",alertAndWarning.getDescription().replace("Event: ", ""));
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "sourceOfInformation",alertAndWarning.getSourceOfInformation());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "communicationStatus",alertAndWarning.getCommunicationStatus());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capIdentifier",alertAndWarning.getCAPIdentifier());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capStatus",alertAndWarning.getCAPStatus());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capResponseType",alertAndWarning.getCAPResponseType());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capUrgency",alertAndWarning.getCAPUrgency());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capCertainty",alertAndWarning.getCAPCertainty());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "language", alertAndWarning.getLanguage());
			if(alertAndWarning.getHazard().toLowerCase().equals("avalanches")){
				checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capCategory","Geo");
			}else if(alertAndWarning.getHazard().toLowerCase().equals("forestfire")){
				checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capCategory","Fire");
			}else{
				checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "capCategory","Met");
			}
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "instruction",alertAndWarning.getInstruction());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "web",alertAndWarning.getWeb());
			checkIfNotNullAndInsertInMultiPartForm(alertAndWarning, builder, "title",new String(alertAndWarning.getTitle().getBytes(usedCharset), usedCharset));
			if(alertAndWarning.getTitle()==null){
				String hazard = alertAndWarning.getDescription().split(" -")[0].replace("Event: ", "");
				String color = "";
				if(alertAndWarning.getWarningLevel().toLowerCase().equals("extreme")) color = "Red";
				else if(alertAndWarning.getWarningLevel().toLowerCase().equals("severe")) color = "Orange";
				else if(alertAndWarning.getWarningLevel().toLowerCase().equals("moderate")) color = "Yellow";
				builder.addTextBody("title", hazard+" "+color+" "+"alert");
			}
			
			if(alertAndWarning.getExtensionData()!=null) {
				builder.addBinaryBody("extensionData", new ByteArrayInputStream(alertAndWarning.getExtensionData().toString().getBytes(usedCharset)));
			}					
			builder.setCharset(usedCharset);			
			HttpEntity entity = builder.build();			
			httpPost.setEntity(entity);
			client = HttpClientBuilder.create().build();
			String line;
			if(httpPost.getEntity().getContentLength()< 8*1024){
				BufferedReader br = new BufferedReader(new InputStreamReader(httpPost.getEntity().getContent()));
				
			}
			
			HttpResponse response = client.execute(httpPost);
			
			String messageError=EntityUtils.toString(response.getEntity());
			int statusCode = response.getStatusLine().getStatusCode();
			Result result=mapper.readValue(messageError, Result.class);
			if (statusCode == 200) {	
				insertToDbMeteoalarmAlert(alertAndWarning, "Create", result);	
				return "OK";
			} else {
				String mailText = "Http statusCode = " + statusCode + ", Message = " + messageError;
				if(mailText.toLowerCase().contains("an emergency communication with the same information already exists")){
					
					int id = 0;	
					if(id==0){
						String idString = mailText.split("n emergency communication with the same information already exists. Id: ")[1].split("\\.")[0];
						id = Integer.parseInt(idString);
					}
					if(id!=0){
						if(MeteoAlarmAlertIdApiLanguage.get(id)!=null && 
							MeteoAlarmAlertIdApiLanguage.get(id).equals(alertAndWarning.getLanguage())){
							updateMeteoAlarm(token, ZonedDateTime.now(), id, false, alertAndWarning.getEnd());
							createMapPolygonActiveMeteoAlarmAlert();
						}						
					}					
				}					
				return mailText;
			}	
		} catch (Exception e) {
			getSession().rollback();
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
			return mailText;
		} 		
	}

	private String updateMeteoAlarm(Token apiToken, ZonedDateTime now, int id, boolean b, String endDate) throws IOException {
		String mailSubject = CLASSNAME+"::updateMeteoAlarmAlert with id:"+id;

		HttpPut httpPut = new HttpPut(); 
		httpPut = new HttpPut(LemsWebConstant.URL_API+EMERGENCY_COMMUNICATION_TAIL);
		String auth = "Bearer " + apiToken.getResult().getAccessToken();
		
		httpPut.setHeader(HttpHeaders.CONNECTION, "keep-alive");
		httpPut.setHeader(HttpHeaders.CONTENT_TYPE, content_type_v2);
		httpPut.setHeader(HttpHeaders.ACCEPT, "*/*");
		httpPut.setHeader(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br");
		httpPut.setHeader(HttpHeaders.AUTHORIZATION, auth);
		
		HttpClient client = HttpClientBuilder.create().build();
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setBoundary("----WebKitFormBoundarypUkY3YHXFOVqZA2Q");
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		if(Integer.toString(id)!=null){
			builder.addTextBody("id", Integer.toString(id));
		}else{
			return "error";
		}
		if(endDate!=null){
			builder.addTextBody("end", endDate);
		}else{
			builder.addTextBody("end", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(now));
		}
		
		HttpEntity entity = builder.build();
		httpPut.setEntity(entity);
		
		client = HttpClientBuilder.create().build();
			
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		entity.writeTo(baos);		
		HttpResponse response = client.execute(httpPut);
		int statusCode = response.getStatusLine().getStatusCode();
		
		if (statusCode==200){
			ObjectMapper mapper3 = new ObjectMapper();
			Result result=mapper3.readValue(EntityUtils.toString(response.getEntity(), usedCharset), Result.class);
			insertToDbMeteoalarmAlert(alertAndWarning, "Update/Close", result);	
			
			return "OK";
		} else {
			String messageError=EntityUtils.toString(response.getEntity(), usedCharset);			
			String mailText = "Http statusCode = " + statusCode + ", Message = " + messageError;
			MailHelper.postMail(mailSubject, mailText);
					
			return mailText;
	}

		
	}
	private void insertToDbMeteoalarmAlert(CreateAlertAndWarning alertAndWarning, String createOrUpdate, Result result) {
		String mailSubject = CLASSNAME+"::insert sent meteoAlarm alert to db: "+alertAndWarning.getCAPIdentifier();
		try{
			StringBuilder sb = new StringBuilder();
			int counter = 0;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			
			Meteoalarmalert maa = new Meteoalarmalert();
			if(idForUpdate!=null)								{maa.setId(idForUpdate);} 										else{maa.setId(0);}
			if(result.getId()!=0)								{maa.setIdapi(result.getId());} 		
			if(alertAndWarning.getType()!=null) 				{maa.setType(alertAndWarning.getType());} 									else{maa.setType("");}
			if(alertAndWarning.getStart()!=null)				{maa.setStartdate(alertAndWarning.getStart());} 							else{maa.setStartdate((""));}
			if(alertAndWarning.getEnd()!=null)					{maa.setEnddate(alertAndWarning.getEnd());} 								else{maa.setEnddate((""));}
			if(alertAndWarning.getLevel()!=null)				{maa.setLevelofthealarm(alertAndWarning.getLevel());} 						else{maa.setLevelofthealarm((""));}
			if(alertAndWarning.getHazard()!=null)				{maa.setHazard(alertAndWarning.getHazard());} 								else{maa.setHazard((""));}
			if(alertAndWarning.getDescription()!=null)			{maa.setDescription(alertAndWarning.getDescription());} 					else{maa.setDescription((""));}
			if(alertAndWarning.getAreaOfInterest()!=null)		{maa.setAreaofinterest(alertAndWarning.getAreaOfInterest());}				else{maa.setAreaofinterest((""));}
			if(alertAndWarning.getLocation()!=null)				{maa.setLocation(alertAndWarning.getLocation());} 							else{maa.setLocation((""));}
			if(alertAndWarning.getReceivers()!=null)			{for(int r:alertAndWarning.getReceivers()){
																		counter++;
																		sb.append(r);
																		if(counter!=alertAndWarning.getReceivers().length) sb.append(" - ");
																	}
																 maa.setReceivers(sb.toString());
																} 																			else{maa.setReceivers((""));}
			if(alertAndWarning.getSourceOfInformation()!=null)	{maa.setSourceofinfo(alertAndWarning.getSourceOfInformation());} 			else{maa.setSourceofinfo((""));}
			if(alertAndWarning.getCommunicationStatus()!=null)	{maa.setCommunicationstatus(alertAndWarning.getCommunicationStatus());} 	else{maa.setCommunicationstatus("");}
			if(alertAndWarning.getExtensionData()!=null)		{maa.setExtensiondata(alertAndWarning.getExtensionData().toString());} 		else{maa.setExtensiondata("");}
			if(alertAndWarning.getInstruction()!=null)			{maa.setInstruction(alertAndWarning.getInstruction());} 					else{maa.setInstruction("");}
			if(alertAndWarning.getResource()!=null)				{maa.setResource(alertAndWarning.getResource());} 							else{maa.setResource("");}
			if(alertAndWarning.getWeb()!=null)					{maa.setWeb(alertAndWarning.getWeb());} 									else{maa.setWeb("");}
			if(alertAndWarning.getTitle()!=null)				{maa.setTitle(alertAndWarning.getTitle());} 									else{maa.setTitle("");}
			if(alertAndWarning.getWarningLevel()!=null)			{maa.setWarninglevel(alertAndWarning.getWarningLevel());} 					else{maa.setWarninglevel("");}
			if(alertAndWarning.getCAPResponseType()!=null)		{maa.setCapresponsetype(alertAndWarning.getCAPResponseType());} 			else{maa.setCapresponsetype("");}
			if(alertAndWarning.getCAPCertainty()!=null)			{maa.setCapcertainty(alertAndWarning.getCAPCertainty());} 					else{maa.setCapcertainty("");}
			if(alertAndWarning.getCAPUrgency()!=null)			{maa.setCapurgency(alertAndWarning.getCAPUrgency());} 						else{maa.setCapurgency("");}
			if(alertAndWarning.getCAPIdentifier()!=null)		{maa.setCapid(alertAndWarning.getCAPIdentifier());} 						else{maa.setCapid("");}
			if(alertAndWarning.getCAPStatus()!=null)			{maa.setCapstatus(alertAndWarning.getCAPStatus());} 						else{maa.setCapstatus("");}
			if(alertAndWarning.getLanguage()!=null)				{maa.setLanguage(alertAndWarning.getLanguage());}							else{maa.setLanguage("");}
			maa.setDateofcreation(format.format(new Date()));
			maa.setCreateorupdate(createOrUpdate);
			getMeteoalarmalertMapper().insertSelective(maa);
			getSession().commit();
			createMapPolygonSentMeteoAlarmAlert();
			mailSubject = CLASSNAME+"::insert sent meteoAlarm alert to meteoAlarmActiveAlert: "+alertAndWarning.getCAPIdentifier();
			try{
				sb = new StringBuilder();
				counter = 0;
				Meteoalarmactivealert maaa = new Meteoalarmactivealert();
				if(idForUpdate!=null)								{maaa.setId(idForUpdate.intValue());} 										else{maaa.setId(0);}
				if(result.getId()!=0)								{maaa.setIdapi(result.getId());} 		
				if(maa.getType()!=null) 							{maaa.setType(maa.getType());} 									else{maaa.setType("");}
				if(maa.getStartdate()!=null)						{maaa.setStartdate(maa.getStartdate());} 							else{maaa.setStartdate((""));}
				if(maa.getEnddate()!=null)							{maaa.setEnddate(maa.getEnddate());} 								else{maaa.setEnddate((""));}
				if(maa.getLevelofthealarm()!=null)					{maaa.setLevelofthealarm(maa.getLevelofthealarm());} 						else{maaa.setLevelofthealarm((""));}
				if(maa.getHazard()!=null)							{maaa.setHazard(maa.getHazard());} 								else{maaa.setHazard((""));}
				if(maa.getDescription()!=null)						{maaa.setDescription(maa.getDescription());} 					else{maaa.setDescription((""));}
				if(maa.getAreaofinterest()!=null)					{maaa.setAreaofinterest(maa.getAreaofinterest());}				else{maaa.setAreaofinterest((""));}
				if(maa.getLocation()!=null)							{maaa.setLocation(maa.getLocation());} 							else{maaa.setLocation((""));}
				if(maa.getReceivers()!=null)						{for(int r:alertAndWarning.getReceivers()){
																			counter++;
																			sb.append(r);
																			if(counter!=alertAndWarning.getReceivers().length) sb.append(" - ");
																		}
																	 maaa.setReceivers(sb.toString());
																	} 																			else{maaa.setReceivers((""));}
				if(maa.getSourceofinfo()!=null)	{maaa.setSourceofinfo(maa.getSourceofinfo());} 			else{maaa.setSourceofinfo((""));}
				if(maa.getCommunicationstatus()!=null)	{maaa.setCommunicationstatus(maa.getCommunicationstatus());} 	else{maaa.setCommunicationstatus("");}
				if(maa.getExtensiondata()!=null)		{maaa.setExtensiondata(maa.getExtensiondata().toString());} 	else{maaa.setExtensiondata("");}
				if(maa.getInstruction()!=null)			{maaa.setInstruction(maa.getInstruction());} 					else{maaa.setInstruction("");}
				if(maa.getResource()!=null)				{maaa.setResource(maa.getResource());} 							else{maaa.setResource("");}
				if(maa.getWeb()!=null)					{maaa.setWeb(maa.getWeb());} 									else{maaa.setWeb("");}
				if(maa.getTitle()!=null)				{maaa.setTitle(maa.getTitle());} 								else{maaa.setTitle("");}
				if(maa.getWarninglevel()!=null)			{maaa.setWarninglevel(maa.getWarninglevel());} 					else{maaa.setWarninglevel("");}
				if(maa.getCapresponsetype()!=null)		{maaa.setCapresponsetype(maa.getCapresponsetype());} 			else{maaa.setCapresponsetype("");}
				if(maa.getCapcertainty()!=null)			{maaa.setCapcertainty(maa.getCapcertainty());} 					else{maaa.setCapcertainty("");}
				if(maa.getCapurgency()!=null)			{maaa.setCapurgency(maa.getCapurgency());} 						else{maaa.setCapurgency("");}
				if(maa.getCapid()!=null)				{maaa.setCapid(maa.getCapid());} 								else{maaa.setCapid("");}
				if(maa.getCapstatus()!=null)			{maaa.setCapstatus(maa.getCapstatus());} 
				if(maa.getLanguage()!=null)				{maaa.setLanguage(maa.getLanguage());} 						else{maaa.setLanguage("");}
				maaa.setDateofcreation(format.format(new Date()));
				getMeteoalarmactivealertMapper().insertSelective(maaa);
				getSession().commit();
				createMapPolygonActiveMeteoAlarmAlert();
				createMapPolygonSentMeteoAlarmAlert();
			} catch (Exception e) {
				getSession().rollback();
				String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
				MailHelper.postMail(mailSubject, mailText);
			} 
				
		}catch (Exception e) {
			getSession().rollback();
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} 
	}
	
	
	public String closeMeteoAlarmActiveAlert(Token token, ZonedDateTime now, Meteoalarmactivealert mapPolygonMeteoAlarmActiveAlert, Boolean forceDateStart) throws Exception {
		
		String mailSubject = CLASSNAME+"::updateMeteoAlarmAlert with id:"+mapPolygonMeteoAlarmActiveAlert.getId()+" CAPId:"+mapPolygonMeteoAlarmActiveAlert.getCapid()+
							"IdApi: "+mapMeteoAlarmCapIdIdApi.get(mapPolygonMeteoAlarmActiveAlert.getId());
		
		HttpPut httpPut = new HttpPut(); 
		httpPut = new HttpPut(LemsWebConstant.URL_API+EMERGENCY_COMMUNICATION_TAIL);
		String auth = "Bearer " + token.getResult().getAccessToken();
		
		httpPut.setHeader(HttpHeaders.CONNECTION, "keep-alive");
		httpPut.setHeader(HttpHeaders.CONTENT_TYPE, content_type_v2);
		httpPut.setHeader(HttpHeaders.ACCEPT, "*/*");
		httpPut.setHeader(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br");
		httpPut.setHeader(HttpHeaders.AUTHORIZATION, auth);
		
		HttpClient client = HttpClientBuilder.create().build();
		ObjectMapper mapper = new ObjectMapper();
			
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setBoundary("----WebKitFormBoundarypUkY3YHXFOVqZA2Q");
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		if(mapMeteoAlarmCapIdIdApi.get(mapPolygonMeteoAlarmActiveAlert.getId())!=null){
			builder.addTextBody("id", mapMeteoAlarmCapIdIdApi.get(mapPolygonMeteoAlarmActiveAlert.getId()).toString());
		}else{
			return "It's not possible to update this alert because no idApi is available";
		}
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "resource",mapPolygonMeteoAlarmActiveAlert.getResource());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "type",mapPolygonMeteoAlarmActiveAlert.getType());	
		if(forceDateStart){
			checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "start",DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").format(now.minusHours(1)));
		}else{
			checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "start",mapPolygonMeteoAlarmActiveAlert.getStartdate());
		}	
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "end",DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").format(now));
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "location",mapPolygonMeteoAlarmActiveAlert.getLocation());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "hazard",mapPolygonMeteoAlarmActiveAlert.getHazard());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "warningLevel",mapPolygonMeteoAlarmActiveAlert.getWarninglevel());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "receivers[0]", mapPolygonMeteoAlarmActiveAlert.getReceivers());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "title",mapPolygonMeteoAlarmActiveAlert.getTitle());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "description",mapPolygonMeteoAlarmActiveAlert.getDescription());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "communicationStatus",mapPolygonMeteoAlarmActiveAlert.getCommunicationstatus());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "capStatus",mapPolygonMeteoAlarmActiveAlert.getCapstatus());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "capResponseType",mapPolygonMeteoAlarmActiveAlert.getCapresponsetype());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "capUrgency",mapPolygonMeteoAlarmActiveAlert.getCapurgency());
		checkIfNotNullAndInsertInMultiPartFormUpdateMeteoAlarm(mapPolygonMeteoAlarmActiveAlert, builder, "instruction",mapPolygonMeteoAlarmActiveAlert.getInstruction());

		if(mapPolygonMeteoAlarmActiveAlert.getExtensiondata()!=null) {		
			builder.addBinaryBody("extensionData", new ByteArrayInputStream(mapPolygonMeteoAlarmActiveAlert.getExtensiondata().toString().getBytes(usedCharset)));
		}		
		HttpEntity entity = builder.build();
		httpPut.setEntity(entity);
		
		client = HttpClientBuilder.create().build();
			
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		entity.writeTo(baos);
		List<Header> httpHeaders = Arrays.asList(httpPut.getAllHeaders());        
	    
		HttpResponse response = client.execute(httpPut);
		int statusCode = response.getStatusLine().getStatusCode();
		
		if (statusCode==200){
			ObjectMapper mapper3 = new ObjectMapper();
			Result result=mapper3.readValue(EntityUtils.toString(response.getEntity(), usedCharset), Result.class);
			insertToDbMeteoalarmAlert(alertAndWarning, "Update/Close", result);	
			
			return "OK";
		} else {
			String messageError=EntityUtils.toString(response.getEntity(), usedCharset);			
			String mailText = "Http statusCode = " + statusCode + ", Message = " + messageError;
			MailHelper.postMail(mailSubject, mailText);
					
			return mailText;
		}
		
	}

}
