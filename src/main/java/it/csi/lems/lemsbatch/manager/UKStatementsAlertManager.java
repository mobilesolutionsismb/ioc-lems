package it.csi.lems.lemsbatch.manager;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.map.ObjectMapper;

import it.csi.lems.lemsbatch.dto.ws.uk.FloodsCsvDto;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson;
import it.csi.lems.lemsbatch.dto.ws.uk.Geojson.Feature;
import it.csi.lems.lemsbatch.dto.ws.uk.StatementDto;
import it.csi.lems.lemsbatch.dto.ws.uk.StatementDto.Statement;
import it.csi.lems.lemsbatch.helper.CsvHelper;
import it.csi.lems.lemsbatch.helper.MailHelper;
import it.csi.lems.lemsbatch.utils.LemsWebConstant;
import it.csi.lems.lemsbatch.utils.Util;

public class UKStatementsAlertManager extends AlertManager{

	final static String CLASSNAME = LemsWebConstant.CLASSNAME_UK_STATEMENT;
	
	public void process() {
		String mailSubject = CLASSNAME+"::process";
		try{
			StatementDto statementDto=callWsStatement();			
			ZonedDateTime now = ZonedDateTime.now();			
			if (statementDto!=null && statementDto.getStatements()!=null){			
				Statement statement=getLastStatement(statementDto.getStatements());				
				if (statement!=null){					
					if (statement.getDetailedCsvUrl()!=null){
						LocalDateTime startDate=Util.isoDateTimeStringToDateTime(statement.getIssuedAt());						
						setIdTask(LemsWebConstant.TASK_UK_5_DAY_BULLETIN);
						setCreationDate(now.format(DateTimeFormatter.ISO_INSTANT));
						setDateLastRevision(now.format(DateTimeFormatter.ISO_INSTANT));
						setDateStart(startDate.plusDays(2).toString()); 
						setDateEnd(startDate.plusDays(8).toString()); 
						setTemporalReferenceStart(Util.getLeadTimeFromDate(startDate)); 
						setTemporalReferenceEnd(Util.getLeadTimeFromDate(startDate.plusDays(5))); 
						setTemporalreferenceDateofcreation(now.format(DateTimeFormatter.ISO_INSTANT)); 
						setTemporalreferenceDateofpublication(now.format(DateTimeFormatter.ISO_INSTANT)); 
						setTemporalreferenceDateoflastrevision(now.format(DateTimeFormatter.ISO_INSTANT)); 
						List<String> geojsonList=new ArrayList<>();
						List<FloodsCsvDto> floodsResult[]=CsvHelper.readFloodCsv(statement.getDetailedCsvUrl());
						String[] leadTime=new String[floodsResult.length];
						BigDecimal[] timespan = new BigDecimal[floodsResult.length];
						int day = 24;
						for (int i=0;i<floodsResult.length;i++){
							Geojson geojson=getGeojson(LemsWebConstant.RESOURCE_UK_5_DAY_BULLETIN_ALERT);							
							HashMap<String, Feature> featuresMap = getFeaturesMap(geojson.getFeatures(),"area_cod");							
							leadTime[i]=Util.getLeadTimeFromDate(startDate.plusDays(i+1));
							timespan[i] = new BigDecimal(day*(i+1));							
							ArrayList<Feature> featuresList=new ArrayList<>();
							int c = 0  ;
							for (FloodsCsvDto flood:floodsResult[i]){
								featuresList.add(getFuture(featuresMap,flood));
								c++;
							}							
							geojson.setFeatures(featuresList.toArray(new Feature[0]));							
							String jsonInString = getGeojson(geojson);							
							geojsonList.add(jsonInString);
						}
						this.setLeadTime(leadTime);
						this.setTimeSpan(timespan);
						
						String result=writeDBAndsendToIDI(null, geojsonList, now);
					}
				}
			}
		} catch (ClientProtocolException e) {
			String mailText="ClientProtocolException message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (IOException e) {
			String mailText="IOException message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		} catch (Exception e) {
			String mailText="Exception message:"+e.getMessage()+ " stacktrace:"+ Util.getStackTrace(e);
			MailHelper.postMail(mailSubject, mailText);
		}
		finally{
			closeDBSession();
		}
	}
	
	public void insertDataDB(Object data, int serviceId, Date today){
		
	}
	
	private StatementDto callWsStatement()
		throws ClientProtocolException, IOException
	{
		HttpGet httpGet = new HttpGet(LemsWebConstant.ENDPOINT_WS_UK_STATEMENTS_ALERT);
		HttpClient client = HttpClientBuilder.create().build();		
	    ObjectMapper mapper = new ObjectMapper();
	    client = HttpClientBuilder.create().build();		
		HttpResponse response = client.execute(httpGet);
		String body = EntityUtils.toString( response.getEntity(), "UTF-8");		
		return mapper.readValue(body, StatementDto.class);
	}
	
		private Statement getLastStatement(Statement[] statements){
		Statement statement=statements[0];
		for (int i=0;i<statements.length;i++){
			if (statements[i].getId().longValue()>statement.getId().longValue())
				statement=statements[i];
		}
		return statement;
	}
		
	
	private Feature getFuture(HashMap<String, Feature> featuresMap,FloodsCsvDto flood){
		Feature result=featuresMap.get(flood.getCounty());				
		addFutureProperties(result,flood);		
		return result;
	}

	
	private void addFutureProperties(Feature feature,FloodsCsvDto flood){
		feature.getProperties().put("rivers",flood.getRivers());
		feature.getProperties().put("surfaceWater",flood.getSurfaceWater());
		feature.getProperties().put("coastalTidal",flood.getCoastalTidal());
		feature.getProperties().put("groundwater",flood.getGroundwater());		
		switch (flood.getFloodRisk()) {
			case "VL": feature.getProperties().put("fill",LemsWebConstant.COLOR_GREEN);
					this.setHazardLevel("1");
					break;
			case "L": feature.getProperties().put("fill",LemsWebConstant.COLOR_YELLOW);
					this.setHazardLevel("2");
					break;
			case "M": feature.getProperties().put("fill",LemsWebConstant.COLOR_ORANGE);
					this.setHazardLevel("3");
					break;
			case "H": feature.getProperties().put("fill",LemsWebConstant.COLOR_RED);
					this.setHazardLevel("4");
					break;
		}		
		setHazardLevelDescription(flood.getFloodRisk());		
		addGeneralProperties(feature);
		addCommonProperties(feature);
	}	
}
