USE [ireactbe-geo-test_db]
GO
/****** Object:  Table [lems].[ParameterTypeMapping]    Script Date: 10/04/2018 09:03:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [lems].[ParameterTypeMapping](
	[Id] [int] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[Creation Time] [datetime] NOT NULL,
	[LastModification Time] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Deletion Time] [datetime] NULL,
 CONSTRAINT [PK_ParameterTypeMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (-1, N'UndefinedDecoding', CAST(N'2018-02-20T16:53:00.000' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (1, N'Oltre 36 ore', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (2, N'Nevicata', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (3, N'Tipo di criticità', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (4, N'Livello di criticità', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (5, N'Successive 36 ore', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (6, N'Livelli di vigilanza', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (7, N'Temporale', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (8, N'Effetti sul territorio', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (9, N'Fenomeni rilevanti', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (10, N'Quota neve', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (11, N'Vento', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (12, N'Anomalia di freddo', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (13, N'Pioggia', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (14, N'Awareness level', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (15, N'Event in Slovenia', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ParameterTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (16, N'Awareness type', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
