USE [ireactbe-geo-test_db]
GO
/****** Object:  Table [lems].[ApiMeasures]    Script Date: 10/04/2018 09:03:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [lems].[ApiMeasures](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ServiceId] [int] NOT NULL,
	[Timestamp] [nvarchar](max) NULL,
	[Notation] [nvarchar](max) NULL,
	[Parameter] [nvarchar](max) NULL,
	[ParameterName] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
	[MeasureUnit] [nvarchar](max) NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModificationTime] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletionTime] [datetime] NULL,
 CONSTRAINT [PK_lems.ApiMeasures] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [lems].[ApiMeasures] ON 

INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (5, 4, N'2017-06-20T18:00:00Z', N'1635TH-level-stage-i-15_min-mASD', N'level', N'Water Level', N'1', N'mASD', CAST(N'2017-06-20T22:36:35.663' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (6, 4, N'2017-06-20T18:00:00Z', N'E700-level-stage-i-15_min-mAOD', N'level', N'Water Level', N'12', N'mAOD', CAST(N'2017-06-20T22:37:11.190' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (7, 4, N'2017-06-20T04:30:00Z', N'3004TH-level-downstage-i-15_min-mAOD', N'level', N'Water Level', N'16', N'mAOD', CAST(N'2017-06-21T00:19:37.627' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (8, 4, N'2017-06-20T21:15:00Z', N'E14992-level-stage-i-15_min-mAOD', N'level', N'Water Level', N'0', N'mAOD', CAST(N'2017-06-20T22:17:44.007' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (9, 4, N'2017-06-20T16:00:00Z', N'531178-rainfall-tipping_bucket_raingauge-t-15_min-mm', N'rainfall', N'Rainfall', N'0', N'mm', CAST(N'2017-06-20T22:18:02.037' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (10, 4, N'2017-06-21T04:00:00Z', N'2195TH-level-stage-i-15_min-mASD', N'level', N'Water Level', N'0', N'mASD', CAST(N'2017-06-21T05:59:25.450' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (11, 4, N'2017-06-21T04:30:00Z', N'2896TH-level-downstage-i-15_min-mASD', N'level', N'Water Level', N'0', N'mASD', CAST(N'2017-06-21T06:09:42.060' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (12, 4, N'2017-06-21T04:00:00Z', N'49147-rainfall-tipping_bucket_raingauge-t-15_min-mm', N'rainfall', N'Rainfall', N'0', N'mm', CAST(N'2017-06-21T06:18:48.630' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (13, 4, N'2017-06-21T04:30:00Z', N'E24845-level-stage-i-15_min-mAOD', N'level', N'Water Level', N'0', N'mAOD', CAST(N'2017-06-21T07:08:30.027' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (14, 4, N'2017-06-21T04:30:00Z', N'E15260-level-stage-i-15_min-mASD', N'level', N'Water Level', N'0', N'mASD', CAST(N'2017-06-21T07:29:05.867' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiMeasures] ([Id], [ServiceId], [Timestamp], [Notation], [Parameter], [ParameterName], [Value], [MeasureUnit], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (15, 4, N'2017-06-21T05:15:00Z', N'L0902-level-stage-i-15_min-mAOD', N'level', N'Water Level', N'1', N'mAOD', CAST(N'2017-06-21T07:35:40.333' AS DateTime), NULL, 0, NULL)
SET IDENTITY_INSERT [lems].[ApiMeasures] OFF
ALTER TABLE [lems].[ApiMeasures]  WITH CHECK ADD  CONSTRAINT [FK_lems.ApiMeasures_lems.Services_ServiceId] FOREIGN KEY([ServiceId])
REFERENCES [lems].[Services] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [lems].[ApiMeasures] CHECK CONSTRAINT [FK_lems.ApiMeasures_lems.Services_ServiceId]
GO
