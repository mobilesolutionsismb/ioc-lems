USE [ireactbe-geo-test_db]
GO
/****** Object:  Table [lems].[ResponseTypeMapping]    Script Date: 10/04/2018 09:03:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [lems].[ResponseTypeMapping](
	[Id] [int] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[Creation Time] [datetime] NOT NULL,
	[LastModification Time] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Deletion Time] [datetime] NULL,
 CONSTRAINT [PK_ResponseTypeMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (-1, N'UndefinedDecoding', CAST(N'2018-02-20T16:53:00.000' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (1, N'Shelter', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (2, N'Evacuate', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (3, N'Prepare', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (4, N'Execute', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (5, N'Avoid', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (6, N'Monitor', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (7, N'Assess', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (8, N'AllClear', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ResponseTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (9, N'None', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
