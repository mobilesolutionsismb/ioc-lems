USE [ireactbe-geo-test_db]
GO
/****** Object:  Table [lems].[StatusMapping]    Script Date: 10/04/2018 09:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [lems].[StatusMapping](
	[Id] [int] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[Creation Time] [datetime] NOT NULL,
	[LastModification Time] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Deletion Time] [datetime] NULL,
 CONSTRAINT [PK_StatusMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [lems].[StatusMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (-1, N'UndefinedDecoding', CAST(N'2018-02-20T16:53:00.000' AS DateTime), NULL, 0, NULL)
INSERT [lems].[StatusMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (1, N'Actual', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[StatusMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (2, N'Exercise', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[StatusMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (3, N'System', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[StatusMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (4, N'Test', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[StatusMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (5, N'Draft', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
