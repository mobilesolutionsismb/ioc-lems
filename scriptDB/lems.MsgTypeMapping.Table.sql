USE [ireactbe-geo-test_db]
GO
/****** Object:  Table [lems].[MsgTypeMapping]    Script Date: 10/04/2018 09:03:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [lems].[MsgTypeMapping](
	[Id] [int] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[Creation Time] [datetime] NOT NULL,
	[LastModification Time] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Deletion Time] [datetime] NULL,
 CONSTRAINT [PK_MsgTypeMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [lems].[MsgTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (-1, N'UndefinedDecoding', CAST(N'2018-02-20T16:53:00.000' AS DateTime), NULL, 0, NULL)
INSERT [lems].[MsgTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (1, N'Alert', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[MsgTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (2, N'Update', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[MsgTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (3, N'Cancel', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[MsgTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (4, N'Ack', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
INSERT [lems].[MsgTypeMapping] ([Id], [Value], [Creation Time], [LastModification Time], [IsDeleted], [Deletion Time]) VALUES (5, N'Error', CAST(N'2018-02-15T08:42:10.100' AS DateTime), NULL, 0, NULL)
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'lems', @level1type=N'TABLE',@level1name=N'MsgTypeMapping'
GO
