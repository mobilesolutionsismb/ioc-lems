USE [ireactbe-geo-test_db]
GO
/****** Object:  Table [lems].[ApiAlerts]    Script Date: 10/04/2018 09:03:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [lems].[ApiAlerts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ServiceId] [int] NOT NULL,
	[Notation] [nvarchar](max) NULL,
	[IsTidal] [bit] NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Severity] [nvarchar](max) NULL,
	[SeverityLevel] [nvarchar](max) NULL,
	[TimeMessageChanged] [datetime] NOT NULL,
	[TimeRaised] [datetime] NOT NULL,
	[TimeSeverityChanged] [datetime] NOT NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastModificationTime] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletionTime] [datetime] NULL,
 CONSTRAINT [PK_lems.ApiAlerts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [lems].[ApiAlerts] ON 

INSERT [lems].[ApiAlerts] ([Id], [ServiceId], [Notation], [IsTidal], [Message], [Severity], [SeverityLevel], [TimeMessageChanged], [TimeRaised], [TimeSeverityChanged], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (10, 1, NULL, 1, NULL, NULL, N'Actual', CAST(N'2017-06-16T19:30:55.110' AS DateTime), CAST(N'2017-06-16T19:30:55.110' AS DateTime), CAST(N'2017-06-16T19:30:55.110' AS DateTime), CAST(N'2017-06-16T19:30:55.110' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiAlerts] ([Id], [ServiceId], [Notation], [IsTidal], [Message], [Severity], [SeverityLevel], [TimeMessageChanged], [TimeRaised], [TimeSeverityChanged], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (11, 1, NULL, 1, NULL, NULL, N'Actual', CAST(N'2017-06-16T19:31:11.487' AS DateTime), CAST(N'2017-06-16T19:31:11.487' AS DateTime), CAST(N'2017-06-16T19:31:11.487' AS DateTime), CAST(N'2017-06-16T19:31:11.487' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiAlerts] ([Id], [ServiceId], [Notation], [IsTidal], [Message], [Severity], [SeverityLevel], [TimeMessageChanged], [TimeRaised], [TimeSeverityChanged], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (12, 1, NULL, 1, NULL, NULL, N'Actual', CAST(N'2017-06-16T20:03:20.957' AS DateTime), CAST(N'2017-06-16T20:03:20.957' AS DateTime), CAST(N'2017-06-16T20:03:20.957' AS DateTime), CAST(N'2017-06-16T20:03:20.957' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiAlerts] ([Id], [ServiceId], [Notation], [IsTidal], [Message], [Severity], [SeverityLevel], [TimeMessageChanged], [TimeRaised], [TimeSeverityChanged], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (13, 1, NULL, 1, NULL, NULL, N'Actual', CAST(N'2017-06-16T20:03:47.477' AS DateTime), CAST(N'2017-06-16T20:03:47.477' AS DateTime), CAST(N'2017-06-16T20:03:47.477' AS DateTime), CAST(N'2017-06-16T20:03:47.477' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiAlerts] ([Id], [ServiceId], [Notation], [IsTidal], [Message], [Severity], [SeverityLevel], [TimeMessageChanged], [TimeRaised], [TimeSeverityChanged], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (14, 1, NULL, 1, NULL, NULL, N'Actual', CAST(N'2017-06-17T13:24:37.527' AS DateTime), CAST(N'2017-06-17T13:24:37.527' AS DateTime), CAST(N'2017-06-17T13:24:37.527' AS DateTime), CAST(N'2017-06-17T13:24:37.527' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiAlerts] ([Id], [ServiceId], [Notation], [IsTidal], [Message], [Severity], [SeverityLevel], [TimeMessageChanged], [TimeRaised], [TimeSeverityChanged], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (15, 1, NULL, 1, NULL, NULL, N'Actual', CAST(N'2017-06-17T17:52:53.117' AS DateTime), CAST(N'2017-06-17T17:52:53.117' AS DateTime), CAST(N'2017-06-17T17:52:53.117' AS DateTime), CAST(N'2017-06-17T17:52:53.117' AS DateTime), NULL, 0, NULL)
INSERT [lems].[ApiAlerts] ([Id], [ServiceId], [Notation], [IsTidal], [Message], [Severity], [SeverityLevel], [TimeMessageChanged], [TimeRaised], [TimeSeverityChanged], [CreationTime], [LastModificationTime], [IsDeleted], [DeletionTime]) VALUES (16, 1, NULL, 1, NULL, NULL, N'Actual', CAST(N'2017-06-18T00:24:20.267' AS DateTime), CAST(N'2017-06-18T00:24:20.267' AS DateTime), CAST(N'2017-06-18T00:24:20.267' AS DateTime), CAST(N'2017-06-18T00:24:20.267' AS DateTime), NULL, 0, NULL)
SET IDENTITY_INSERT [lems].[ApiAlerts] OFF
ALTER TABLE [lems].[ApiAlerts]  WITH CHECK ADD  CONSTRAINT [FK_lems.ApiAlerts_lems.Services_ServiceId] FOREIGN KEY([ServiceId])
REFERENCES [lems].[Services] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [lems].[ApiAlerts] CHECK CONSTRAINT [FK_lems.ApiAlerts_lems.Services_ServiceId]
GO
